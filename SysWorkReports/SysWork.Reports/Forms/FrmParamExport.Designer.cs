﻿namespace SysWork.Reports.Forms
{
    partial class FrmParamExport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CmbFormat = new System.Windows.Forms.ComboBox();
            this.LblFormat = new System.Windows.Forms.Label();
            this.LblDestination = new System.Windows.Forms.Label();
            this.CmbDestination = new System.Windows.Forms.ComboBox();
            this.BtnAccept = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CmbFormat
            // 
            this.CmbFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbFormat.FormattingEnabled = true;
            this.CmbFormat.Items.AddRange(new object[] {
            "Portable Document Format (PDF)",
            "Microsoft Excel",
            "Microsoft Word"});
            this.CmbFormat.Location = new System.Drawing.Point(59, 44);
            this.CmbFormat.Name = "CmbFormat";
            this.CmbFormat.Size = new System.Drawing.Size(295, 21);
            this.CmbFormat.TabIndex = 1;
            // 
            // LblFormat
            // 
            this.LblFormat.AutoSize = true;
            this.LblFormat.Location = new System.Drawing.Point(8, 45);
            this.LblFormat.Name = "LblFormat";
            this.LblFormat.Size = new System.Drawing.Size(45, 13);
            this.LblFormat.TabIndex = 0;
            this.LblFormat.Text = "&Formato";
            // 
            // LblDestination
            // 
            this.LblDestination.AutoSize = true;
            this.LblDestination.Location = new System.Drawing.Point(8, 72);
            this.LblDestination.Name = "LblDestination";
            this.LblDestination.Size = new System.Drawing.Size(43, 13);
            this.LblDestination.TabIndex = 2;
            this.LblDestination.Text = "&Destino";
            // 
            // CmbDestination
            // 
            this.CmbDestination.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbDestination.FormattingEnabled = true;
            this.CmbDestination.Items.AddRange(new object[] {
            "Archivo",
            "Aplicacion predeterminada"});
            this.CmbDestination.Location = new System.Drawing.Point(59, 72);
            this.CmbDestination.Name = "CmbDestination";
            this.CmbDestination.Size = new System.Drawing.Size(295, 21);
            this.CmbDestination.TabIndex = 3;
            // 
            // BtnAccept
            // 
            this.BtnAccept.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BtnAccept.Location = new System.Drawing.Point(55, 108);
            this.BtnAccept.Name = "BtnAccept";
            this.BtnAccept.Size = new System.Drawing.Size(112, 22);
            this.BtnAccept.TabIndex = 4;
            this.BtnAccept.Text = "&Aceptar";
            this.BtnAccept.UseVisualStyleBackColor = true;
            this.BtnAccept.Click += new System.EventHandler(this.BtnAccept_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BtnCancel.Location = new System.Drawing.Point(213, 108);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(112, 22);
            this.BtnCancel.TabIndex = 5;
            this.BtnCancel.Text = "&Cancelar";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(26)))), ((int)(((byte)(26)))));
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(380, 26);
            this.label1.TabIndex = 6;
            this.label1.Text = "Parametros de Exportacion";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FrmParamExport
            // 
            this.AcceptButton = this.BtnAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this.BtnCancel;
            this.ClientSize = new System.Drawing.Size(380, 146);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnAccept);
            this.Controls.Add(this.LblDestination);
            this.Controls.Add(this.CmbDestination);
            this.Controls.Add(this.LblFormat);
            this.Controls.Add(this.CmbFormat);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmParamExport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox CmbFormat;
        private System.Windows.Forms.Label LblFormat;
        private System.Windows.Forms.Label LblDestination;
        private System.Windows.Forms.ComboBox CmbDestination;
        private System.Windows.Forms.Button BtnAccept;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.Label label1;
    }
}