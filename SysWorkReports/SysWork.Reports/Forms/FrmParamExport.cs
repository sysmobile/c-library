﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SysWork.Reports.Forms
{
    public partial class FrmParamExport : Form
    {
        Dictionary<string, ReportExportFormat> Formats = new Dictionary<string, ReportExportFormat>()
        {
            { "Portable Document Format (*.PDF)" , ReportExportFormat.PDF},
            { "Microsoft Excel (*.xls)" , ReportExportFormat.EXCEL},
            { "Microsoft Word (*.doc)" , ReportExportFormat.WORD},
            { "Tagged Image File Format (*.tiff)" , ReportExportFormat.IMAGE_TIFF}
        };

        Dictionary<string, ReportExportDestination> Destinations = new Dictionary<string, ReportExportDestination>()
        {
            { "Archivo en disco" , ReportExportDestination.File},
            { "Abrir en aplicacion predeterminada" , ReportExportDestination.OpenApplication}
        };

        public ReportExportFormat ExportFormat { get; set; }
        public ReportExportDestination ExportDestination { get; set; }

        public FrmParamExport()
        {
            InitializeComponent();
            LoadData();
            
            if (ReportManager.MetroFrameworkize)
                MetroFrameworkIze();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Hide();
            DialogResult = DialogResult.Cancel;
        }

        private void BtnAccept_Click(object sender, EventArgs e)
        {
            ExportDestination = (ReportExportDestination)CmbDestination.SelectedValue;
            ExportFormat = (ReportExportFormat)CmbFormat.SelectedValue;
            DialogResult = DialogResult.OK;
        }
        
        void LoadData()
        {
            CmbFormat.Items.Clear();
            CmbFormat.DataSource = new BindingSource(Formats, null);
            CmbFormat.DisplayMember = "Key";
            CmbFormat.ValueMember = "Value";
            CmbFormat.SelectedValue = ExportFormat;

            CmbDestination.Items.Clear();
            CmbDestination.DataSource = new BindingSource(Destinations, null);
            CmbDestination.DisplayMember = "Key";
            CmbDestination.ValueMember = "Value";
            CmbDestination.SelectedValue = ExportDestination;
        }

        void MetroFrameworkIze()
        {
            var font = new FontResolver().ResolveFont("Open Sans", 12.0f, FontStyle.Regular, GraphicsUnit.Pixel); ;
            Font = font;
            Refresh();
            
            Height = 190;

            BackColor = ReportManager.MetroFrameworkizeUseDarkTheme ? Color.Black : Color.White;
            LblFormat.ForeColor = ReportManager.MetroFrameworkizeUseDarkTheme ? Color.White : Color.Black;
            LblDestination.ForeColor = ReportManager.MetroFrameworkizeUseDarkTheme ? Color.White : Color.Black;

            CmbDestination.ForeColor = ReportManager.MetroFrameworkizeUseDarkTheme ? Color.White : Color.Black;
            CmbDestination.BackColor = ReportManager.MetroFrameworkizeUseDarkTheme ? Color.Black : Color.White;
            
            CmbFormat.ForeColor = ReportManager.MetroFrameworkizeUseDarkTheme ? Color.White : Color.Black;
            CmbFormat.BackColor = ReportManager.MetroFrameworkizeUseDarkTheme ? Color.Black : Color.White;
        }

        private class FontResolver
        {
            public Font ResolveFont(string familyName, float emSize, FontStyle fontStyle, GraphicsUnit unit)
            {
                Font fontTester = new Font(familyName, emSize, fontStyle, unit);
                if (fontTester.Name == familyName || !TryResolve(ref familyName, ref fontStyle))
                {
                    return fontTester;
                }
                fontTester.Dispose();

                FontFamily fontFamily = GetFontFamily(familyName);
                return new Font(fontFamily, emSize, fontStyle, unit);
            }

            private const string OPEN_SANS_REGULAR = "Open Sans";
            private const string OPEN_SANS_LIGHT = "Open Sans Light";
            private const string OPEN_SANS_BOLD = "Open Sans Bold";

            private readonly PrivateFontCollection fontCollection = new PrivateFontCollection();

            private static bool TryResolve(ref string familyName, ref FontStyle fontStyle)
            {
                if (familyName == "Segoe UI Light")
                {
                    familyName = OPEN_SANS_LIGHT;
                    if (fontStyle != FontStyle.Bold) fontStyle = FontStyle.Regular;
                    return true;
                }

                if (familyName == "Segoe UI")
                {
                    if (fontStyle == FontStyle.Bold)
                    {
                        familyName = OPEN_SANS_BOLD;
                        return true;
                    }

                    familyName = OPEN_SANS_REGULAR;
                    return true;
                }

                return false;
            }

            private FontFamily GetFontFamily(string familyName)
            {
                lock (fontCollection)
                {
                    foreach (FontFamily fontFamily in fontCollection.Families)
                        if (fontFamily.Name == familyName) return fontFamily;

                    string resourceName = GetType().Namespace + ".Resources." + familyName.Replace(' ', '_') + ".ttf";

                    Stream fontStream = null;
                    IntPtr data = IntPtr.Zero;
                    try
                    {
                        fontStream = GetType().Assembly.GetManifestResourceStream(resourceName);
                        int bytes = (int)fontStream.Length;
                        data = Marshal.AllocCoTaskMem(bytes);
                        byte[] fontdata = new byte[bytes];
                        fontStream.Read(fontdata, 0, bytes);
                        Marshal.Copy(fontdata, 0, data, bytes);
                        fontCollection.AddMemoryFont(data, bytes);
                        return fontCollection.Families[fontCollection.Families.Length - 1];
                    }
                    finally
                    {
                        if (fontStream != null) fontStream.Dispose();
                        if (data != IntPtr.Zero) Marshal.FreeCoTaskMem(data);
                    }
                }
            }
        }
     }
}