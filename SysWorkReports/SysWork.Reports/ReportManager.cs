﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using SysWork.Reports.Forms;

namespace SysWork.Reports
{
    /// <summary>
    /// The destination of the report
    /// </summary>
    public enum ReportDestination
    {
        Screen,
        Printer,
        ExportToFile
    }
    
    public enum ReportExportDestination
    {
        File,
        OpenApplication,
    }

    public enum ReportExportFormat
    {
        EXCEL,
        WORD,
        PDF,
        IMAGE_TIFF
    }
    
    public static class ReportManagerDefaults
    {
        public static ReportExportFormat REPORT_EXPORT_FORMAT { get; set; } = ReportExportFormat.PDF;
        public static ReportExportDestination REPORT_EXPORT_DESTINATION { get; set; } = ReportExportDestination.File;
    }

    public class ReportManager
    {
        public static bool MetroFrameworkize { get; set; }
        public static bool MetroFrameworkizeUseDarkTheme { get; set; }

        /// <summary>
        /// If set, in case the report name does not include a path,
        /// it will be taken as the report directory
        /// </summary>
        public static string ReportsDirectory { get; set; }

        readonly string _reportFileName;
        readonly FrmReportManager _frmReportManager;

        string _formTitle;
        /// <summary>
        /// Gets or sets the title of the form.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string FormTitle { get => _formTitle; set => _formTitle = value; }

        private ReportDestination _reportDestination;
        /// <summary>
        /// Gets or sets the destination of Report (Screen, Printer, Export).
        /// </summary>
        /// <value>
        /// Screen, Printer, Export.
        /// </value>
        public ReportDestination ReportDestination { get => _reportDestination; set => _reportDestination = value; }

        private ReportExportFormat _reportExportFormat = ReportManagerDefaults.REPORT_EXPORT_FORMAT;
        /// <summary>
        /// Gets or sets the format for export.
        /// </summary>
        /// <value>
        /// The format.
        /// </value>
        public ReportExportFormat ReportExportFormat { get => _reportExportFormat; set => _reportExportFormat = value; }

        private ReportExportDestination _reportExportDestination = ReportManagerDefaults.REPORT_EXPORT_DESTINATION;
        /// <summary>
        /// 
        /// </summary>
        public ReportExportDestination ReportExportDestination { get => _reportExportDestination; set => _reportExportDestination = value; }

        /// <summary>
        /// Gets or sets the report data sources.
        /// </summary>
        /// <value>
        /// The report data sources.
        /// </value>
        public List<ReportDataSource> ReportDataSources { get; set; }

        /// <summary>
        /// Gets or sets the binding source.
        /// </summary>
        /// <value>
        /// The binding source.
        /// </value>
        public BindingSource BindingSource { get; set; }

        bool _showPrinterDialog;
        /// <summary>
        /// Gets or sets a value indicating whether [show printer dialog].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [show printer dialog]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowPrinterDialog { get => _showPrinterDialog; set => _showPrinterDialog = value; }

        PrinterSettings _printerSettings;
        /// <summary>
        /// Gets or sets the printer settings.
        /// </summary>
        /// <value>
        /// The printer settings.
        /// </value>
        public PrinterSettings PrinterSettings { get => _printerSettings; set => _printerSettings = value; }


        /// <summary>
        /// Gets or sets the print controller.
        /// </summary>
        /// <value>
        /// The print controller.
        /// </value>
        PrintController _printerController;
        public PrintController PrinterController { get => _printerController; set => _printerController = value; }

        /// <summary>
        /// Gets the report viewer.
        /// </summary>
        /// <value>
        /// The report viewer.
        /// </value>
        readonly ReportViewer _reportViewer;
        public ReportViewer ReportViewer { get { return _reportViewer; } }

        readonly List<ReportParameter> _reportParameters;

        public int ZoomPercent { get => _reportViewer.ZoomPercent; set => _reportViewer.ZoomPercent = value; }

        public ReportManager(string reportFileName)
        {
            _reportFileName = reportFileName;
            _reportParameters = new List<ReportParameter>();

            _frmReportManager = new FrmReportManager();
            _reportViewer = _frmReportManager.ReportViewer;

            _reportViewer.SetDisplayMode(DisplayMode.PrintLayout);

            _reportLeyendList = new List<ReportLeyend>();

            ReportDataSources = new List<ReportDataSource>();
            BindingSource = _frmReportManager.BindingSource;
        }

        public ReportManager AddParameter(string name, string value)
        {
            var reportParameter = new ReportParameter(name, value);
            _reportParameters.Add(reportParameter);
            return this;
        }

        public ReportManager AddParameter(string name, string[] values)
        {
            var reportParameter = new ReportParameter(name, values);
            _reportParameters.Add(reportParameter);
            return this;
        }

        public ReportManager SetDisplayMode(DisplayMode displayMode)
        {
            _reportViewer.SetDisplayMode(displayMode);
            return this;
        }
        public void ExecuteReport()
        {
            SetReportViewerProperties();

            switch (ReportDestination)
            {
                case ReportDestination.Screen:

                    _frmReportManager.Show();
                    break;

                case ReportDestination.Printer:

                    ReportPrintDocument reportPrintDocument = new ReportPrintDocument(_reportViewer.LocalReport);

                    if (_printerSettings != null)
                        reportPrintDocument.PrinterSettings = _printerSettings;

                    if (_printerController != null)
                        reportPrintDocument.PrintController = _printerController;

                    if (_showPrinterDialog)
                    {
                        var printerDialog = new PrintDialog();
                        if (printerDialog.ShowDialog() == DialogResult.OK)
                            reportPrintDocument.PrinterSettings = printerDialog.PrinterSettings;
                        else
                            return;
                    }

                    reportPrintDocument.Print();
                    break;

                case ReportDestination.ExportToFile:

                    using (var frmParameters = new FrmParamExport())
                    {
                        frmParameters.ExportFormat = _reportExportFormat;
                        if (frmParameters.ShowDialog() == DialogResult.OK)
                        {
                            if (frmParameters.ExportDestination == ReportExportDestination.File)
                            {
                                Export(frmParameters.ExportFormat, true, true);
                            }
                            else   // application
                            {
                                var tempFile = Path.GetTempFileName().Replace(".tmp",DefaultExtension(frmParameters.ExportFormat));
                                Export(frmParameters.ExportFormat, tempFile);
                                System.Diagnostics.Process.Start(tempFile); 
                            }
                        }
                    }
                    break;
            }
        }

        void SetReportViewerProperties()
        {
            _frmReportManager.Text = _formTitle;
            _reportViewer.ProcessingMode = ProcessingMode.Local;

            var rdlcFile = _reportFileName;
            if (!rdlcFile.Contains(@"\"))
                if (!string.IsNullOrWhiteSpace(ReportManager.ReportsDirectory))
                    rdlcFile = Path.Combine(ReportManager.ReportsDirectory, _reportFileName);

            _reportViewer.LocalReport.ReportPath = rdlcFile;
            //_ = _reportViewer.DisplayMode;
            _reportViewer.LocalReport.SetParameters(_reportParameters);

            foreach (ReportDataSource rds in ReportDataSources)
                _reportViewer.LocalReport.DataSources.Add(rds);

            _reportViewer.RefreshReport();
        }
        public void Export(ReportExportFormat format, string fileName)
        {
            ExportImplementation(format, fileName, false, false);
        }

        public void Export(ReportExportFormat format, bool showSaveDialog, bool promptOverwriteFile = false)
        {
            ExportImplementation(format, "", showSaveDialog, promptOverwriteFile);
        }

        public void Export(ReportExportFormat format, bool showSaveDialog, string defaultName, bool promptOverwriteFile = false)
        {
            ExportImplementation(format, defaultName, showSaveDialog, promptOverwriteFile);
        }

        void ExportImplementation(ReportExportFormat format, string fileName, bool showSaveDialog, bool promptOverwriteFile)
        {
            SetReportViewerProperties();

            if (showSaveDialog)
            {
                var saveFileDialog = new SaveFileDialog
                {
                    Title = "Exportar",
                    Filter = FilterFormat(format),
                    DefaultExt = DefaultExtension(format),
                    FileName = fileName,
                    OverwritePrompt = promptOverwriteFile
                };

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    fileName = saveFileDialog.FileName;
                else
                    return;
            }

            byte[] byteViewer = _reportViewer.LocalReport.Render(format.ToString(), null, out string v_mimetype, out string v_encoding, out string v_filename_extension, out string[] v_streamids, out Warning[] warnings);
            FileStream newFile = new FileStream(fileName, FileMode.Create);
            newFile.Write(byteViewer, 0, byteViewer.Length);
            newFile.Close();
        }
        string FilterFormat(ReportExportFormat format)
        {
            switch (format)
            {
                case ReportExportFormat.EXCEL:
                    return "Microsoft Excel (*.XLS) | *.xls";
                case ReportExportFormat.WORD:
                    return "Microsoft Word (*.DOC) | *.doc";
                case ReportExportFormat.PDF:
                    return "Portable Document Format (*.PDF) | *.pdf";
                case ReportExportFormat.IMAGE_TIFF:
                    return "Tagged Image File Format (*.TIFF) | *.tiff";
                default:
                    throw new FormatException("The format is not supported");
            }
        }
        string DefaultExtension(ReportExportFormat format)
        {
            switch (format)
            {
                case ReportExportFormat.EXCEL:
                    return ".xls";
                case ReportExportFormat.WORD:
                    return ".doc";
                case ReportExportFormat.PDF:
                    return ".pdf";
                case ReportExportFormat.IMAGE_TIFF:
                    return ".tiff";
                default:
                    throw new FormatException("The format is not supported");
            }
        }

        public void AddReportLeyend(string leyendName, object value, string ifFromAndToAreNullReturnThisValue = "")
        {
            var filter = _reportLeyendList.Where(f => f.LeyendName == leyendName).SingleOrDefault();
            if (filter == null)
            {
                filter = new ReportLeyend(leyendName, value);
                filter.IfFromAndToAreNullReturnThisValue = ifFromAndToAreNullReturnThisValue;
                _reportLeyendList.Add(filter);
            }
            else
            {
                filter.ToValue = value;
            }
        }
        

        public void AddReportLeyend(string leyendName, object fromValue, object toValue,string ifFromAndToAreNullReturnThisValue = null)
        {
            var filter = _reportLeyendList.Where(f => f.LeyendName == leyendName).SingleOrDefault();
            if (filter == null)
            {
                filter = new ReportLeyend(leyendName, fromValue, toValue);
                filter.IfFromAndToAreNullReturnThisValue = ifFromAndToAreNullReturnThisValue;

                _reportLeyendList.Add(filter);
            }
            else
            {
                filter.FromValue = fromValue;
                filter.ToValue = toValue;
            }
        }

        public string GetReportLeyend()
        {
            var result = "";
            foreach (var filter in _reportLeyendList)
            {
                if (filter.FromValue != null && (filter.FromValue is DateTime))
                    filter.FromValue = FriendlyDateValue((DateTime)filter.FromValue);

                if (filter.ToValue != null && filter.ToValue is DateTime)
                    filter.ToValue = FriendlyDateValue((DateTime)filter.ToValue);

                if (filter.FromValue == null && filter.ToValue == null)
                {
                    if (filter.IfFromAndToAreNullReturnThisValue!=null)
                        result += $"| {filter.LeyendName}: {filter.IfFromAndToAreNullReturnThisValue}"; 
                }
                else if (filter.FromValue != null && filter.ToValue != null)
                {
                    if (filter.FromValue.ToString() == filter.ToValue.ToString())
                        result += $"| {filter.LeyendName}={filter.FromValue}";
                    else
                        result += $"| {filter.LeyendName}>={filter.FromValue} & <={filter.ToValue}";
                }
                else if (filter.FromValue != null && filter.ToValue == null)
                {
                    result += $"| {filter.LeyendName}>={filter.FromValue} ";
                }
                else if (filter.FromValue == null && filter.ToValue != null)
                {
                    result += $" | {filter.LeyendName}<={filter.ToValue}";
                }
            }
 
            if ((!string.IsNullOrWhiteSpace(result)) && (result.StartsWith("|")))
                result = result.Substring(1);



            return result;
        }
        
        private string FriendlyDateValue(DateTime dateTimeValue)
        {
            var format = "dd/MM/yy HH:mm:ss";
            if (dateTimeValue.Second == 0 && dateTimeValue.Minute == 0 && dateTimeValue.Hour == 0)
                format = "dd/MM/yy";

            return dateTimeValue.ToString(format);
        }

        private readonly List<ReportLeyend> _reportLeyendList;

        private class ReportLeyend
        {
            public string LeyendName { get; set; }
            public object FromValue { get; set; }
            public object ToValue { get; set; }
            public string IfFromAndToAreNullReturnThisValue { get; set; }

            public ReportLeyend(string leyendName, object fromValue, object toValue) : this(leyendName,fromValue)
            {
                ToValue = toValue;
            }

            public ReportLeyend(string leyendName,object fromValue):this()
            {
                LeyendName=leyendName;
                FromValue = fromValue;
            }

            public ReportLeyend()
            {
            }
        }
    }

    public class ReportManagerException : Exception 
    {
        public ReportManagerException(string message) : base(message) 
        {
        }
    }
}