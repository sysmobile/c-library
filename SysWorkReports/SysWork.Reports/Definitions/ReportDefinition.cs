﻿namespace SysWork.Reports.Definitions
{
    public class ReportDefinition
    {
        public long IdReportDefinition { get; set; }
        public string ReportDefinitionKey { get; set; }
        /// <summary>
        /// Friendly name, commonly used in parameters
        /// </summary>
        public string FriendlyName { get; set; }
        /// <summary>
        /// Title that will be printed on the report
        /// </summary>
        public string ReportTitle { get; set; }
        /// <summary>
        /// report file path.
        /// </summary>
        public string RDLCFileName { get; set; }
        public ReportDefinition()
        {

        }

        public ReportDefinition(string friendlyName, string rdlcFileName, string reportTitle)
        {
            FriendlyName = friendlyName;
            RDLCFileName = rdlcFileName;
            ReportTitle = reportTitle;
        }
    }
}
