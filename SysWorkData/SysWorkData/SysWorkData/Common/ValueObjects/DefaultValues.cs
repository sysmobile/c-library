﻿namespace SysWork.Data.Common.ValueObjects
{
    /// <summary>
    /// Determines the default database engine for methods where it is not provided.
    /// </summary>
    public static class DefaultValues
    {
        public static EDatabaseEngine DefaultDatabaseEngine { get; set; } = EDatabaseEngine.MSSqlServer;
    }
}
