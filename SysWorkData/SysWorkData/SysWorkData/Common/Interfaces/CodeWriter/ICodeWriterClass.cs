﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysWork.Data.Common.Interfaces.CodeWriter
{
    public interface ICodeWriterClass
    {
        string GetTextClass();
        string GetTextClassWithoutConstructorsAndMethods();
    }
}
