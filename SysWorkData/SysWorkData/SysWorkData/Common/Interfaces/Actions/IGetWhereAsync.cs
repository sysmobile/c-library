﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SysWork.Data.Common.Interfaces.Actions
{
    /// <summary>
    /// Gets a record that match with the predicate.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    public interface IGetWhereAsync<TEntity>
    {
        /// <summary>
        /// Gets a record that match with the predicate.
        /// </summary>
        /// <param name="predicate">The lambda expression filter.</param>
        /// <returns></returns>
        Task<TEntity> GetWhereAsync(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Gets a record that match with the predicate using a custom dbCommand timeout.
        /// </summary>
        /// <param name="predicate">The lambda expression filter.</param>
        /// <param name="commandTimeOut">The command time out.</param>
        /// <returns></returns>
        Task<TEntity> GetWhereAsync(Expression<Func<TEntity, bool>> predicate, int commandTimeOut);

        /// <summary>
        /// Gets a record that match with the predicate using an DbConnection.
        /// </summary>
        /// <param name="predicate">The lambda expression filter.</param>
        /// <param name="dbConnection">The database connection.</param>
        /// <returns></returns>
        Task<TEntity> GetWhereAsync(Expression<Func<TEntity, bool>> predicate, DbConnection dbConnection);

        /// <summary>
        /// Gets a record that match with the predicate using an DbConnection and a custom dbCommand timeout.
        /// </summary>
        /// <param name="predicate">The lambda expression filter.</param>
        /// <param name="dbConnection">The database connection.</param>
        /// <param name="commandTimeOut">The command time out.</param>
        /// <returns></returns>
        Task<TEntity> GetWhereAsync(Expression<Func<TEntity, bool>> predicate, DbConnection dbConnection, int commandTimeOut);

        /// <summary>
        /// Gets a record that match with the predicate using an DbTransaction.
        /// </summary>
        /// <param name="predicate">The lambda expression filter.</param>
        /// <param name="dbTransaction">The database transaction.</param>
        /// <returns></returns>
        Task<TEntity> GetWhereAsync(Expression<Func<TEntity, bool>> predicate, DbTransaction dbTransaction);

        /// <summary>
        /// Gets a record that match with the predicate using an DbTransaction and a custom dbCommand timeout.
        /// </summary>
        /// <param name="predicate">The lambda expression filter.</param>
        /// <param name="dbTransaction">The database transaction.</param>
        /// <param name="commandTimeOut">The command time out.</param>
        /// <returns></returns>
        Task<TEntity> GetWhereAsync(Expression<Func<TEntity, bool>> predicate, DbTransaction dbTransaction, int commandTimeOut);

        /// <summary>
        /// Gets a record that match with the predicate using an DbConnection and DbTransaction.
        /// </summary>
        /// <param name="predicate">The lambda expression filter.</param>
        /// <param name="dbConnection">The database connection.</param>
        /// <param name="dbTransaction">The database transaction.</param>
        /// <returns></returns>
        Task<TEntity> GetWhereAsync(Expression<Func<TEntity, bool>> predicate, DbConnection dbConnection, DbTransaction dbTransaction);

        /// <summary>
        /// Gets a record that match with the predicate using an DbConnection, DbTransaction and a custom dbCommand timeout.
        /// </summary>
        /// <param name="predicate">The lambda expression filter.</param>
        /// <param name="dbConnection">The database connection.</param>
        /// <param name="dbTransaction">The database transaction.</param>
        /// <param name="commandTimeOut">The command time out.</param>
        /// <returns></returns>
        Task<TEntity> GetWhereAsync(Expression<Func<TEntity, bool>> predicate, DbConnection dbConnection, DbTransaction dbTransaction, int? commandTimeOut);
    }
}
