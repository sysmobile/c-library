﻿using System;
using System.Data;
using System.Linq.Expressions;

namespace SysWork.Data.Common.Interfaces.Actions
{
    /// <summary>
    /// Gets and DataTable with the records that match with the predicate.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    public interface IGetDataTableWhere<TEntity>
    {
        /// <summary>
        /// Gets and DataTable with the records that match with the predicate.
        /// </summary>
        /// <param name="predicate">The lambda expression filter.</param>
        /// <returns></returns>
        DataTable GetDataTableWhere(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Gets and DataTable with the records that match with the predicate using a custom dbCommand timeout.
        /// </summary>
        /// <param name="predicate">The lambda expression filter.</param>
        /// <param name="commandTimeOut">The command time out.</param>
        /// <returns></returns>
        DataTable GetDataTableWhere(Expression<Func<TEntity, bool>> predicate, int commandTimeOut);

        /// <summary>
        /// Gets and DataTable with the records that match with the predicate using an DbConnection.
        /// </summary>
        /// <param name="predicate">The lambda expression filter.</param>
        /// <param name="dbConnection">The database connection.</param>
        /// <returns></returns>
        DataTable GetDataTableWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection);

        /// <summary>
        /// Gets and DataTable with the records that match with the predicate using an DbConnection and a custom dbCommand timeout.
        /// </summary>
        /// <param name="predicate">The lambda expression filter.</param>
        /// <param name="dbConnection">The database connection.</param>
        /// <param name="commandTimeOut">The command time out.</param>
        /// <returns></returns>
        DataTable GetDataTableWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection, int commandTimeOut);

        /// <summary>
        /// Gets and DataTable with the records that match with the predicate using an DbTransaction.
        /// </summary>
        /// <param name="predicate">The lambda expression filter.</param>
        /// <param name="dbTransaction">The database transaction.</param>
        /// <returns></returns>
        DataTable GetDataTableWhere(Expression<Func<TEntity, bool>> predicate, IDbTransaction dbTransaction);

        /// <summary>
        /// Gets and DataTable with the records that match with the predicate using an DbTransaction and a custom dbCommand timeout.
        /// </summary>
        /// <param name="predicate">The lambda expression filter.</param>
        /// <param name="dbTransaction">The database transaction.</param>
        /// <param name="commandTimeOut">The command time out.</param>
        /// <returns></returns>
        DataTable GetDataTableWhere(Expression<Func<TEntity, bool>> predicate, IDbTransaction dbTransaction, int commandTimeOut);

        /// <summary>
        /// Gets and DataTable with the records that match with the predicate using an DbConnection and DbTransaction.
        /// </summary>
        /// <param name="predicate">The lambda expression filter.</param>
        /// <param name="dbConnection">The database connection.</param>
        /// <param name="dbTransaction">The database transaction.</param>
        /// <returns></returns>
        DataTable GetDataTableWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection, IDbTransaction dbTransaction);

        /// <summary>
        /// Gets and DataTable with the records that match with the predicate using an DbConnection, DbTransaction and a custom dbCommand timeout.
        /// </summary>
        /// <param name="predicate">The lambda expression filter.</param>
        /// <param name="dbConnection">The database connection.</param>
        /// <param name="dbTransaction">The database transaction.</param>
        /// <param name="commandTimeOut">The command time out.</param>
        /// <returns></returns>
        DataTable GetDataTableWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection, IDbTransaction dbTransaction, int? commandTimeOut);
    }
}
