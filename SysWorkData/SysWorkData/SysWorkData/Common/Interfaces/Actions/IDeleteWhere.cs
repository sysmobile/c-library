﻿using System;
using System.Data;
using System.Linq.Expressions;

namespace SysWork.Data.Common.Interfaces.Actions
{
    /// <summary>
    /// Deletes all records that match with the predicate.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    public interface IDeleteWhere<TEntity>
    {
        /// <summary>
        /// Deletes all records that match with the predicate.
        /// </summary>
        /// <param name="predicate">The lambda expression filter .</param>
        /// <returns></returns>
        long DeleteWhere(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Deletes all records that match with the predicate, using a custom dbCommand timeout.
        /// </summary>
        /// <param name="predicate">The lambda expression filter .</param>
        /// <param name="commandTimeOut">The command time out.</param>
        /// <returns></returns>
        long DeleteWhere(Expression<Func<TEntity, bool>> predicate, int commandTimeOut);

        /// <summary>
        /// Deletes all records that match with the predicate using an DnConnection.
        /// </summary>
        /// <param name="predicate">The lambda expression filter .</param>
        /// <param name="dbConnection">The database connection.</param>
        /// <returns></returns>
        long DeleteWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection);

        /// <summary>
        /// Deletes all records that match with the predicate, using an DbConnection and a custom dbCommand timeout.
        /// </summary>
        /// <param name="predicate">The lambda expression filter .</param>
        /// <param name="dbConnection">The database connection.</param>
        /// <param name="commandTimeOut">The command time out.</param>
        /// <returns></returns>
        long DeleteWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection, int commandTimeOut);

        /// <summary>
        /// Deletes all records that match with the predicate, using an dbTransaction.
        /// </summary>
        /// <param name="predicate">The lambda expression filter .</param>
        /// <param name="dbTransaction">The database transaction.</param>
        /// <returns></returns>
        long DeleteWhere(Expression<Func<TEntity, bool>> predicate, IDbTransaction dbTransaction);

        /// <summary>
        /// Deletes all records that match with the predicate, using an dbTransaction and a custom dbCommand timeout.
        /// </summary>
        /// <param name="predicate">The lambda expression filter .</param>
        /// <param name="dbTransaction">The database transaction.</param>
        /// <param name="commandTimeOut">The command time out.</param>
        /// <returns></returns>
        long DeleteWhere(Expression<Func<TEntity, bool>> predicate, IDbTransaction dbTransaction, int commandTimeOut);

        /// <summary>
        /// Deletes all records that match with the predicate, using an dbConnection and dbTransaction.
        /// </summary>
        /// <param name="predicate">The lambda expression filter .</param>
        /// <param name="dbConnection">The database connection.</param>
        /// <param name="dbTransaction">The database transaction.</param>
        /// <returns></returns>
        long DeleteWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection, IDbTransaction dbTransaction);

        /// <summary>
        /// Deletes all records that match with the predicate, using an dbConnection, dbTransaction and a custom dbCommand timeout.
        /// </summary>
        /// <param name="predicate">The lambda expression filter .</param>
        /// <param name="dbConnection">The database connection.</param>
        /// <param name="dbTransaction">The database transaction.</param>
        /// <param name="commandTimeOut">The command time out.</param>
        /// <returns></returns>
        long DeleteWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection, IDbTransaction dbTransaction, int? commandTimeOut);
    }
}
