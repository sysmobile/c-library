﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SysWork.Data.Common.Extensions
{
    public static class LinqExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="source"></param>
        /// <param name="keySelector"></param>
        /// <![CDATA[
        /// var query = people.DistinctBy(p => p.Id);
        /// ]]>
        /// <returns></returns>
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="list"></param>
        /// <![CDATA[
        ///      var states = _objdatasources.StateList().Where(s => s.In(countrycodes));
        ///      var states = tooManyStates.Where(s => s.In("x", "y", "z"));        
        /// ]]>
        /// <returns></returns>
        public static bool In<T>(this T source, params T[] list)
        {
            return list.Contains(source);
        }
    }

}
