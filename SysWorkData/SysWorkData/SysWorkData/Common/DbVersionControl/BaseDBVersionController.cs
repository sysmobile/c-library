﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using SysWork.Data.Common.DataObjectProvider;
using SysWork.Data.Common.ValueObjects;

namespace SysWork.Data.Common.DbVersionControl
{
    public abstract class BaseDBVersionController
    {
        public abstract string GetVersion();

        private readonly IDbConnection _dbConnection;
        protected IDbConnection DbConnection { get { return _dbConnection; } }

        private readonly EDatabaseEngine _databaseEngine;
        protected EDatabaseEngine DatabaseEngine { get { return _databaseEngine; } }

        private readonly string _connectionString;
        protected string ConnectionString { get { return _connectionString; } }

        public BaseDBVersionController(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }
        public BaseDBVersionController(EDatabaseEngine databaseEngine, string connectionString)
        {
            _databaseEngine = databaseEngine;
            _connectionString = connectionString;

            _dbConnection = new DbObjectProvider(_databaseEngine).GetIDbConnection();
        }

        /// <summary>
        /// Controls the version.
        /// </summary>
        /// <returns></returns>
        public bool RunCheckPoints() 
        {
            if (_dbConnection.State != ConnectionState.Open)
                _dbConnection.Open();

            var @class = this.GetType();
            var methods = @class.GetMethods().Where(m => m.Name.StartsWith("CheckPoint") && m.ReturnType == typeof(bool)).ToList();
            try
            {
                foreach (var m in methods)
                {
                    MethodInfo method = @class.GetMethod(m.Name);
                    var result = method.Invoke(this, null);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return true;
        }

        protected bool VerifyTables()
        {
            return true;
        }

        private string GetScriptDbVersion_MSSQLServer()
        {
            var result = "";

            return result;
        }
        private string GetScriptDbVersionHistory_MSSQLServer()
        {
            var result = "";

            return result;
        }
    }
}
