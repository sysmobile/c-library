﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace SysWork.Data.Utilities.MSSqlServer
{
    public class DbUtilMSSqlServer : DbUtilBase
    {
        readonly string _connectionString;

        public DbUtilMSSqlServer()
        {
        }

        public DbUtilMSSqlServer(string connectionString)
        {
            _connectionString = connectionString;
        }

        #region ConnectionSuccess
        public override bool ConnectionSuccess()
            => ConnectionSuccessImplementation(_connectionString, out _);

        public override bool ConnectionSuccess(string connectionString)
            => ConnectionSuccessImplementation(connectionString, out _);

        public override bool ConnectionSuccess(out string errorMessage)
            => ConnectionSuccessImplementation(_connectionString, out errorMessage);

        public override bool ConnectionSuccess(string connectionString, out string errorMessage)
            => ConnectionSuccessImplementation(connectionString, out errorMessage);

        private bool ConnectionSuccessImplementation(string connectionString, out string errorMessage)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new ArgumentException("ConnectionString is not set.");

            errorMessage = "";
            try
            {
                using (var dbConnection = new SqlConnection(connectionString))
                {
                    dbConnection.Open();
                    dbConnection.Close();
                }
                return true;
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                return false;
            }
        }
        #endregion

        #region ExecuteBatchNonQuery
        public override bool ExecuteBatchNonQuery(string query) => ExecuteBatchNonQueryImplementation(query, _connectionString);
        public override bool ExecuteBatchNonQuery(string ConnectionString, string query) => ExecuteBatchNonQueryImplementation(query, ConnectionString);
        public override bool ExecuteBatchNonQuery(IDbConnection dbConnection, string query) => ExecuteBatchNonQueryImplementation(query, null, dbConnection);
        public override bool ExecuteBatchNonQuery(IDbTransaction dbTransaction, string query) => ExecuteBatchNonQueryImplementation(query, null, null, dbTransaction);
        public bool ExecuteBatchNonQueryImplementation(string query, string connectionString = null, IDbConnection dbConnection = null,IDbTransaction dbTransaction = null)
        {
            bool closeConnection = ((dbConnection == null) && (dbTransaction == null));

            if (dbConnection == null && dbTransaction != null)
                dbConnection = dbTransaction.Connection;

            IDbConnection dbConnectionInUse = dbConnection ?? new SqlConnection(connectionString);

            string sqlBatch = string.Empty;
            try
            {
                if (dbConnectionInUse.State != ConnectionState.Open)
                    dbConnectionInUse.Open();

                IDbCommand dbCommand = dbConnectionInUse.CreateCommand();
                dbCommand.CommandText = string.Empty;

                if (dbTransaction != null)
                    dbCommand.Transaction = dbTransaction;

                query += "\nGO";
                foreach (string line in query.Split(new string[2] { "\n", "\r" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    if (line.ToUpperInvariant().Trim() == "GO")
                    {
                        if (!string.IsNullOrEmpty(sqlBatch.Trim()))
                        {
                            dbCommand.CommandText = sqlBatch;
                            dbCommand.ExecuteNonQuery();
                        }
                        sqlBatch = string.Empty;
                    }
                    else
                    {
                        sqlBatch += line + "\n";
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (closeConnection)
                {
                    if (dbConnectionInUse.State == ConnectionState.Open)
                        dbConnectionInUse.Close();

                    dbConnectionInUse.Dispose();
                }
            }
            return true;
        }
        #endregion

        #region ExistsColumn
        public override bool ExistsColumn(string tableName, string columnName)
            => ExistColumnImplementation(tableName, columnName, _connectionString);

        public override bool ExistsColumn(string connectionString, string tableName, string columnName)
            => ExistColumnImplementation(tableName, columnName, connectionString);

        public override bool ExistsColumn(DbConnection dbConnection, string tableName, string columnName)
            => ExistColumnImplementation(tableName, columnName, null, dbConnection);

        private bool ExistColumnImplementation(string tableName, string columnName, string connectionString = null, DbConnection dbConnection = null)
        {
            return GetColumnSchemaInformationImplementation(tableName, columnName, connectionString, dbConnection).Rows.Count > 0;
        }
        #endregion

        #region ExistsTable
        public override bool ExistsTable(string tableName)
            => ExistsTableImplementation(tableName, _connectionString);
        
        public override bool ExistsTable(string connectionString, string tableName)
            => ExistsTableImplementation(tableName, connectionString);
        
        public override bool ExistsTable(DbConnection dbConnection, string tableName)
            => ExistsTableImplementation(tableName, null, dbConnection);

        private bool ExistsTableImplementation(string tableName, string connectionString = null, DbConnection dbConnection = null)
            => GetTableSchemaInformationImplementation(tableName, connectionString, dbConnection).Rows.Count > 0;
        #endregion

        #region ExistsView
        public override bool ExistsView(string viewName)
            => ExistsViewImplementation(viewName, _connectionString);

        public override bool ExistsView(string connectionString, string viewName)
            => ExistsViewImplementation(viewName, connectionString);

        public override bool ExistsView(DbConnection dbConnection, string viewName)
            => ExistsViewImplementation(viewName, null, dbConnection);

        private bool ExistsViewImplementation(string viewName, string connectionString = null, DbConnection dbConnection = null)
            => GetViewSchemaInformationImplementation(viewName, connectionString, dbConnection).Rows.Count > 0;
        #endregion

        #region GetColumnSchemaInformation
        public override DataTable GetColumnSchemaInformation(string tableName, string columnName)
            => GetColumnSchemaInformationImplementation(tableName, columnName, _connectionString);

        public override DataTable GetColumnSchemaInformation(string connectionString, string tableName, string columnName)
            => GetColumnSchemaInformationImplementation(tableName, columnName, connectionString);

        public override DataTable GetColumnSchemaInformation(DbConnection dbConnection, string tableName, string columnName)
            => GetColumnSchemaInformationImplementation(tableName, columnName, null, dbConnection);

        private DataTable GetColumnSchemaInformationImplementation(string tableName, string columnName, string connectionString = null, DbConnection dbConnection = null)
        {
            DbConnection dbConnectionInUse;
            bool closeConnection = false;

            if (dbConnection == null)
            {
                if (string.IsNullOrWhiteSpace(connectionString))
                    throw new ArgumentException("ConnectionString is not set.");

                dbConnectionInUse = new SqlConnection(connectionString);
                closeConnection = true;
            }
            else
            {
                dbConnectionInUse = dbConnection;
            }

            if (dbConnectionInUse.State != ConnectionState.Open)
                dbConnectionInUse.Open();

            DataTable dtColumns = dbConnectionInUse.GetSchema("Columns", new[] { null, null, tableName, columnName });

            if (closeConnection)
                dbConnectionInUse.Close();

            return dtColumns;
        }
        #endregion

        #region GetTables
        public override List<string> GetTables()
            => GetTablesImplementation(_connectionString);

        public override List<string> GetTables(string connectionString)
            => GetTablesImplementation(connectionString);

        public override List<string> GetTables(DbConnection dbConnection)
            => GetTablesImplementation(null, dbConnection);

        private List<string> GetTablesImplementation(string connectionString = null, DbConnection dbConnection = null)
        {
            var tables = new List<string>();

            DataTable schema = GetTableSchemaInformationImplementation(null, connectionString, dbConnection);
            foreach (DataRow row in schema.Rows)
                tables.Add(row[2].ToString());

            return tables;
        }
        #endregion

        #region GetTableSchemaInformation
        public override DataTable GetTableSchemaInformation(string tableName)
            => GetTableSchemaInformationImplementation(tableName, _connectionString);

        public override DataTable GetTableSchemaInformation(string connectionString, string tableName)
            => GetTableSchemaInformationImplementation(tableName, connectionString);

        public override DataTable GetTableSchemaInformation(DbConnection dbConnection, string tableName)
            => GetTableSchemaInformationImplementation(tableName, null, dbConnection);

        private DataTable GetTableSchemaInformationImplementation(string tableName, string connectionString = null, DbConnection dbConnection = null)
        {
            DbConnection dbConnectionInUse;
            bool closeConnection = false;

            if (dbConnection == null)
            {
                if (string.IsNullOrWhiteSpace(connectionString))
                    throw new ArgumentException("ConnectionString is not set.");

                dbConnectionInUse = new SqlConnection(connectionString);
                closeConnection = true;
            }
            else
            {
                dbConnectionInUse = dbConnection;
            }

            if (dbConnectionInUse.State != ConnectionState.Open)
                dbConnectionInUse.Open();

            var dtTables = dbConnectionInUse.GetSchema("Tables", new string[4] { null, null, tableName, "BASE TABLE" });

            if (closeConnection)
                dbConnectionInUse.Close();

            return dtTables;
        }
        #endregion

        #region GetViews
        public override List<string> GetViews()
            => GetViewsImplementation(_connectionString);

        public override List<string> GetViews(string connectionString)
            => GetViewsImplementation(connectionString);

        public override List<string> GetViews(DbConnection dbConnection)
            => GetViewsImplementation(null, dbConnection);

        private List<string> GetViewsImplementation(string connectionString=null,DbConnection dbConnection = null)
        {

            List<string> views = new List<string>();
            DataTable schema = GetViewSchemaInformationImplementation(null, connectionString, dbConnection);

            foreach (DataRow row in schema.Rows)
                    views.Add(row[2].ToString());
            
            return views;
        }
        #endregion

        #region GetViewSchemaInformation
        public override DataTable GetViewSchemaInformation(string viewName)
            => GetViewSchemaInformationImplementation(viewName, _connectionString, null);
        public override DataTable GetViewSchemaInformation(string connectionString, string viewName)
            => GetViewSchemaInformationImplementation(viewName, connectionString, null);

        public override DataTable GetViewSchemaInformation(DbConnection dbConnection, string viewName)
            => GetViewSchemaInformationImplementation(viewName, null, dbConnection);

        private DataTable GetViewSchemaInformationImplementation(string viewName, string connectionString = null, DbConnection dbConnection = null)
        {
            DbConnection dbConnectionInUse;
            bool closeConnection = false;

            if (dbConnection == null)
            {
                if (string.IsNullOrWhiteSpace(connectionString))
                    throw new ArgumentException("ConnectionString is not set.");

                dbConnectionInUse = new SqlConnection(connectionString);
                closeConnection = true;
            }
            else
            {
                dbConnectionInUse = dbConnection;
            }

            if (dbConnectionInUse.State != ConnectionState.Open)
                dbConnectionInUse.Open();

            var dtViews = dbConnectionInUse.GetSchema("Tables", new string[] { null, null, viewName, "VIEW" });

            if (closeConnection)
                dbConnectionInUse.Close();

            return dtViews;
        }
        #endregion

        #region IsValidConnectionString
        public override bool IsValidConnectionString()
            => IsValidConnectionStringImplementation(_connectionString, out _);

        public override bool IsValidConnectionString(string connectionString)
            => IsValidConnectionStringImplementation(connectionString, out _);

        public override bool IsValidConnectionString(out string errorMessage)
            => IsValidConnectionStringImplementation(_connectionString, out errorMessage);

        public override bool IsValidConnectionString(string connectionString, out string errorMessage)
            => IsValidConnectionStringImplementation(connectionString, out errorMessage);

        private bool IsValidConnectionStringImplementation(string connectionString, out string errorMessage)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new ArgumentException("ConnectionString is not set.");

            bool isValid = true;
            errorMessage = "";
            try
            {
                var csbMSSQL = new SqlConnectionStringBuilder(connectionString);
            }
            catch (Exception e)
            {
                isValid = false;
                errorMessage = e.Message;
            }
            return isValid;
        }
        #endregion
    }
}
