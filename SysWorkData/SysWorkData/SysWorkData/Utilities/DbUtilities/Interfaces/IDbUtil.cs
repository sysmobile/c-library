﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace SysWork.Data.Utilities
{
    public interface IDbUtil
    {
        //TODO: 20210901 Testear en todos los motores de base de datos
        bool ConnectionSuccess();
        bool ConnectionSuccess(string connectionString);
        bool ConnectionSuccess(out string errorMessage);
        bool ConnectionSuccess(string connectionString, out string errorMessage);

        //TODO: 20210901 Testear en todos los motores de base de datos
        bool ExecuteBatchNonQuery(string query);
        bool ExecuteBatchNonQuery(string ConnectionString, string query);
        bool ExecuteBatchNonQuery(IDbConnection dbConnection, string query);
        bool ExecuteBatchNonQuery(IDbTransaction dbTransaction, string query);

        //TODO: 20210901 Testear en todos los motores de base de datos
        bool ExistsColumn(string tableName, string columnName);
        bool ExistsColumn(string connectionString, string tableName, string columnName);
        bool ExistsColumn(DbConnection dbConnection, string tableName, string columnName);

        //TODO: 20210901 Testear en todos los motores de base de datos
        bool ExistsTable(string tableName);
        bool ExistsTable(string connectionString, string tableName);
        bool ExistsTable(DbConnection dbConnection, string tableName);

        //TODO: 20210901 Testear en todos los motores de base de datos
        bool ExistsView(string viewName);
        bool ExistsView(string connectionString, string viewName);
        bool ExistsView(DbConnection dbConnection, string viewName);

        //TODO: 20210901 Testear en todos los motores de base de datos
        DataTable GetColumnSchemaInformation(string tableName, string columnName);
        DataTable GetColumnSchemaInformation(string connectionString, string tableName, string columnName);
        DataTable GetColumnSchemaInformation(DbConnection dbConnection, string tableName, string columnName);

        //TODO: 20210901 Testear en todos los motores de base de datos
        List<string> GetTables();
        List<string> GetTables(string connectionString);
        List<string> GetTables(DbConnection dbConnection);

        //TODO: 20210901 Testear en todos los motores de base de datos
        DataTable GetTableSchemaInformation(string tableName);
        DataTable GetTableSchemaInformation(string connectionString, string tableName);
        DataTable GetTableSchemaInformation(DbConnection dbConnection, string tableName);

        //TODO: 20210901 Testear en todos los motores de base de datos
        List<string> GetViews();
        List<string> GetViews(string connectionString);
        List<string> GetViews(DbConnection dbConnection);

        //TODO: 20210901 Testear en todos los motores de base de datos
        DataTable GetViewSchemaInformation(string viewName);
        DataTable GetViewSchemaInformation(string connectionString, string viewName);
        DataTable GetViewSchemaInformation(DbConnection dbConnection, string viewName);

        //TODO: 20210901 Testear en todos los motores de base de datos
        bool IsValidConnectionString();
        bool IsValidConnectionString(string connectionString);
        bool IsValidConnectionString(out string errorMessage);
        bool IsValidConnectionString(string connectionString, out string errorMessage);
    }
}
