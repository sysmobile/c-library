﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using SysWork.Data.Common.ValueObjects;
using SysWork.Data.Utilities.MSSqlServer;
using SysWork.Data.Utilities.MySql;
using SysWork.Data.Utilities.OleDb;
using SysWork.Data.Utilities.SQLite;

namespace SysWork.Data.Utilities
{
    public class DbUtil: DbUtilBase
    {
        private readonly DbUtilBase _dbUtil;

        /// <summary>
        /// Initializes a new instance of the <see cref="DbUtil"/> class.
        /// </summary>
        /// <param name="databaseEngine">The database engine.</param>
        /// <param name="connectionString">The connection string.</param>
        /// <exception cref="ArgumentOutOfRangeException">The databaseEngine value is not supported by this method.</exception>
        public DbUtil(EDatabaseEngine databaseEngine, string connectionString) 
        {
            switch (databaseEngine)
            {
                case EDatabaseEngine.MSSqlServer:
                    _dbUtil = new DbUtilMSSqlServer(connectionString);
                    break;
                case EDatabaseEngine.SqLite:
                    _dbUtil = new DbUtilSQLite(connectionString);
                    break;
                case EDatabaseEngine.OleDb:
                    _dbUtil = new DbUtilOleDb(connectionString);
                    break;
                case EDatabaseEngine.MySql:
                    _dbUtil = new DbUtilMySql(connectionString);

                    break;
                default:
                    throw new ArgumentOutOfRangeException("The databaseEngine value is not supported by this method.");
            }
        }
        /// <summary>
        /// Connections the success.
        /// </summary>
        /// <returns></returns>
        public override bool ConnectionSuccess()
        {
            return _dbUtil.ConnectionSuccess();
        }
        /// <summary>
        /// Connections the success.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <returns></returns>
        public override bool ConnectionSuccess(string connectionString)
        {
            return _dbUtil.ConnectionSuccess(connectionString);
        }
        /// <summary>
        /// Connections the success.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        /// <returns></returns>
        public override bool ConnectionSuccess(out string errorMessage)
        {
            return _dbUtil.ConnectionSuccess(out errorMessage);
        }
        /// <summary>
        /// Connections the success.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <returns></returns>
        public override bool ConnectionSuccess(string connectionString, out string errorMessage)
        {
            return _dbUtil.ConnectionSuccess(connectionString, out errorMessage);
        }
        /// <summary>
        /// Executes the batch non query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns></returns>
        public override bool ExecuteBatchNonQuery(string query)
        {
            return _dbUtil.ExecuteBatchNonQuery(query);
        }
        /// <summary>
        /// Executes the batch non query.
        /// </summary>
        /// <param name="ConnectionString">The connection string.</param>
        /// <param name="query">The query.</param>
        /// <returns></returns>
        public override bool ExecuteBatchNonQuery(string ConnectionString, string query)
        {
            return _dbUtil.ExecuteBatchNonQuery(ConnectionString, query);
        }
        /// <summary>
        /// Executes the batch non query.
        /// </summary>
        /// <param name="dbConnection">The database connection.</param>
        /// <param name="query">The query.</param>
        /// <returns></returns>
        public override bool ExecuteBatchNonQuery(IDbConnection dbConnection, string query)
        {
            return _dbUtil.ExecuteBatchNonQuery(dbConnection, query);
        }

        public override bool ExecuteBatchNonQuery(IDbTransaction dbTransaction, string query)
        {
            return _dbUtil.ExecuteBatchNonQuery(dbTransaction, query);
        }

        /// <summary>
        /// Existses the column.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <returns></returns>
        public override bool ExistsColumn(string tableName, string columnName)
        {
            return _dbUtil.ExistsColumn(tableName, columnName);
        }
        /// <summary>
        /// Existses the column.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <returns></returns>
        public override bool ExistsColumn(string connectionString, string tableName, string columnName)
        {
            return _dbUtil.ExistsColumn(connectionString, tableName, columnName);
        }
        /// <summary>
        /// Existses the column.
        /// </summary>
        /// <param name="dbConnection">The database connection.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <returns></returns>
        public override bool ExistsColumn(DbConnection dbConnection, string tableName, string columnName)
        {
            return _dbUtil.ExistsColumn(dbConnection, tableName, columnName);
        }
        /// <summary>
        /// Existses the table.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <returns></returns>
        public override bool ExistsTable(string tableName)
        {
            return _dbUtil.ExistsTable(tableName);
        }
        /// <summary>
        /// Existses the table.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <returns></returns>
        public override bool ExistsTable(string connectionString, string tableName)
        {
            return _dbUtil.ExistsTable(connectionString, tableName);
        }
        /// <summary>
        /// Existses the table.
        /// </summary>
        /// <param name="dbConnection">The database connection.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <returns></returns>
        public override bool ExistsTable(DbConnection dbConnection, string tableName)
        {
            return _dbUtil.ExistsTable(dbConnection, tableName);
        }
        /// <summary>
        /// Existses the view.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <returns></returns>
        public override bool ExistsView(string viewName)
        {
            return _dbUtil.ExistsView(viewName);
        }
        /// <summary>
        /// Existses the view.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <returns></returns>
        public override bool ExistsView(string connectionString, string viewName)
        {
            return _dbUtil.ExistsView(connectionString, viewName);
        }
        /// <summary>
        /// Existses the view.
        /// </summary>
        /// <param name="dbConnection">The database connection.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <returns></returns>
        public override bool ExistsView(DbConnection dbConnection, string viewName)
        {
            return _dbUtil.ExistsView(dbConnection, viewName);
        }
        /// <summary>
        /// Gets the column schema information.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <returns></returns>
        public override DataTable GetColumnSchemaInformation(string tableName, string columnName)
        {
            return _dbUtil.GetColumnSchemaInformation(tableName, columnName);
        }
        /// <summary>
        /// Gets the column schema information.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <returns></returns>
        public override DataTable GetColumnSchemaInformation(string connectionString, string tableName, string columnName)
        {
            return _dbUtil.GetColumnSchemaInformation(connectionString, tableName, columnName);
        }
        /// <summary>
        /// Gets the column schema information.
        /// </summary>
        /// <param name="dbConnection">The database connection.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <returns></returns>
        public override DataTable GetColumnSchemaInformation(DbConnection dbConnection, string tableName, string columnName)
        {
            return _dbUtil.GetColumnSchemaInformation(dbConnection, tableName, columnName);
        }
        /// <summary>
        /// Gets the tables.
        /// </summary>
        /// <returns></returns>
        public override List<string> GetTables()
        {
            return _dbUtil.GetTables();
        }
        /// <summary>
        /// Gets the tables.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <returns></returns>
        public override List<string> GetTables(string connectionString)
        {
            return _dbUtil.GetTables(connectionString);
        }
        /// <summary>
        /// Gets the tables.
        /// </summary>
        /// <param name="dbConnection">The database connection.</param>
        /// <returns></returns>
        public override List<string> GetTables(DbConnection dbConnection)
        {
            return _dbUtil.GetTables(dbConnection);
        }
        /// <summary>
        /// Gets the table schema information.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <returns></returns>
        public override DataTable GetTableSchemaInformation(string tableName)
        {
            return _dbUtil.GetTableSchemaInformation(tableName);
        }
        /// <summary>
        /// Gets the table schema information.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <returns></returns>
        public override DataTable GetTableSchemaInformation(string connectionString, string tableName)
        {
            return _dbUtil.GetTableSchemaInformation(connectionString, tableName);
        }
        /// <summary>
        /// Gets the table schema information.
        /// </summary>
        /// <param name="dbConnection">The database connection.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <returns></returns>
        public override DataTable GetTableSchemaInformation(DbConnection dbConnection, string tableName)
        {
            return _dbUtil.GetTableSchemaInformation(dbConnection, tableName);
        }
        /// <summary>
        /// Gets the views.
        /// </summary>
        /// <returns></returns>
        public override List<string> GetViews()
        {
            return _dbUtil.GetViews();
        }
        /// <summary>
        /// Gets the views.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <returns></returns>
        public override List<string> GetViews(string connectionString)
        {
            return _dbUtil.GetViews(connectionString);
        }
        /// <summary>
        /// Gets the views.
        /// </summary>
        /// <param name="dbConnection">The database connection.</param>
        /// <returns></returns>
        public override List<string> GetViews(DbConnection dbConnection)
        {
            return _dbUtil.GetViews(dbConnection);
        }
        /// <summary>
        /// Gets the view schema information.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <returns></returns>
        public override DataTable GetViewSchemaInformation(string viewName)
        {
            return _dbUtil.GetViewSchemaInformation(viewName);
        }
        /// <summary>
        /// Gets the view schema information.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <returns></returns>
        public override DataTable GetViewSchemaInformation(string connectionString, string viewName)
        {
            return _dbUtil.GetViewSchemaInformation(connectionString, viewName);
        }
        /// <summary>
        /// Gets the view schema information.
        /// </summary>
        /// <param name="dbConnection">The database connection.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <returns></returns>
        public override DataTable GetViewSchemaInformation(DbConnection dbConnection, string viewName)
        {
            return _dbUtil.GetViewSchemaInformation(dbConnection, viewName);
        }
        /// <summary>
        /// Determines whether /[is valid connection string].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if [is valid connection string]; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsValidConnectionString()
        {
            return _dbUtil.IsValidConnectionString();
        }
        /// <summary>
        /// Determines whether /[is valid connection string] [the specified connection string].
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <returns>
        ///   <c>true</c> if [is valid connection string] [the specified connection string]; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsValidConnectionString(string connectionString)
        {
            return _dbUtil.IsValidConnectionString(connectionString);
        }
        /// <summary>
        /// Determines whether [is valid connection string] [the specified error message].
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        /// <returns>
        ///   <c>true</c> if [is valid connection string] [the specified error message]; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsValidConnectionString(out string errorMessage)
        {
            return _dbUtil.IsValidConnectionString(out errorMessage);
        }

        /// <summary>
        /// Determines whether [is valid connection string] [the specified connection string].
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <returns>
        ///   <c>true</c> if [is valid connection string] [the specified connection string]; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsValidConnectionString(string connectionString, out string errorMessage)
        {
            return _dbUtil.IsValidConnectionString(connectionString, out errorMessage);
        }
    }
}
