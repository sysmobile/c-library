﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Common;

namespace SysWork.Data.Utilities
{
    public abstract class DbUtilBase : IDbUtil
    {
        public abstract bool ConnectionSuccess();
        public abstract bool ConnectionSuccess(string connectionString);
        public abstract bool ConnectionSuccess(out string errorMessage);
        public abstract bool ConnectionSuccess(string connectionString, out string errorMessage);
        
        public abstract bool ExecuteBatchNonQuery(string query);
        public abstract bool ExecuteBatchNonQuery(string ConnectionString, string query);
        public abstract bool ExecuteBatchNonQuery(IDbConnection dbConnection, string query);
        public abstract bool ExecuteBatchNonQuery(IDbTransaction dbTransaction, string query);

        public abstract bool ExistsColumn(string tableName, string columnName);
        public abstract bool ExistsColumn(string connectionString, string tableName, string columnName);
        public abstract bool ExistsColumn(DbConnection dbConnection, string tableName, string columnName);
        
        public abstract bool ExistsTable(string tableName);
        public abstract bool ExistsTable(string connectionString, string tableName);
        public abstract bool ExistsTable(DbConnection dbConnection, string tableName);
        
        public abstract bool ExistsView(string viewName);
        public abstract bool ExistsView(string connectionString, string viewName);
        public abstract bool ExistsView(DbConnection dbConnection, string viewName);

        public abstract DataTable GetColumnSchemaInformation(string tableName, string columnName);
        public abstract DataTable GetColumnSchemaInformation(string connectionString, string tableName, string columnName);
        public abstract DataTable GetColumnSchemaInformation(DbConnection dbConnection, string tableName, string columnName);
        
        public abstract List<string> GetTables();
        public abstract List<string> GetTables(string connectionString);
        public abstract List<string> GetTables(DbConnection dbConnection);
        
        public abstract DataTable GetTableSchemaInformation(string tableName);
        public abstract DataTable GetTableSchemaInformation(string connectionString, string tableName);
        public abstract DataTable GetTableSchemaInformation(DbConnection dbConnection, string tableName);
        
        public abstract List<string> GetViews();
        public abstract List<string> GetViews(string connectionString);
        public abstract List<string> GetViews(DbConnection dbConnection);
        
        public abstract DataTable GetViewSchemaInformation(string viewName);
        public abstract DataTable GetViewSchemaInformation(string connectionString, string viewName);
        public abstract DataTable GetViewSchemaInformation(DbConnection dbConnection, string viewName);
        
        public abstract bool IsValidConnectionString();
        public abstract bool IsValidConnectionString(string connectionString);
        public abstract bool IsValidConnectionString(out string errorMessage);
        public abstract bool IsValidConnectionString(string connectionString, out string errorMessage);
        
        public bool ExistsConnectionStringInConfig(string connectionStringName)
        {
            return ExistsConnectionStringInConfig(connectionStringName, out _);
        }

        public bool ExistsConnectionStringInConfig(string connectionStringName, out string errorMessage)
        {
            errorMessage = "";
            try
            {
                var conectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                return false;
            }
            return true;
        }
        public DataTable ConvertToDatatable<T>(List<T> data)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]);
                else
                    table.Columns.Add(prop.Name, prop.PropertyType);
            }

            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                    values[i] = props[i].GetValue(item);

                table.Rows.Add(values);
            }
            return table;
        }

        public long ParseToLong(object result)
        {
            if (result.GetType() == typeof(System.Int64))
                return (long)result;
            if (result.GetType() == typeof(System.Int32))
                return (long)(Int32)result;
            else if (result.GetType() == typeof(System.Decimal))
                return (long)(Decimal)result;
            else
                return long.Parse(result.ToString());
        }

        public string ConvertCommandParamatersToLiteralValues(IDbCommand dbCommand)
        {
            string query;
            try
            {
                query = dbCommand.CommandText;

                foreach (IDataParameter prm in dbCommand.Parameters)
                {
                    switch (prm.DbType)
                    {
                        case DbType.Boolean:
                            int boolToInt = (bool)prm.Value ? 1 : 0;
                            query = query.Replace(prm.ParameterName, string.Format("{0}", (bool)prm.Value ? 1 : 0));
                            break;
                        case DbType.String:
                            if (prm.Value == DBNull.Value)
                                query = query.Replace(prm.ParameterName, string.Format("{0}", "NULL"));
                            else
                                query = query.Replace(prm.ParameterName, string.Format("'{0}'", prm.Value));
                            break;
                        case DbType.StringFixedLength:
                            if (prm.Value == DBNull.Value)
                                query = query.Replace(prm.ParameterName, string.Format("{0}", "NULL"));
                            else
                                query = query.Replace(prm.ParameterName, string.Format("'{0}'", prm.Value));
                            break;
                        case DbType.DateTime:
                            if (prm.Value == DBNull.Value)
                                query = query.Replace(prm.ParameterName, string.Format("{0}", "NULL"));
                            else
                                query = query.Replace(prm.ParameterName, string.Format("'{0}'", prm.Value));
                            break;
                        default:
                            query = query.Replace(prm.ParameterName, string.Format("{0}", prm.Value == DBNull.Value ? "NULL" : prm.Value));
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                query = "ERROR in convertCommandParamatersToLiteralValues : " + e.Message;
            }

            return query;
        }
    }
}
