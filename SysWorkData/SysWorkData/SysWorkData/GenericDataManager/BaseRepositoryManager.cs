﻿using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using SysWork.Data.Common.DataObjectProvider;
using SysWork.Data.Common.Utilities;
using SysWork.Data.Common.ValueObjects;

namespace SysWork.Data.GenericDataManager
{
    public class BaseRepositoryManager
    {
        public BaseRepositoryManager(EDatabaseEngine databaseEngine, string connectionString)
        {
            _databaseEngine = databaseEngine;
            _connectionString = connectionString;
            _objectProvider = new DbObjectProvider(_databaseEngine);
        }

        public BaseRepositoryManager(string connectionString): this(DefaultValues.DefaultDatabaseEngine,connectionString)
        {
          
        }

        public IDbConnection GetIDBconnection() => _objectProvider.GetIDbConnection();
        
        public DbConnection GetDBconnection() => _objectProvider.GetDbConnection();
        
        public IDbDataParameter GetIDbDataParameter() => _objectProvider.GetIDbDataParameter();
        
        public QueryExecutor GetQueryExecutor()
        {
            return new QueryExecutor(_connectionString, _databaseEngine);
        }

        public QueryExecutor GetQueryExecutor(DbConnection dbConnection)
        {
            return new QueryExecutor(dbConnection);
        }

        public QueryExecutor GetQueryExecutor(DbTransaction dbTransaction)
        {
            return new QueryExecutor(dbTransaction);
        }

        readonly DbObjectProvider _objectProvider;
        readonly EDatabaseEngine _databaseEngine;
        readonly string _connectionString;
    }
}
