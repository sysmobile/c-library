﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysWork.Data.GenericDataManager.CodeWriter
{
    public enum EDatamagerStyle
    {
        Singleton,
        SingletonPublicProperties,
        Instantiable
    }

}
