﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using SysWork.Data.Common.DataObjectProvider;
using SysWork.Data.Common.Filters;
using SysWork.Data.Common.Syntax;
using SysWork.Data.Common.ValueObjects;
using SysWork.Data.Common.Mapper;
using SysWork.Data.Common.Attributes.Helpers;
using SysWork.Data.Mapping;
using SysWork.Data.GenericRepository.Exceptions;
using SysWork.Data.GenericViewManager.Interfaces;

namespace SysWork.Data.GenericViewManager
{
    /// <summary>
    /// Generic Class to manage views
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    public partial class BaseViewManager<TEntity> : IViewManager<TEntity> where TEntity : class, new()
    {
        private string _connectionString;
        /// <summary>
        /// Gets the active ConnectionString.
        /// </summary>
        public string ConnectionString { get { return _connectionString; } private set { _connectionString = value; } }

        private EDatabaseEngine _databaseEngine;
        /// <summary>
        /// Gets the Database Engine.
        /// </summary>
        public EDatabaseEngine DatabaseEngine { get { return _databaseEngine; } private set { _databaseEngine = value; } }

        /// <summary>
        /// Gets the data object provider.
        /// </summary>
        /// <value>The DbObjectProvider used in this class.</value>
        private DbObjectProvider _dbObjectProvider;
        protected DbObjectProvider DbObjectProvider { get => _dbObjectProvider; private set => _dbObjectProvider = value; }

        private SyntaxProvider _syntaxProvider;

        /// <summary>
        /// Get the name of the database table to represent.
        /// </summary>
        public string ViewName { get; private set; }

        /// <summary>
        /// Get the propertyInfo of the columns.
        /// </summary>
        private IList<PropertyInfo> _entityProperties;
        public IList<PropertyInfo> EntityProperties { get => _entityProperties; private set => _entityProperties = value; }

        /// <summary>
        /// Get a list of the columns to perform a SELECT sentence on the represented table, separated by commas.
        /// </summary>
        public string ColumnsForSelect { get; private set; }

        private MapDataReaderToEntity _mapper;

        private int _defaultCommandTimeout = 30;
        /// <summary>
        /// Gets or sets the default CommandTimeOut.
        /// </summary>
        /// <value>
        /// The default CommandTimeOut.
        /// </value>
        protected int DefaultCommandTimeOut { get { return _defaultCommandTimeout; } set { _defaultCommandTimeout = value; } }



        /// <summary>
        /// Initializes a new instance class. Using MSSqlServer as DatabaseEngine.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public BaseViewManager(string connectionString)
        {
            BaseGenericViewManagerConstructorResolver(connectionString, DefaultValues.DefaultDatabaseEngine);
        }

        /// <summary>
        /// Initializes a new instance class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="databaseEngine">The data base engine.</param>
        public BaseViewManager(string connectionString, EDatabaseEngine databaseEngine)
        {
            BaseGenericViewManagerConstructorResolver(connectionString, databaseEngine);
        }
        private void BaseGenericViewManagerConstructorResolver(string connectionString, EDatabaseEngine databaseEngine)
        {
            _connectionString = connectionString;
            _databaseEngine = databaseEngine;
            _dbObjectProvider = new DbObjectProvider(_databaseEngine);
            _syntaxProvider = new SyntaxProvider(_databaseEngine);

            _mapper = new MapDataReaderToEntity();
            _mapper.UseTypeCache = false;

            TEntity entity = new TEntity();
            EntityProperties = ColumnHelper.GetProperties(entity);

            ViewName = GetViewNameFromEntity(entity.GetType());

            if ((EntityProperties == null) || (EntityProperties.Count == 0))
            {
                throw new Exception(string.Format("The Entity {0}, has not linked attibutes to table: {1}, Use [DbColumn] attribute to link properties to the table.", entity.GetType().Name, ViewName));
            }

            GetDbColumns();
        }
        /// <summary>
        /// Gets IDbDataParameter instantiated according to the databaseEngine.
        /// </summary>
        /// <returns>
        /// An IDbDataParameter instantiated according to the database engine.
        /// </returns>
        protected IDbDataParameter GetIDbDataParameter()
        {
            return DbObjectProvider.GetIDbDataParameter();
        }

        /// <summary>
        /// Creates an IDbDataParameter.
        /// </summary>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="value">The value.</param>
        /// <param name="size">The size.</param>
        /// <returns>
        /// An IDbDataParameter instantiated according to the database engine.
        /// </returns>
        protected IDbDataParameter CreateIDbDataParameter(string parameterName, object value, Int32? size = null)
        {
            IDbDataParameter dataParameter = GetIDbDataParameter();

            dataParameter.ParameterName = parameterName;
            dataParameter.Value = value ?? (Object)DBNull.Value;

            if (size != null)
                dataParameter.Size = (int)size;

            return dataParameter;
        }
        
        /// <summary>
        /// Gets IDBConnection instantiated according to the databaseEngine.
        /// </summary>
        /// <returns>
        /// A closed IDBConnection instantiated according to the database engine
        /// </returns>

        /// <summary>
        /// Gets the list by generic where filter.
        /// </summary>
        /// <param name="whereFilter">The where filter.</param>
        /// <returns></returns>
        private string GetViewNameFromEntity(Type type)
        {
            var DbView = type.GetCustomAttributes(false).OfType<ViewAttribute>().FirstOrDefault();
            if (DbView != null)
                return DbView.Name ?? type.Name;
            else
                throw new Exception(string.Format("The Entity {0}, has not linked to any view, Use [DbView] attribute to link it to a view.", type.Name));
        }

        private void GetDbColumns()
        {
            StringBuilder sbColumnsSelect = new StringBuilder();

            foreach (PropertyInfo i in EntityProperties)
            {
                var customAttribute = i.GetCustomAttribute(typeof(ColumnAttribute)) as ColumnAttribute;
                string columnName = _syntaxProvider.GetSecureColumnName(customAttribute.Name ?? i.Name);

                sbColumnsSelect.Append(string.Format("{0},", columnName));
            }

            if (sbColumnsSelect.Length > 0)
                sbColumnsSelect.Remove(sbColumnsSelect.Length - 1, 1);

            ColumnsForSelect = sbColumnsSelect.ToString();
        }

        /// <summary>
        /// Gets the generic where filter.
        /// </summary>
        /// <returns></returns>
        public GenericWhereFilter GetGenericWhereFilter()
        {
            GenericWhereFilter whereFilter = new GenericWhereFilter();
            whereFilter.SetColumnsForSelect<TEntity>();
            whereFilter.SetTableOrViewName<TEntity>();

            return whereFilter;
        }

        protected IDbConnection BaseIDbConnection()
        {
            return DbObjectProvider.GetIDbConnection(_connectionString);
        }
        
        protected DbConnection BaseDbConnection()
        {
            return DbObjectProvider.GetDbConnection(_connectionString);
        }

        public IEntityTable<TEntity> View()
        {
            return View(null, null);
        }

        public IEntityTable<TEntity> View(IDbTransaction dbTransaction)
        {
            return View(null, dbTransaction);
        }

        public IEntityTable<TEntity> View(IDbConnection dbConnection)
        {
            return View(dbConnection, null);
        }

        internal DataTable ConvertToDatatable(IList<TEntity> list)
        {
            DataTable table = new DataTable();
            foreach (PropertyInfo i in EntityProperties)
                table.Columns.Add(i.Name, i.PropertyType);

            object[] values = new object[EntityProperties.Count];
            foreach (TEntity item in list)
            {
                for (int i = 0; i < values.Length; i++)
                    values[i] = EntityProperties[i].GetValue(item);

                table.Rows.Add(values);
            }
            return table;
        }

        public IEntityTable<TEntity> View(IDbConnection dbConnection, IDbTransaction dbTransaction)
        {
            IEntityTable<TEntity> result;

            if (dbConnection == null && dbTransaction != null)
                dbConnection = dbTransaction.Connection;

            IDbConnection dbConnectionInUse = dbConnection ?? PersistentIDbConnection;

            try
            {
                if (dbConnectionInUse.State != ConnectionState.Open)
                    dbConnectionInUse.Open();

                DbEntityProvider entityProvider = _dbObjectProvider.GetQueryProvider((DbConnection)dbConnectionInUse);
                if (dbTransaction != null)
                {
                    entityProvider.Transaction = (DbTransaction)dbTransaction;
                    entityProvider.Isolation = dbTransaction.IsolationLevel;
                }

                result = entityProvider.GetTable<TEntity>();
            }
            catch (Exception e)
            {
                throw new RepositoryException(e);
            }

            return result;
        }

        private IDbConnection _persistentIDBConnection = null;
        internal IDbConnection PersistentIDbConnection { get { return GetPersistentIDbConnection(); } }

        internal IDbConnection GetPersistentIDbConnection()
        {
            if ((_persistentIDBConnection == null) || (_persistentIDBConnection.State != ConnectionState.Open))
            {
                _persistentIDBConnection = BaseIDbConnection();
                _persistentIDBConnection.Open();
            }

            return _persistentIDBConnection;
        }



    }
}
