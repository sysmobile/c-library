﻿using System;
using System.Data;
using System.Linq.Expressions;
using SysWork.Data.GenericRepository.Exceptions;

namespace SysWork.Data.GenericViewManager
{
    public partial class BaseViewManager<TEntity> 
    {

        public DataTable GetDataTableWhere(Expression<Func<TEntity, bool>> predicate)
        {
            return GetDataTableWhere(predicate, null, (IDbTransaction)null, null);
        }

        public DataTable GetDataTableWhere(Expression<Func<TEntity, bool>> predicate, int commandTimeOut)
        {
            return GetDataTableWhere(predicate, null, (IDbTransaction)null, commandTimeOut);
        }

        public DataTable GetDataTableWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection)
        {
            return GetDataTableWhere(predicate, dbConnection, null, null);
        }

        public DataTable GetDataTableWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection, int commandTimeOut)
        {
            return GetDataTableWhere(predicate, dbConnection, null, commandTimeOut);
        }

        public DataTable GetDataTableWhere(Expression<Func<TEntity, bool>> predicate, IDbTransaction dbTransaction)
        {
            return GetDataTableWhere(predicate, null, dbTransaction, null);
        }

        public DataTable GetDataTableWhere(Expression<Func<TEntity, bool>> predicate, IDbTransaction dbTransaction, int commandTimeOut)
        {
            return GetDataTableWhere(predicate, null, dbTransaction, commandTimeOut);
        }

        public DataTable GetDataTableWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection, IDbTransaction dbTransaction)
        {
            return GetDataTableWhere(predicate, dbConnection, dbTransaction, null);
        }

        public DataTable GetDataTableWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection, IDbTransaction dbTransaction, int? commandTimeOut)
        {
            DataTable result;
            try
            {
                result = ConvertToDatatable(GetListWhere(predicate, dbConnection, dbTransaction, commandTimeOut));

            }
            catch (Exception e)
            {
                throw new RepositoryException(e);
            }
            
            return result;
        }

        #region DEPRECATED CODE
        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetDataTableWhere")]
        public DataTable GetDataTableByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter)
        {
            return GetDataTableWhere(filter, null, (IDbTransaction)null, null);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetDataTableWhere")]
        public DataTable GetDataTableByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, int commandTimeOut)
        {
            return GetDataTableWhere(filter, null, (IDbTransaction)null, commandTimeOut);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetDataTableWhere")]
        public DataTable GetDataTableByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, IDbConnection dbConnection)
        {
            return GetDataTableWhere(filter, dbConnection, null, null);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetDataTableWhere")]
        public DataTable GetDataTableByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, IDbConnection dbConnection, int commandTimeOut)
        {
            return GetDataTableWhere(filter, dbConnection, null, commandTimeOut);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetDataTableWhere")]
        public DataTable GetDataTableByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, IDbTransaction dbTransaction)
        {
            return GetDataTableWhere(filter, null, dbTransaction, null);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetDataTableWhere")]
        public DataTable GetDataTableByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, IDbTransaction dbTransaction, int commandTimeOut)
        {
            return GetDataTableWhere(filter, null, dbTransaction, commandTimeOut);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetDataTableWhere")]
        public DataTable GetDataTableByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, IDbConnection dbConnection, IDbTransaction dbTransaction)
        {
            return GetDataTableWhere(filter, dbConnection, dbTransaction, null);
        }
        #endregion

    }
}
