﻿using SysWork.Data.Common.Interfaces.Actions;

namespace SysWork.Data.GenericViewManager.Interfaces
{
    public interface IViewManager<TEntity>:
        IFind<TEntity>,
        IFindAsync<TEntity>,
        IGetAll<TEntity>,
        IGetAllAsync<TEntity>,
        IGetByGenericWhereFilter<TEntity>,
        IGetByGenericWhereFilterAsync<TEntity>,
        IGetDataTableWhere<TEntity>,
        IGetDataTableWhereAsync<TEntity>,
        IGetListByGenericWhereFilter<TEntity>,
        IGetListByGenericWhereFilterAsync<TEntity>,
        IGetListWhere<TEntity>,
        IGetListWhereAsync<TEntity>,
        IGetWhere<TEntity>,
        IGetWhereAsync<TEntity>{}
}
