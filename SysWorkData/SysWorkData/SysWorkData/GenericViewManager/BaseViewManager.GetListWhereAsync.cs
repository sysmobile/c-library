﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using SysWork.Data.GenericRepository.Exceptions;
using System.Threading.Tasks;
using System.Data.Common;
using System.Linq;
//using System.Data.Entity;

namespace SysWork.Data.GenericViewManager
{
    public partial class BaseViewManager<TEntity> 
    {
        public async Task<IList<TEntity>> GetListWhereAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await GetListWhereAsync(predicate, null, null, null);
        }

        public async Task<IList<TEntity>> GetListWhereAsync(Expression<Func<TEntity, bool>> predicate, int commandTimeOut)
        {
            return await GetListWhereAsync(predicate, null, null, commandTimeOut);
        }

        public async Task<IList<TEntity>> GetListWhereAsync(Expression<Func<TEntity, bool>> predicate, DbConnection dbConnection)
        {
            return await GetListWhereAsync(predicate, dbConnection, null, null);
        }

        public async Task<IList<TEntity>> GetListWhereAsync(Expression<Func<TEntity, bool>> predicate, DbConnection dbConnection, int commandTimeOut)
        {
            return await GetListWhereAsync(predicate, dbConnection, null, commandTimeOut);
        }

        public async Task<IList<TEntity>> GetListWhereAsync(Expression<Func<TEntity, bool>> predicate, DbTransaction dbTransaction)
        {
            return await GetListWhereAsync(predicate, null, dbTransaction, null);
        }

        public async Task<IList<TEntity>> GetListWhereAsync(Expression<Func<TEntity, bool>> predicate, DbTransaction dbTransaction, int commandTimeOut)
        {
            return await GetListWhereAsync(predicate, null, dbTransaction, commandTimeOut);
        }

        public async Task<IList<TEntity>> GetListWhereAsync(Expression<Func<TEntity, bool>> predicate, DbConnection dbConnection, DbTransaction dbTransaction)
        {
            return await GetListWhereAsync(predicate, dbConnection, dbTransaction, null);
        }

        public async Task<IList<TEntity>> GetListWhereAsync(Expression<Func<TEntity, bool>> predicate, DbConnection dbConnection, DbTransaction dbTransaction, int? commandTimeOut)
        {
            List<TEntity> result = new List<TEntity>();
            bool closeConnection = ((dbConnection == null) && (dbTransaction == null));

            if (dbConnection == null && dbTransaction != null)
                dbConnection = dbTransaction.Connection;

            DbConnection dbConnectionInUse = dbConnection ?? BaseDbConnection();
            try
            {
                if (dbConnectionInUse.State != ConnectionState.Open)
                    await dbConnectionInUse.OpenAsync();

                DbEntityProvider entityProvider = _dbObjectProvider.GetQueryProvider((DbConnection)dbConnectionInUse);
                if (dbTransaction != null)
                {
                    entityProvider.Transaction = (DbTransaction)dbTransaction;
                    entityProvider.Isolation = dbTransaction.IsolationLevel;
                }

                var table = entityProvider.GetTable<TEntity>();
                result = await Task.FromResult(table.Where(predicate).ToList());

            }
            catch (Exception exception)
            {
                throw new RepositoryException(exception);
            }
            finally
            {
                if ((dbConnectionInUse != null) && (dbConnectionInUse.State == ConnectionState.Open) && (closeConnection))
                {
                    dbConnectionInUse.Close();
                    dbConnectionInUse.Dispose();
                }
            }
            return result;
        }

        #region DEPRECATED CODE
        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetListWhereAsync")]
        public async Task<IList<TEntity>> GetListByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter)
        {
            return await GetListWhereAsync(filter, null, null, null);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetListWhereAsync")]
        public async Task<IList<TEntity>> GetListByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, int commandTimeOut)
        {
            return await GetListWhereAsync(filter, null, null, commandTimeOut);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetListWhereAsync")]
        public async Task<IList<TEntity>> GetListByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, DbConnection dbConnection)
        {
            return await GetListWhereAsync(filter, dbConnection, null, null);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetListWhereAsync")]
        public async Task<IList<TEntity>> GetListByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, DbConnection dbConnection, int commandTimeOut)
        {
            return await GetListWhereAsync(filter, dbConnection, null, commandTimeOut);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetListWhereAsync")]
        public async Task<IList<TEntity>> GetListByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, DbTransaction dbTransaction)
        {
            return await GetListWhereAsync(filter, null, dbTransaction, null);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetListWhereAsync")]
        public async Task<IList<TEntity>> GetListByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, DbTransaction dbTransaction, int commandTimeOut)
        {
            return await GetListWhereAsync(filter, null, dbTransaction, commandTimeOut);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetListWhereAsync")]
        public async Task<IList<TEntity>> GetListByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, DbConnection dbConnection, DbTransaction dbTransaction)
        {
            return await GetListWhereAsync(filter, dbConnection, dbTransaction, null);
        }

        #endregion
    }
}
