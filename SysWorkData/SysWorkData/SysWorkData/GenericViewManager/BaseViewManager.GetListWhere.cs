﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using SysWork.Data.GenericRepository.Exceptions;

namespace SysWork.Data.GenericViewManager
{
    public partial class BaseViewManager<TEntity> 
    {
        public IList<TEntity> GetListWhere(Expression<Func<TEntity, bool>> predicate)
        {
            return GetListWhere(predicate, null, null, null);
        }

        public IList<TEntity> GetListWhere(Expression<Func<TEntity, bool>> predicate, int commandTimeOut)
        {
            return GetListWhere(predicate, null, null, commandTimeOut);
        }

        public IList<TEntity> GetListWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection)
        {
            return GetListWhere(predicate, dbConnection, null, null);
        }

        public IList<TEntity> GetListWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection, int commandTimeOut)
        {
            return GetListWhere(predicate, dbConnection, null, commandTimeOut);
        }

        public IList<TEntity> GetListWhere(Expression<Func<TEntity, bool>> predicate, IDbTransaction dbTransaction)
        {
            return GetListWhere(predicate, null, dbTransaction, null);
        }

        public IList<TEntity> GetListWhere(Expression<Func<TEntity, bool>> predicate, IDbTransaction dbTransaction, int commandTimeOut)
        {
            return GetListWhere(predicate, null, dbTransaction, commandTimeOut);
        }

        public IList<TEntity> GetListWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection, IDbTransaction dbTransaction)
        {
            return GetListWhere(predicate, dbConnection, dbTransaction, null);
        }

        public IList<TEntity> GetListWhere(Expression<Func<TEntity, bool>> filter, IDbConnection dbConnection, IDbTransaction dbTransaction, int? commandTimeOut)
        {
            IList<TEntity> result = new List<TEntity>();
            bool closeConnection = ((dbConnection == null) && (dbTransaction == null));

            if (dbConnection == null && dbTransaction != null)
                dbConnection = dbTransaction.Connection;

            IDbConnection dbConnectionInUse = dbConnection ?? BaseIDbConnection();
            try
            {
                if (dbConnectionInUse.State != ConnectionState.Open)
                    dbConnectionInUse.Open();

                DbEntityProvider entityProvider = DbObjectProvider.GetQueryProvider((DbConnection)dbConnectionInUse);
                if (dbTransaction != null)
                {
                    entityProvider.Transaction = (DbTransaction)dbTransaction;
                    entityProvider.Isolation = dbTransaction.IsolationLevel;
                }

                var table = entityProvider.GetTable<TEntity>();
                result = table.Where(filter).ToList();

            }
            catch (Exception exception)
            {
                throw new RepositoryException(exception);
            }
            finally
            {
                if ((dbConnectionInUse != null) && (dbConnectionInUse.State == ConnectionState.Open) && (closeConnection))
                {
                    dbConnectionInUse.Close();
                    dbConnectionInUse.Dispose();
                }
            }
            return result;
        }

        #region DEPRECATED CODE
        public IList<TEntity> GetListByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter)
        {
            return GetListWhere(filter, null, null, null);
        }

        public IList<TEntity> GetListByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, int commandTimeOut)
        {
            return GetListWhere(filter, null, null, commandTimeOut);
        }

        public IList<TEntity> GetListByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, IDbConnection dbConnection)
        {
            return GetListWhere(filter, dbConnection, null, null);
        }

        public IList<TEntity> GetListByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, IDbConnection dbConnection, int commandTimeOut)
        {
            return GetListWhere(filter, dbConnection, null, commandTimeOut);
        }

        public IList<TEntity> GetListByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, IDbTransaction dbTransaction)
        {
            return GetListWhere(filter, null, dbTransaction, null);
        }

        public IList<TEntity> GetListByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, IDbTransaction dbTransaction, int commandTimeOut)
        {
            return GetListWhere(filter, null, dbTransaction, commandTimeOut);
        }

        public IList<TEntity> GetListByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, IDbConnection dbConnection, IDbTransaction dbTransaction)
        {
            return GetListWhere(filter, dbConnection, dbTransaction, null);
        }

        #endregion
    }
}
