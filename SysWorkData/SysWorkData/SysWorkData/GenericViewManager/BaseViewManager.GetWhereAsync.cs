﻿using System;
using System.Data;
using System.Linq.Expressions;
using SysWork.Data.GenericRepository.Exceptions;
using System.Threading.Tasks;
using System.Data.Common;
using System.Linq;

namespace SysWork.Data.GenericViewManager
{
    public partial class BaseViewManager<TEntity> 
    {
        public async Task<TEntity> GetWhereAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await GetWhereAsync(predicate, null, null, null);
        }

        public async Task<TEntity> GetWhereAsync(Expression<Func<TEntity, bool>> predicate, int commandTimeOut)
        {
            return await GetWhereAsync(predicate, null, null, commandTimeOut);
        }

        public async Task<TEntity> GetWhereAsync(Expression<Func<TEntity, bool>> predicate, DbConnection dbConnection)
        {
            return await GetWhereAsync(predicate, dbConnection, null, null);
        }

        public async Task<TEntity> GetWhereAsync(Expression<Func<TEntity, bool>> predicate, DbConnection dbConnection, int commandTimeOut)
        {
            return await GetWhereAsync(predicate, dbConnection, null, commandTimeOut);
        }

        public async Task<TEntity> GetWhereAsync(Expression<Func<TEntity, bool>> predicate, DbTransaction dbTransaction)
        {
            return await GetWhereAsync(predicate, null, dbTransaction, null);
        }

        public async Task<TEntity> GetWhereAsync(Expression<Func<TEntity, bool>> predicate, DbTransaction dbTransaction, int commandTimeOut)
        {
            return await GetWhereAsync(predicate, null, dbTransaction, commandTimeOut);
        }

        public async Task<TEntity> GetWhereAsync(Expression<Func<TEntity, bool>> predicate, DbConnection dbConnection, DbTransaction dbTransaction)
        {
            return await GetWhereAsync(predicate, dbConnection, dbTransaction, null);
        }

        public async Task<TEntity> GetWhereAsync(Expression<Func<TEntity, bool>> predicate, DbConnection dbConnection, DbTransaction dbTransaction, int? commandTimeOut)
        {
            TEntity result = null;
            bool closeConnection = ((dbConnection == null) && (dbTransaction == null));

            if (dbConnection == null && dbTransaction != null)
                dbConnection = dbTransaction.Connection;

            DbConnection dbConnectionInUse = dbConnection ?? BaseDbConnection();

            try
            {
                if (dbConnectionInUse.State != ConnectionState.Open)
                    await dbConnectionInUse.OpenAsync();

                DbEntityProvider entityProvider = _dbObjectProvider.GetQueryProvider((DbConnection)dbConnectionInUse);
                if (dbTransaction != null)
                {
                    entityProvider.Transaction = (DbTransaction)dbTransaction;
                    entityProvider.Isolation = dbTransaction.IsolationLevel;
                }

                var table = entityProvider.GetTable<TEntity>();
                result = await Task.FromResult(table.Where(predicate).FirstOrDefault());
            }
            catch (Exception e)
            {
                throw new RepositoryException(e);
            }
            finally 
            {
                if ((dbConnectionInUse != null) && (dbConnectionInUse.State == ConnectionState.Open) && (closeConnection))
                {
                    dbConnectionInUse.Close();
                    dbConnectionInUse.Dispose();
                }
            }
            return result ;
        }

        #region DEPRECATED CODE
        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetWhereAsync")]
        public async Task<TEntity> GetByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter)
        {
            return await GetWhereAsync(filter, null, null, null);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetWhereAsync")]
        public async Task<TEntity> GetByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, int commandTimeOut)
        {
            return await GetWhereAsync(filter, null, null, commandTimeOut);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetWhereAsync")]
        public async Task<TEntity> GetByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, DbConnection dbConnection)
        {
            return await GetWhereAsync(filter, dbConnection, null, null);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetWhereAsync")]
        public async Task<TEntity> GetByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, DbConnection dbConnection, int commandTimeOut)
        {
            return await GetWhereAsync(filter, dbConnection, null, commandTimeOut);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetWhereAsync")]
        public async Task<TEntity> GetByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, DbTransaction dbTransaction)
        {
            return await GetWhereAsync(filter, null, dbTransaction, null);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetWhereAsync")]
        public async Task<TEntity> GetByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, DbTransaction dbTransaction, int commandTimeOut)
        {
            return await GetWhereAsync(filter, null, dbTransaction, commandTimeOut);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetWhereAsync")]
        public async Task<TEntity> GetByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, DbConnection dbConnection, DbTransaction dbTransaction)
        {
            return await GetWhereAsync(filter, dbConnection, dbTransaction, null);
        }

        #endregion

    }
}