﻿using System;
using System.Data;
using System.Linq.Expressions;
using System.Data.Common;
using SysWork.Data.GenericRepository.Exceptions;

namespace SysWork.Data.GenericRepository
{
    public partial class BaseRepository<TEntity> 
    {
        public long DeleteWhere(Expression<Func<TEntity, bool>> predicate)
        {
            return DeleteWhere(predicate,null,null,null);
        }
        public long DeleteWhere(Expression<Func<TEntity, bool>> predicate, int commandTimeOut)
        {
            return DeleteWhere(predicate, null, null, commandTimeOut);
        }

        public long DeleteWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection)
        {
            return DeleteWhere(predicate, dbConnection,null , null);
        }

        public long DeleteWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection, int commandTimeOut)
        {
            return DeleteWhere(predicate, dbConnection, null, commandTimeOut);
        }

        public long DeleteWhere(Expression<Func<TEntity, bool>> predicate, IDbTransaction dbTransaction)
        {
            return DeleteWhere(predicate, null, dbTransaction, null);
        }

        public long DeleteWhere(Expression<Func<TEntity, bool>> predicate, IDbTransaction dbTransaction, int commandTimeOut)
        {
            return DeleteWhere(predicate, null, dbTransaction, commandTimeOut);
        }

        public long DeleteWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection, IDbTransaction dbTransaction)
        {
            return DeleteWhere(predicate, dbConnection, dbTransaction, null);
        }

        public long DeleteWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection, IDbTransaction dbTransaction, int? commandTimeOut)
        {
            bool closeConnection = ((dbConnection == null) && (dbTransaction == null));
            long result;

            if (dbConnection == null && dbTransaction != null)
                dbConnection = dbTransaction.Connection;

            IDbConnection dbConnectionInUse = dbConnection ?? BaseIDbConnection();

            try
            {
                if (dbConnectionInUse.State != ConnectionState.Open)
                    dbConnectionInUse.Open();

                DbEntityProvider entityProvider = _dbObjectProvider.GetQueryProvider((DbConnection)dbConnectionInUse);
                if (dbTransaction != null)
                {
                    entityProvider.Transaction = (DbTransaction)dbTransaction;
                    entityProvider.Isolation = dbTransaction.IsolationLevel;
                }

                var table = entityProvider.GetTable<TEntity>();
                result = table.Delete<TEntity>(predicate);

            }
            catch (Exception e)
            {
                throw new RepositoryException(e);
            }
            finally 
            {
                if ((dbConnectionInUse != null) && (dbConnectionInUse.State == ConnectionState.Open) && (closeConnection))
                {
                    dbConnectionInUse.Close();
                    dbConnectionInUse.Dispose();
                }
            }
            return result;
        }

        #region DEPRECATED CODE
        [Obsolete("This method is obsolete, and will be deprecated soon, please use DeleteWhere")]
        public long DeleteByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter)
        {
            return DeleteWhere(filter, null, null, null);
        }
        
        [Obsolete("This method is obsolete, and will be deprecated soon, please use DeleteWhere")]
        public long DeleteByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, int commandTimeOut)
        {
            return DeleteWhere(filter, null, null, commandTimeOut);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use DeleteWhere")]
        public long DeleteByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, IDbConnection dbConnection)
        {
            return DeleteWhere(filter, dbConnection, null, null);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use DeleteWhere")]
        public long DeleteByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, IDbConnection dbConnection, int commandTimeOut)
        {
            return DeleteWhere(filter, dbConnection, null, commandTimeOut);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use DeleteWhere")]
        public long DeleteByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, IDbTransaction dbTransaction)
        {
            return DeleteWhere(filter, null, dbTransaction, null);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use DeleteWhere")]
        public long DeleteByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, IDbTransaction dbTransaction, int commandTimeOut)
        {
            return DeleteWhere(filter, null, dbTransaction, commandTimeOut);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use DeleteWhere")]
        public long DeleteByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, IDbConnection dbConnection, IDbTransaction dbTransaction)
        {
            return DeleteWhere(filter, dbConnection, dbTransaction, null);
        }

        #endregion
    }
}
