﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq.Expressions;
using SysWork.Data.GenericRepository.Exceptions;
using System.Threading.Tasks;

namespace SysWork.Data.GenericRepository
{
    public partial class BaseRepository<TEntity> 
    {

        public async Task<DataTable> GetDataTableWhereAsync(Expression<Func<TEntity, bool>> filter)
        {
            return await GetDataTableWhereAsync(filter, null, null, null);
        }

        public async Task<DataTable> GetDataTableWhereAsync(Expression<Func<TEntity, bool>> filter, int commandTimeOut)
        {
            return await GetDataTableWhereAsync(filter, null, null, commandTimeOut);
        }

        public async Task<DataTable> GetDataTableWhereAsync(Expression<Func<TEntity, bool>> filter, DbConnection dbConnection)
        {
            return await GetDataTableWhereAsync(filter, dbConnection, null, null);
        }

        public async Task<DataTable> GetDataTableWhereAsync(Expression<Func<TEntity, bool>> filter, DbConnection dbConnection, int commandTimeOut)
        {
            return await GetDataTableWhereAsync(filter, dbConnection, null, commandTimeOut);
        }

        public async Task<DataTable> GetDataTableWhereAsync(Expression<Func<TEntity, bool>> filter, DbTransaction dbTransaction)
        {
            return await GetDataTableWhereAsync(filter, null, dbTransaction, null);
        }

        public async Task<DataTable> GetDataTableWhereAsync(Expression<Func<TEntity, bool>> filter, DbTransaction dbTransaction, int commandTimeOut)
        {
            return await GetDataTableWhereAsync(filter, null, dbTransaction, commandTimeOut);
        }

        public async Task<DataTable> GetDataTableWhereAsync(Expression<Func<TEntity, bool>> filter, DbConnection dbConnection, DbTransaction dbTransaction)
        {
            return await GetDataTableWhereAsync(filter, dbConnection, dbTransaction, null);
        }

        public async Task<DataTable> GetDataTableWhereAsync(Expression<Func<TEntity, bool>> predicate, DbConnection dbConnection, DbTransaction dbTransaction, int? commandTimeOut)
        {
            DataTable result;
            try
            {
                result = ConvertToDatatable(await GetListWhereAsync(predicate, dbConnection, dbTransaction, commandTimeOut));

            }
            catch (Exception e)
            {
                throw new RepositoryException(e);
            }

            return result;
        }

        #region DEPRECATED CODE
        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetDataTableWhereAsync")]
        public async Task<DataTable> GetDataTableByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter)
        {
            return await GetDataTableWhereAsync(filter, null, null, null);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetDataTableWhereAsync")]
        public async Task<DataTable> GetDataTableByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, int commandTimeOut)
        {
            return await GetDataTableWhereAsync(filter, null, null, commandTimeOut);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetDataTableWhereAsync")]
        public async Task<DataTable> GetDataTableByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, DbConnection dbConnection)
        {
            return await GetDataTableWhereAsync(filter, dbConnection, null, null);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetDataTableWhereAsync")]
        public async Task<DataTable> GetDataTableByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, DbConnection dbConnection, int commandTimeOut)
        {
            return await GetDataTableWhereAsync(filter, dbConnection, null, commandTimeOut);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetDataTableWhereAsync")]
        public async Task<DataTable> GetDataTableByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, DbTransaction dbTransaction)
        {
            return await GetDataTableWhereAsync(filter, null, dbTransaction, null);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetDataTableWhereAsync")]
        public async Task<DataTable> GetDataTableByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, DbTransaction dbTransaction, int commandTimeOut)
        {
            return await GetDataTableWhereAsync(filter, null, dbTransaction, commandTimeOut);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetDataTableWhereAsync")]
        public async Task<DataTable> GetDataTableByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, DbConnection dbConnection, DbTransaction dbTransaction)
        {
            return await GetDataTableWhereAsync(filter, dbConnection, dbTransaction, null);
        }

        #endregion
    }
}
