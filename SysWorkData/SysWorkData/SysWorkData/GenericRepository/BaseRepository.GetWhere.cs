﻿using System;
using System.Data;
using System.Linq.Expressions;
using System.Linq;
using System.Data.Common;
using SysWork.Data.GenericRepository.Exceptions;

namespace SysWork.Data.GenericRepository
{
    public partial class BaseRepository<TEntity> 
    {
        public TEntity GetWhere(Expression<Func<TEntity, bool>> predicate)
        {
            return GetWhere(predicate, null, null, null);
        }

        public TEntity GetWhere(Expression<Func<TEntity, bool>> predicate, int commandTimeOut)
        {
            return GetWhere(predicate, null, null, commandTimeOut);
        }

        public TEntity GetWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection)
        {
            return GetWhere(predicate, dbConnection, null, null);
        }

        public TEntity GetWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection, int commandTimeOut)
        {
            return GetWhere(predicate, dbConnection, null, commandTimeOut);
        }

        public TEntity GetWhere(Expression<Func<TEntity, bool>> predicate, IDbTransaction dbTransaction)
        {
            return GetWhere(predicate, null, dbTransaction, null);
        }

        public TEntity GetWhere(Expression<Func<TEntity, bool>> predicate, IDbTransaction dbTransaction, int commandTimeOut)
        {
            return GetWhere(predicate, null, dbTransaction, commandTimeOut);
        }

        public TEntity GetWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection, IDbTransaction dbTransaction)
        {
            return GetWhere(predicate, dbConnection, dbTransaction, null);
        }
        
        public TEntity GetWhere(Expression<Func<TEntity, bool>> predicate, IDbConnection dbConnection, IDbTransaction dbTransaction, int? commandTimeOut)
        {
            TEntity result = null;
            bool closeConnection = ((dbConnection == null) && (dbTransaction == null));

            if (dbConnection == null && dbTransaction != null)
                dbConnection = dbTransaction.Connection;

            IDbConnection dbConnectionInUse = dbConnection ?? BaseIDbConnection();

            try
            {
                if (dbConnectionInUse.State != ConnectionState.Open)
                    dbConnectionInUse.Open();

                DbEntityProvider entityProvider = _dbObjectProvider.GetQueryProvider((DbConnection)dbConnectionInUse);
                if (dbTransaction != null)
                {
                    entityProvider.Transaction = (DbTransaction)dbTransaction;
                    entityProvider.Isolation = dbTransaction.IsolationLevel;
                }

                var table = entityProvider.GetTable<TEntity>();
                result = table.Where(predicate).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new RepositoryException(e);
            }
            finally 
            {
                if ((dbConnectionInUse != null) && (dbConnectionInUse.State == ConnectionState.Open) && (closeConnection))
                {
                    dbConnectionInUse.Close();
                    dbConnectionInUse.Dispose();
                }
            }
            return result;
        }

        #region DEPRECATED CODE
        //  TO DELETE
        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetWhere")]
        public TEntity GetByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter)
        {
            return GetWhere(filter, null, null, null);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetWhere")]
        public TEntity GetByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, int commandTimeOut)
        {
            return GetWhere(filter, null, null, commandTimeOut);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetWhere")]
        public TEntity GetByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, IDbConnection dbConnection)
        {
            return GetWhere(filter, dbConnection, null, null);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetWhere")]
        public TEntity GetByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, IDbConnection dbConnection, int commandTimeOut)
        {
            return GetWhere(filter, dbConnection, null, commandTimeOut);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetWhere")]
        public TEntity GetByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, IDbTransaction dbTransaction)
        {
            return GetWhere(filter, null, dbTransaction, null);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetWhere")]
        public TEntity GetByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, IDbTransaction dbTransaction, int commandTimeOut)
        {
            return GetWhere(filter, null, dbTransaction, commandTimeOut);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use GetWhere")]
        public TEntity GetByLambdaExpressionFilter(Expression<Func<TEntity, bool>> filter, IDbConnection dbConnection, IDbTransaction dbTransaction)
        {
            return GetWhere(filter, dbConnection, dbTransaction, null);
        }

        //

        #endregion

    }
}