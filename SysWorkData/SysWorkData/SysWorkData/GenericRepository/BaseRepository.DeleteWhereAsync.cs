﻿using System;
using System.Data;
using System.Linq.Expressions;
using SysWork.Data.GenericRepository.Exceptions;
using System.Threading.Tasks;
using System.Data.Common;

namespace SysWork.Data.GenericRepository
{
    public partial class BaseRepository<TEntity> 
    {
        public async Task<long> DeleteWhereAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await DeleteWhereAsync(predicate,null,null,null);
        }
        public async Task<long> DeleteWhereAsync(Expression<Func<TEntity, bool>> predicate, int commandTimeOut)
        {
            return await DeleteWhereAsync(predicate, null, null, commandTimeOut);
        }

        public async Task<long> DeleteWhereAsync(Expression<Func<TEntity, bool>> predicate, DbConnection dbConnection)
        {
            return await DeleteWhereAsync(predicate, dbConnection,null , null);
        }

        public async Task<long> DeleteWhereAsync(Expression<Func<TEntity, bool>> predicate, DbConnection dbConnection, int commandTimeOut)
        {
            return await DeleteWhereAsync(predicate, dbConnection, null, commandTimeOut);
        }

        public async Task<long> DeleteWhereAsync(Expression<Func<TEntity, bool>> predicate, DbTransaction dbTransaction)
        {
            return await DeleteWhereAsync(predicate, null, dbTransaction, null);
        }

        public async Task<long> DeleteWhereAsync(Expression<Func<TEntity, bool>> predicate, DbTransaction dbTransaction, int commandTimeOut)
        {
            return await DeleteWhereAsync(predicate, null, dbTransaction, commandTimeOut);
        }

        public async Task<long> DeleteWhereAsync(Expression<Func<TEntity, bool>> predicate, DbConnection dbConnection, DbTransaction dbTransaction)
        {
            return await DeleteWhereAsync(predicate, dbConnection, dbTransaction, null);
        }

        public async Task<long> DeleteWhereAsync(Expression<Func<TEntity, bool>> predicate, DbConnection dbConnection, DbTransaction dbTransaction, int? commandTimeOut)
        {
            long recordsAffected = 0;
            bool closeConnection = ((dbConnection == null) && (dbTransaction == null));

            if (dbConnection == null && dbTransaction != null)
                dbConnection = dbTransaction.Connection;

            DbConnection dbConnectionInUse = dbConnection ?? BaseDbConnection();
            try
            {
                if (dbConnectionInUse.State != ConnectionState.Open)
                    await dbConnectionInUse.OpenAsync();

                DbEntityProvider entityProvider = _dbObjectProvider.GetQueryProvider((DbConnection)dbConnectionInUse);
                if (dbTransaction != null)
                {
                    entityProvider.Transaction = (DbTransaction)dbTransaction;
                    entityProvider.Isolation = dbTransaction.IsolationLevel;
                }

                var table = entityProvider.GetTable<TEntity>();
                recordsAffected = table.Delete<TEntity>(predicate);

            }
            catch (Exception e)
            {
                throw new RepositoryException(e);
            }
            finally
            {
                if ((dbConnectionInUse != null) && (dbConnectionInUse.State == ConnectionState.Open) && (closeConnection))
                {
                    dbConnectionInUse.Close();
                    dbConnectionInUse.Dispose();
                }
            }
            return recordsAffected;
        }

        #region DEPRECATED CODE
        [Obsolete("This method is obsolete, and will be deprecated soon, please use DeleteWhereAsync")]
        public async Task<long> DeleteByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter)
        {
            return await DeleteWhereAsync(filter, null, null, null);
        }
        
        [Obsolete("This method is obsolete, and will be deprecated soon, please use DeleteWhereAsync")]
        public async Task<long> DeleteByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, int commandTimeOut)
        {
            return await DeleteWhereAsync(filter, null, null, commandTimeOut);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use DeleteWhereAsync")]
        public async Task<long> DeleteByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, DbConnection dbConnection)
        {
            return await DeleteWhereAsync(filter, dbConnection, null, null);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use DeleteWhereAsync")]
        public async Task<long> DeleteByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, DbConnection dbConnection, int commandTimeOut)
        {
            return await DeleteWhereAsync(filter, dbConnection, null, commandTimeOut);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use DeleteWhereAsync")]
        public async Task<long> DeleteByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, DbTransaction dbTransaction)
        {
            return await DeleteWhereAsync(filter, null, dbTransaction, null);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use DeleteWhereAsync")]
        public async Task<long> DeleteByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, DbTransaction dbTransaction, int commandTimeOut)
        {
            return await DeleteWhereAsync(filter, null, dbTransaction, commandTimeOut);
        }

        [Obsolete("This method is obsolete, and will be deprecated soon, please use DeleteWhereAsync")]
        public async Task<long> DeleteByLambdaExpressionFilterAsync(Expression<Func<TEntity, bool>> filter, DbConnection dbConnection, DbTransaction dbTransaction)
        {
            return await DeleteWhereAsync(filter, dbConnection, dbTransaction, null);
        }

        #endregion

    }
}
