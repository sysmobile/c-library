﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysWork.Data.Mapping
{
    /// <summary>
    /// An attribute used to define information to help map between CLR types/members and database tables/columns.
    /// </summary>
    public abstract class MappingAttribute : Attribute
    {
    }

}
