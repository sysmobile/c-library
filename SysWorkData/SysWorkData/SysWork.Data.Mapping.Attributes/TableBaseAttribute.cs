﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysWork.Data.Mapping
{
    public abstract class TableBaseAttribute : MappingAttribute
    {
        /// <summary>
        /// The name of the table in the database. 
        /// If not specified, the table's name will be the name of the member or type the attribute is placed on.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The ID to use for this table in advanced multi-table mapping.
        /// If not specified, the <see cref="Id"/> will be the table's name.
        /// </summary>
        public string Id { get; set; }
    }
}
