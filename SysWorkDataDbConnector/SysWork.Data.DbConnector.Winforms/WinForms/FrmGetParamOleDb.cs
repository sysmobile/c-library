﻿using System;
using System.Windows.Forms;
using SysWork.Data.Common.Utilities;
using SysWork.Data.Common.ValueObjects;
using SysWork.Data.DbConnector.Forms.Interfaces;
using SysWork.Data.Utilities;

namespace SysWork.Data.Common.WinForms.GetParameters
{
    /// <summary>
    /// Form to Get Data Parameters for OleDb.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    internal partial class FrmGetParamOleDb : Form, IFormGetDBParameters
    {
        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>
        /// The connection string.
        /// </value>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>
        /// The error message.
        /// </value>
        public string ErrMessage { get; set; }
        public string Server { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string Login { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string Password { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string DataBase { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public EConnectorParameterTypeUsed ParameterTypeUsed { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrmGetParamOleDb"/> class.
        /// </summary>
        public FrmGetParamOleDb()
        {
            InitializeComponent();
        }

        private void FrmGetParamOleDb_Load(object sender, EventArgs e)
        {
            txtConnectionString.Text = ConnectionString;

            lblErrMessage.Text = ErrMessage;
            lblErrMessage.Refresh();
            if (!string.IsNullOrEmpty(ErrMessage))
            {
                this.Height += lblErrMessage.Height;
            }
            else
            {
                this.Height -= lblErrMessage.Height;
            }

            this.CenterToScreen();

        }

        private void BtnAceptar_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty( txtConnectionString.Text))
            {
                MessageBox.Show("Se debe informar la cadena de conexion!!!", "Aviso al operador", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            if (!new DbUtil(EDatabaseEngine.OleDb, txtConnectionString.Text).IsValidConnectionString(out string errMessage))
            {
                MessageBox.Show($"Error en el formato de la cadena de conexion \r\n \r\n detalle del error: \r\n \r\n {errMessage} ", "Aviso al operador", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            ConnectionString = txtConnectionString.Text;
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
