﻿namespace SysWork.Data.DbConnector.MetroForms.GetParameters
{
    partial class MetroFormGetParamMySQL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.rbtnParametrosManuales = new MetroFramework.Controls.MetroRadioButton();
            this.rbtnConnectionString = new MetroFramework.Controls.MetroRadioButton();
            this.grpManualParameters = new MetroFramework.Controls.MetroGroupBox();
            this.txtDataBase = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.txtPassword = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txtLogin = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtServer = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroStyleManager1 = new MetroFramework.Components.MetroStyleManager(this.components);
            this.grpConnectionString = new MetroFramework.Controls.MetroGroupBox();
            this.txtConnectionString = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.btnOk = new MetroFramework.Controls.MetroButton();
            this.btnCancel = new MetroFramework.Controls.MetroButton();
            this.metroStyleExtender1 = new MetroFramework.Components.MetroStyleExtender(this.components);
            this.metroRendererManager1 = new MetroFramework.Components.MetroRendererManager(this.components);
            this.lblErrMessage = new MetroFramework.Controls.MetroLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.grpManualParameters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).BeginInit();
            this.grpConnectionString.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // rbtnParametrosManuales
            // 
            this.rbtnParametrosManuales.AutoSize = true;
            this.rbtnParametrosManuales.Location = new System.Drawing.Point(20, 105);
            this.rbtnParametrosManuales.Name = "rbtnParametrosManuales";
            this.rbtnParametrosManuales.Size = new System.Drawing.Size(137, 15);
            this.rbtnParametrosManuales.TabIndex = 0;
            this.rbtnParametrosManuales.Text = "Parametros &Manuales";
            this.rbtnParametrosManuales.UseSelectable = true;
            this.rbtnParametrosManuales.CheckedChanged += new System.EventHandler(this.RbtnParametrosManuales_CheckedChanged);
            // 
            // rbtnConnectionString
            // 
            this.rbtnConnectionString.AutoSize = true;
            this.rbtnConnectionString.Location = new System.Drawing.Point(23, 268);
            this.rbtnConnectionString.Name = "rbtnConnectionString";
            this.rbtnConnectionString.Size = new System.Drawing.Size(115, 15);
            this.rbtnConnectionString.TabIndex = 5;
            this.rbtnConnectionString.Text = "Connection&string";
            this.rbtnConnectionString.UseSelectable = true;
            this.rbtnConnectionString.CheckedChanged += new System.EventHandler(this.RbtnConnectionString_CheckedChanged);
            // 
            // grpManualParameters
            // 
            this.grpManualParameters.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.grpManualParameters.BorderStyle = MetroFramework.Controls.MetroGroupBox.BorderMode.Full;
            this.grpManualParameters.Controls.Add(this.txtDataBase);
            this.grpManualParameters.Controls.Add(this.metroLabel4);
            this.grpManualParameters.Controls.Add(this.txtPassword);
            this.grpManualParameters.Controls.Add(this.metroLabel3);
            this.grpManualParameters.Controls.Add(this.txtLogin);
            this.grpManualParameters.Controls.Add(this.metroLabel2);
            this.grpManualParameters.Controls.Add(this.txtServer);
            this.grpManualParameters.Controls.Add(this.metroLabel1);
            this.grpManualParameters.DrawBottomLine = false;
            this.grpManualParameters.DrawShadows = false;
            this.grpManualParameters.Font = new System.Drawing.Font("Segoe UI Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.grpManualParameters.FontSize = MetroFramework.MetroLabelSize.Medium;
            this.grpManualParameters.FontWeight = MetroFramework.MetroLabelWeight.Light;
            this.grpManualParameters.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.grpManualParameters.Location = new System.Drawing.Point(20, 126);
            this.grpManualParameters.Name = "grpManualParameters";
            this.grpManualParameters.PaintDefault = false;
            this.grpManualParameters.Size = new System.Drawing.Size(573, 136);
            this.grpManualParameters.Style = MetroFramework.MetroColorStyle.Blue;
            this.grpManualParameters.StyleManager = this.metroStyleManager1;
            this.grpManualParameters.TabIndex = 14;
            this.grpManualParameters.TabStop = false;
            this.grpManualParameters.Text = "Datos de Conexion";
            this.grpManualParameters.Theme = MetroFramework.MetroThemeStyle.Light;
            this.grpManualParameters.UseStyleColors = false;
            // 
            // txtDataBase
            // 
            // 
            // 
            // 
            this.txtDataBase.CustomButton.Image = null;
            this.txtDataBase.CustomButton.Location = new System.Drawing.Point(417, 2);
            this.txtDataBase.CustomButton.Name = "";
            this.txtDataBase.CustomButton.Size = new System.Drawing.Size(15, 15);
            this.txtDataBase.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtDataBase.CustomButton.TabIndex = 1;
            this.txtDataBase.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtDataBase.CustomButton.UseSelectable = true;
            this.txtDataBase.CustomButton.Visible = false;
            this.txtDataBase.Lines = new string[0];
            this.txtDataBase.Location = new System.Drawing.Point(124, 100);
            this.txtDataBase.MaxLength = 32767;
            this.txtDataBase.Name = "txtDataBase";
            this.txtDataBase.PasswordChar = '\0';
            //this.txtDataBase.PromptText = "test";
            this.txtDataBase.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDataBase.SelectedText = "";
            this.txtDataBase.SelectionLength = 0;
            this.txtDataBase.SelectionStart = 0;
            this.txtDataBase.ShortcutsEnabled = true;
            this.txtDataBase.Size = new System.Drawing.Size(435, 20);
            this.txtDataBase.TabIndex = 4;
            this.txtDataBase.UseSelectable = true;
            this.txtDataBase.WaterMark = "test";
            this.txtDataBase.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtDataBase.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(6, 100);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(92, 19);
            this.metroLabel4.TabIndex = 6;
            this.metroLabel4.Text = "Base de Datos";
            // 
            // txtPassword
            // 
            // 
            // 
            // 
            this.txtPassword.CustomButton.Image = null;
            this.txtPassword.CustomButton.Location = new System.Drawing.Point(417, 2);
            this.txtPassword.CustomButton.Name = "";
            this.txtPassword.CustomButton.Size = new System.Drawing.Size(15, 15);
            this.txtPassword.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPassword.CustomButton.TabIndex = 1;
            this.txtPassword.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPassword.CustomButton.UseSelectable = true;
            this.txtPassword.CustomButton.Visible = false;
            this.txtPassword.Lines = new string[0];
            this.txtPassword.Location = new System.Drawing.Point(124, 74);
            this.txtPassword.MaxLength = 32767;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            //this.txtPassword.PromptText = "root";
            this.txtPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPassword.SelectedText = "";
            this.txtPassword.SelectionLength = 0;
            this.txtPassword.SelectionStart = 0;
            this.txtPassword.ShortcutsEnabled = true;
            this.txtPassword.Size = new System.Drawing.Size(435, 20);
            this.txtPassword.TabIndex = 3;
            this.txtPassword.UseSelectable = true;
            this.txtPassword.WaterMark = "root";
            this.txtPassword.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPassword.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(6, 74);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(63, 19);
            this.metroLabel3.TabIndex = 4;
            this.metroLabel3.Text = "Password";
            // 
            // txtLogin
            // 
            // 
            // 
            // 
            this.txtLogin.CustomButton.Image = null;
            this.txtLogin.CustomButton.Location = new System.Drawing.Point(417, 2);
            this.txtLogin.CustomButton.Name = "";
            this.txtLogin.CustomButton.Size = new System.Drawing.Size(15, 15);
            this.txtLogin.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtLogin.CustomButton.TabIndex = 1;
            this.txtLogin.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtLogin.CustomButton.UseSelectable = true;
            this.txtLogin.CustomButton.Visible = false;
            this.txtLogin.Lines = new string[0];
            this.txtLogin.Location = new System.Drawing.Point(124, 48);
            this.txtLogin.MaxLength = 32767;
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.PasswordChar = '\0';
            //this.txtLogin.PromptText = "root";
            this.txtLogin.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLogin.SelectedText = "";
            this.txtLogin.SelectionLength = 0;
            this.txtLogin.SelectionStart = 0;
            this.txtLogin.ShortcutsEnabled = true;
            this.txtLogin.Size = new System.Drawing.Size(435, 20);
            this.txtLogin.TabIndex = 2;
            this.txtLogin.UseSelectable = true;
            this.txtLogin.WaterMark = "root";
            this.txtLogin.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtLogin.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(6, 48);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(99, 19);
            this.metroLabel2.TabIndex = 2;
            this.metroLabel2.Text = "Inicio de Sesion";
            // 
            // txtServer
            // 
            // 
            // 
            // 
            this.txtServer.CustomButton.Image = null;
            this.txtServer.CustomButton.Location = new System.Drawing.Point(417, 2);
            this.txtServer.CustomButton.Name = "";
            this.txtServer.CustomButton.Size = new System.Drawing.Size(15, 15);
            this.txtServer.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtServer.CustomButton.TabIndex = 1;
            this.txtServer.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtServer.CustomButton.UseSelectable = true;
            this.txtServer.CustomButton.Visible = false;
            this.txtServer.Lines = new string[0];
            this.txtServer.Location = new System.Drawing.Point(124, 22);
            this.txtServer.MaxLength = 32767;
            this.txtServer.Name = "txtServer";
            this.txtServer.PasswordChar = '\0';
            //this.txtServer.PromptText = "localhost";
            this.txtServer.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtServer.SelectedText = "";
            this.txtServer.SelectionLength = 0;
            this.txtServer.SelectionStart = 0;
            this.txtServer.ShortcutsEnabled = true;
            this.txtServer.Size = new System.Drawing.Size(435, 20);
            this.txtServer.TabIndex = 1;
            this.txtServer.UseSelectable = true;
            this.txtServer.WaterMark = "localhost";
            this.txtServer.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtServer.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(6, 22);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(59, 19);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Servidor";
            // 
            // metroStyleManager1
            // 
            this.metroStyleManager1.Owner = this;
            // 
            // grpConnectionString
            // 
            this.grpConnectionString.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.grpConnectionString.BorderStyle = MetroFramework.Controls.MetroGroupBox.BorderMode.Full;
            this.grpConnectionString.Controls.Add(this.txtConnectionString);
            this.grpConnectionString.Controls.Add(this.metroLabel5);
            this.grpConnectionString.DrawBottomLine = false;
            this.grpConnectionString.DrawShadows = false;
            this.grpConnectionString.Font = new System.Drawing.Font("Segoe UI Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.grpConnectionString.FontSize = MetroFramework.MetroLabelSize.Medium;
            this.grpConnectionString.FontWeight = MetroFramework.MetroLabelWeight.Light;
            this.grpConnectionString.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.grpConnectionString.Location = new System.Drawing.Point(20, 289);
            this.grpConnectionString.Name = "grpConnectionString";
            this.grpConnectionString.PaintDefault = false;
            this.grpConnectionString.Size = new System.Drawing.Size(573, 115);
            this.grpConnectionString.Style = MetroFramework.MetroColorStyle.Blue;
            this.grpConnectionString.StyleManager = this.metroStyleManager1;
            this.grpConnectionString.TabIndex = 15;
            this.grpConnectionString.TabStop = false;
            this.grpConnectionString.Text = "Datos de Conexion";
            this.grpConnectionString.Theme = MetroFramework.MetroThemeStyle.Light;
            this.grpConnectionString.UseStyleColors = false;
            // 
            // txtConnectionString
            // 
            // 
            // 
            // 
            this.txtConnectionString.CustomButton.Image = null;
            this.txtConnectionString.CustomButton.Location = new System.Drawing.Point(351, 1);
            this.txtConnectionString.CustomButton.Name = "";
            this.txtConnectionString.CustomButton.Size = new System.Drawing.Size(77, 77);
            this.txtConnectionString.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtConnectionString.CustomButton.TabIndex = 1;
            this.txtConnectionString.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtConnectionString.CustomButton.UseSelectable = true;
            this.txtConnectionString.CustomButton.Visible = false;
            this.txtConnectionString.Lines = new string[0];
            this.txtConnectionString.Location = new System.Drawing.Point(124, 22);
            this.txtConnectionString.MaxLength = 32767;
            this.txtConnectionString.Multiline = true;
            this.txtConnectionString.Name = "txtConnectionString";
            this.txtConnectionString.PasswordChar = '\0';
            //this.txtConnectionString.PromptText = "Server=localhost;\rPort=1234;\rDatabase=test;\rUid=root;\rPassword=root;";
            this.txtConnectionString.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtConnectionString.SelectedText = "";
            this.txtConnectionString.SelectionLength = 0;
            this.txtConnectionString.SelectionStart = 0;
            this.txtConnectionString.ShortcutsEnabled = true;
            this.txtConnectionString.Size = new System.Drawing.Size(429, 79);
            this.txtConnectionString.TabIndex = 6;
            this.txtConnectionString.UseSelectable = true;
            this.txtConnectionString.WaterMark = "Server=localhost;\rPort=1234;\rDatabase=test;\rUid=root;\rPassword=root;";
            this.txtConnectionString.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtConnectionString.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(6, 22);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(107, 19);
            this.metroLabel5.TabIndex = 0;
            this.metroLabel5.Text = "Connectionstring";
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOk.Location = new System.Drawing.Point(161, 435);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(121, 23);
            this.btnOk.TabIndex = 7;
            this.btnOk.Text = "&Aceptar";
            this.btnOk.UseSelectable = true;
            this.btnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(335, 435);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(121, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "&Cancelar";
            this.btnCancel.UseSelectable = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // metroRendererManager1
            // 
            this.metroRendererManager1.Renderers = MetroFramework.Components.Renderer.MetroRenderer;
            this.metroRendererManager1.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroRendererManager1.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // lblErrMessage
            // 
            this.lblErrMessage.AutoSize = true;
            this.lblErrMessage.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblErrMessage.Location = new System.Drawing.Point(23, 407);
            this.lblErrMessage.MaximumSize = new System.Drawing.Size(560, 0);
            this.lblErrMessage.MinimumSize = new System.Drawing.Size(560, 0);
            this.lblErrMessage.Name = "lblErrMessage";
            this.lblErrMessage.Size = new System.Drawing.Size(156, 15);
            this.lblErrMessage.TabIndex = 16;
            this.lblErrMessage.Text = "Ha ocurrido el siguiente error:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::SysWork.Data.DbConnector.MetroForms.Properties.Resources.connector_icon_mysql;
            this.pictureBox1.Location = new System.Drawing.Point(468, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(125, 135);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // MetroFormGetParamMySQL
            // 
            this.AcceptButton = this.btnCancel;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(610, 481);
            this.ControlBox = false;
            this.Controls.Add(this.lblErrMessage);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.grpConnectionString);
            this.Controls.Add(this.grpManualParameters);
            this.Controls.Add(this.rbtnConnectionString);
            this.Controls.Add(this.rbtnParametrosManuales);
            this.Controls.Add(this.pictureBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MetroFormGetParamMySQL";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Parametros de Conexion a MySQL";
            this.Theme = MetroFramework.MetroThemeStyle.Default;
            this.Load += new System.EventHandler(this.MetroFormGetParamMSSQL_Load);
            this.grpManualParameters.ResumeLayout(false);
            this.grpManualParameters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).EndInit();
            this.grpConnectionString.ResumeLayout(false);
            this.grpConnectionString.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroRadioButton rbtnParametrosManuales;
        private MetroFramework.Controls.MetroRadioButton rbtnConnectionString;
        private MetroFramework.Controls.MetroGroupBox grpManualParameters;
        private MetroFramework.Controls.MetroTextBox txtDataBase;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox txtPassword;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox txtLogin;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtServer;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroGroupBox grpConnectionString;
        private MetroFramework.Controls.MetroTextBox txtConnectionString;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroButton btnOk;
        private MetroFramework.Controls.MetroButton btnCancel;
        private MetroFramework.Components.MetroStyleManager metroStyleManager1;
        private MetroFramework.Components.MetroStyleExtender metroStyleExtender1;
        private MetroFramework.Components.MetroRendererManager metroRendererManager1;
        private MetroFramework.Controls.MetroLabel lblErrMessage;
    }
}