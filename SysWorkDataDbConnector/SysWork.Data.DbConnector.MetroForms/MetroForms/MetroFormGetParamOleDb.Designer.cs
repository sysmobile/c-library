﻿namespace SysWork.Data.DbConnector.MetroForms.GetParameters
{
    partial class MetroFormGetParamOleDb
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grpConnectionString = new MetroFramework.Controls.MetroGroupBox();
            this.BtnBrowseConnection = new MetroFramework.Controls.MetroButton();
            this.txtConnectionString = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroStyleManager1 = new MetroFramework.Components.MetroStyleManager(this.components);
            this.btnOk = new MetroFramework.Controls.MetroButton();
            this.btnCancel = new MetroFramework.Controls.MetroButton();
            this.metroStyleExtender1 = new MetroFramework.Components.MetroStyleExtender(this.components);
            this.metroRendererManager1 = new MetroFramework.Components.MetroRendererManager(this.components);
            this.lblErrMessage = new MetroFramework.Controls.MetroLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.metroToolTip1 = new MetroFramework.Components.MetroToolTip();
            this.grpConnectionString.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // grpConnectionString
            // 
            this.grpConnectionString.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.grpConnectionString.BorderStyle = MetroFramework.Controls.MetroGroupBox.BorderMode.Full;
            this.grpConnectionString.Controls.Add(this.BtnBrowseConnection);
            this.grpConnectionString.Controls.Add(this.txtConnectionString);
            this.grpConnectionString.Controls.Add(this.metroLabel5);
            this.grpConnectionString.DrawBottomLine = false;
            this.grpConnectionString.DrawShadows = false;
            this.grpConnectionString.Font = new System.Drawing.Font("Segoe UI Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.grpConnectionString.FontSize = MetroFramework.MetroLabelSize.Medium;
            this.grpConnectionString.FontWeight = MetroFramework.MetroLabelWeight.Light;
            this.grpConnectionString.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.grpConnectionString.Location = new System.Drawing.Point(14, 131);
            this.grpConnectionString.Name = "grpConnectionString";
            this.grpConnectionString.PaintDefault = false;
            this.grpConnectionString.Size = new System.Drawing.Size(579, 129);
            this.grpConnectionString.Style = MetroFramework.MetroColorStyle.Blue;
            this.grpConnectionString.StyleManager = this.metroStyleManager1;
            this.grpConnectionString.TabIndex = 15;
            this.grpConnectionString.TabStop = false;
            this.grpConnectionString.Text = "Datos de Conexion";
            this.grpConnectionString.Theme = MetroFramework.MetroThemeStyle.Light;
            this.grpConnectionString.UseStyleColors = false;
            // 
            // BtnBrowseConnection
            // 
            this.BtnBrowseConnection.Location = new System.Drawing.Point(553, 22);
            this.BtnBrowseConnection.Name = "BtnBrowseConnection";
            this.BtnBrowseConnection.Size = new System.Drawing.Size(20, 18);
            this.BtnBrowseConnection.TabIndex = 7;
            this.BtnBrowseConnection.Text = "...";
            this.metroToolTip1.SetToolTip(this.BtnBrowseConnection, "Generar Cadena de Conexion");
            this.BtnBrowseConnection.UseSelectable = true;
            this.BtnBrowseConnection.Click += new System.EventHandler(this.BtnBrowseConnection_Click);
            // 
            // txtConnectionString
            // 
            // 
            // 
            // 
            this.txtConnectionString.CustomButton.Image = null;
            this.txtConnectionString.CustomButton.Location = new System.Drawing.Point(332, 1);
            this.txtConnectionString.CustomButton.Name = "";
            this.txtConnectionString.CustomButton.Size = new System.Drawing.Size(91, 91);
            this.txtConnectionString.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtConnectionString.CustomButton.TabIndex = 1;
            this.txtConnectionString.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtConnectionString.CustomButton.UseSelectable = true;
            this.txtConnectionString.CustomButton.Visible = false;
            this.txtConnectionString.Lines = new string[0];
            this.txtConnectionString.Location = new System.Drawing.Point(124, 22);
            this.txtConnectionString.MaxLength = 32767;
            this.txtConnectionString.Multiline = true;
            this.txtConnectionString.Name = "txtConnectionString";
            this.txtConnectionString.PasswordChar = '\0';
            this.txtConnectionString.PromptText = "Provider=Microsoft.ACE.OLEDB.12.0;\rData Source=C:\\data\\miBaseDeDatos.mdb;\rPersist" +
    " Security Info=False;";
            this.txtConnectionString.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtConnectionString.SelectedText = "";
            this.txtConnectionString.SelectionLength = 0;
            this.txtConnectionString.SelectionStart = 0;
            this.txtConnectionString.ShortcutsEnabled = true;
            this.txtConnectionString.Size = new System.Drawing.Size(424, 93);
            this.txtConnectionString.TabIndex = 6;
            this.txtConnectionString.UseSelectable = true;
            this.txtConnectionString.WaterMark = "Provider=Microsoft.ACE.OLEDB.12.0;\rData Source=C:\\data\\miBaseDeDatos.mdb;\rPersist" +
    " Security Info=False;";
            this.txtConnectionString.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtConnectionString.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(6, 22);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(107, 19);
            this.metroLabel5.TabIndex = 0;
            this.metroLabel5.Text = "Connectionstring";
            // 
            // metroStyleManager1
            // 
            this.metroStyleManager1.Owner = this;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOk.Location = new System.Drawing.Point(186, 289);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(121, 23);
            this.btnOk.TabIndex = 7;
            this.btnOk.Text = "&Aceptar";
            this.btnOk.UseSelectable = true;
            this.btnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(360, 289);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(121, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "&Cancelar";
            this.btnCancel.UseSelectable = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // metroRendererManager1
            // 
            this.metroRendererManager1.Renderers = MetroFramework.Components.Renderer.MetroRenderer;
            this.metroRendererManager1.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroRendererManager1.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // lblErrMessage
            // 
            this.lblErrMessage.AutoSize = true;
            this.lblErrMessage.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblErrMessage.Location = new System.Drawing.Point(14, 263);
            this.lblErrMessage.MaximumSize = new System.Drawing.Size(560, 0);
            this.lblErrMessage.MinimumSize = new System.Drawing.Size(560, 0);
            this.lblErrMessage.Name = "lblErrMessage";
            this.lblErrMessage.Size = new System.Drawing.Size(156, 15);
            this.lblErrMessage.TabIndex = 16;
            this.lblErrMessage.Text = "Ha ocurrido el siguiente error:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::SysWork.Data.DbConnector.MetroForms.Properties.Resources.connector_icon_oledb;
            this.pictureBox1.Location = new System.Drawing.Point(468, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(125, 135);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // metroToolTip1
            // 
            this.metroToolTip1.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroToolTip1.StyleManager = null;
            this.metroToolTip1.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // MetroFormGetParamOleDb
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(610, 335);
            this.ControlBox = false;
            this.Controls.Add(this.lblErrMessage);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.grpConnectionString);
            this.Controls.Add(this.pictureBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MetroFormGetParamOleDb";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Parametros de Conexion a OleDb";
            this.Theme = MetroFramework.MetroThemeStyle.Default;
            this.Load += new System.EventHandler(this.MetroFrmGetParamOleDb_Load);
            this.grpConnectionString.ResumeLayout(false);
            this.grpConnectionString.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroGroupBox grpConnectionString;
        private MetroFramework.Controls.MetroTextBox txtConnectionString;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroButton btnOk;
        private MetroFramework.Controls.MetroButton btnCancel;
        private MetroFramework.Components.MetroStyleManager metroStyleManager1;
        private MetroFramework.Components.MetroStyleExtender metroStyleExtender1;
        private MetroFramework.Components.MetroRendererManager metroRendererManager1;
        private MetroFramework.Controls.MetroLabel lblErrMessage;
        private MetroFramework.Controls.MetroButton BtnBrowseConnection;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private MetroFramework.Components.MetroToolTip metroToolTip1;
    }
}