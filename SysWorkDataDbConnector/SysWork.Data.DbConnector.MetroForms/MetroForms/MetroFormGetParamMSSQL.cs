﻿using MetroFramework;
using MetroFramework.Forms;
using System.Windows.Forms;
using SysWork.Data.Common.ValueObjects;
using SysWork.Data.DbConnector.Forms.Interfaces;
using SysWork.Data.Utilities;

namespace SysWork.Data.DbConnector.MetroForms.GetParameters
{
    public partial class MetroFormGetParamMSSQL : MetroForm,IFormGetDBParameters
    {
        /// <summary>
        /// Gets or sets the server.
        /// </summary>
        /// <value>
        /// The server.
        /// </value>
        public string Server { get; set; }

        /// <summary>
        /// Gets or sets the login.
        /// </summary>
        /// <value>
        /// The login.
        /// </value>
        public string Login { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the dataBase.
        /// </summary>
        /// <value>
        /// The dataBase.
        /// </value>
        public string DataBase { get; set; }

        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>
        /// The connection string.
        /// </value>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>
        /// The error message.
        /// </value>
        public string ErrMessage { get; set; }

        /// <summary>
        /// Gets or sets the parameter type used.
        /// </summary>
        /// <value>
        /// The parameter type used.
        /// </value>
        public EConnectorParameterTypeUsed ParameterTypeUsed { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrmGetParamSQL"/> class.
        /// </summary>

        readonly MetroThemeStyle _theme;
        readonly MetroColorStyle _style;

        public MetroFormGetParamMSSQL(MetroThemeStyle theme, MetroColorStyle style)
        {
            _theme = theme;
            _style = style;

            InitializeComponent();
            
            metroStyleManager1.Theme = _theme;
            metroStyleManager1.Style = _style;

            this.Theme = _theme;
            this.Style = _style;
            
            metroStyleManager1.Update();

            this.Refresh();

        }

        private void MetroFrmGetParamMSSQL_Load(object sender, System.EventArgs e)
        {
            txtServer.Text = Server;
            txtLogin.Text = Login;
            txtPassword.Text = Password;
            txtDataBase.Text = DataBase;

            txtConnectionString.Text = ConnectionString;

            rbtnConnectionString.Checked = ParameterTypeUsed == EConnectorParameterTypeUsed.ConnectionString;
            rbtnParametrosManuales.Checked = !rbtnConnectionString.Checked;

            lblErrMessage.Text = ErrMessage;
            lblErrMessage.Refresh();

            if (!string.IsNullOrEmpty(ErrMessage))
                this.Height += lblErrMessage.Height;
            else
                this.Height -= lblErrMessage.Height;

            this.CenterToScreen();
        }

        private void BtnOk_Click(object sender, System.EventArgs e)
        {
            Server = txtServer.Text;
            Login = txtLogin.Text;
            Password = txtPassword.Text;
            DataBase = txtDataBase.Text;

            ConnectionString = txtConnectionString.Text;

            ParameterTypeUsed = rbtnConnectionString.Checked ? EConnectorParameterTypeUsed.ConnectionString : EConnectorParameterTypeUsed.ManualParameters;

            if (rbtnConnectionString.Checked)
            {
                if (string.IsNullOrEmpty(ConnectionString.Trim()))
                {
                    MetroMessageBox.Show(this,"Se debe informar la cadena de conexion!!!", "Aviso al operador", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (!new DbUtil(EDatabaseEngine.MSSqlServer, ConnectionString.Trim()).IsValidConnectionString(out string errMessage))
                {
                    MetroMessageBox.Show(this,$"Error en el formato de la cadena de conexion \r\n \r\n detalle del error: \r\n \r\n {errMessage} ", "Aviso al operador", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                ConnectionString = "";
            }


            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();

        }

        private void BtnCancel_Click(object sender, System.EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void RbtnParametrosManuales_CheckedChanged(object sender, System.EventArgs e)
        {
            grpManualParameters.Enabled = rbtnParametrosManuales.Checked;
            grpConnectionString.Enabled = rbtnConnectionString.Checked;

        }

        private void RbtnConnectionString_CheckedChanged(object sender, System.EventArgs e)
        {
            grpManualParameters.Enabled = rbtnParametrosManuales.Checked;
            grpConnectionString.Enabled = rbtnConnectionString.Checked;

        }
    }
}
