﻿using MetroFramework;
using MetroFramework.Forms;
using System;
using System.Windows.Forms;
using SysWork.Data.Common.ValueObjects;
using SysWork.Data.DbConnector.Forms.Interfaces;
using SysWork.Data.Utilities;

namespace SysWork.Data.DbConnector.MetroForms.GetParameters
{
    public partial class MetroFormGetParamSQLite : MetroForm,IFormGetDBParameters
    {
        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>
        /// The connection string.
        /// </value>
        public string ConnectionString { get; set; }
        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>
        /// The error message.
        /// </value>
        public string ErrMessage { get; set; }
        public string Server { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string Login { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string Password { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string DataBase { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public EConnectorParameterTypeUsed ParameterTypeUsed { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrmGetParamSQLite"/> class.
        /// </summary>
        readonly MetroThemeStyle _theme;
        readonly MetroColorStyle _style;

        public MetroFormGetParamSQLite(MetroThemeStyle theme, MetroColorStyle style)
        {
            _theme = theme;
            _style = style;
            
            InitializeComponent();

            this.Theme = _theme;
            this.Style = _style;

            metroStyleManager1.Theme = _theme;
            metroStyleManager1.Style = _style;

            this.Refresh();
        }

        private void MetroFrmGetParamMSSQL_Load(object sender, System.EventArgs e)
        {
            txtConnectionString.Text = ConnectionString;

            lblErrMessage.Text = ErrMessage;
            lblErrMessage.Refresh();
            if (!string.IsNullOrEmpty(ErrMessage))
            {
                this.Height += lblErrMessage.Height;
            }
            else
            {
                this.Height -= lblErrMessage.Height;
            }

            this.CenterToScreen();
        }

        private void BtnOk_Click(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(txtConnectionString.Text))
            {
                MetroMessageBox.Show(this,"Se debe informar la cadena de conexion!!!", "Aviso al operador", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!new DbUtil(EDatabaseEngine.SqLite, txtConnectionString.Text).IsValidConnectionString(out string errMessage))
            {
                MetroMessageBox.Show(this,$"Error en el formato de la cadena de conexion \r\n \r\n detalle del error: \r\n \r\n {errMessage} ", "Aviso al operador", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            ConnectionString = txtConnectionString.Text;
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void BtnCancel_Click(object sender, System.EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void BtnBrowseConnection_Click(object sender, System.EventArgs e)
        {
            var ruta = @"c:\data\sample.sqlite";
            if (MetroMessageBox.Show(this,
                "Desea buscar el archivo de base de datos?",
                "Aviso al operador", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                openFileDialog1.Title = "Abrir Base SQLite";
                openFileDialog1.Filter = "Bases de datos SQLite V3(*.db;*.sqlite;*.db3)|*.db;*.sqlite;*.db3";
                openFileDialog1.Multiselect = false;
                openFileDialog1.ValidateNames = true;
                openFileDialog1.FilterIndex = 0;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    var filename = openFileDialog1.FileName;
                    ruta = filename;
                }
            }

            txtConnectionString.Text = $"data source={ruta};version=3;new=False;compress=True;pragma jounal_mode=WAL";

        }
    }
}
