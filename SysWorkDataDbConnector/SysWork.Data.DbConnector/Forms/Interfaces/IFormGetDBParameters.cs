﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SysWork.Data.Common.ValueObjects;

namespace SysWork.Data.DbConnector.Forms.Interfaces
{
    public interface IFormGetDBParameters
    {
        /// <summary>
        /// Gets or sets the server.
        /// </summary>
        /// <value>
        /// The server.
        /// </value>
        string Server { get; set; }

        /// <summary>
        /// Gets or sets the login.
        /// </summary>
        /// <value>
        /// The login.
        /// </value>
        string Login { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        string Password { get; set; }

        /// <summary>
        /// Gets or sets the dataBase.
        /// </summary>
        /// <value>
        /// The dataBase.
        /// </value>
        string DataBase { get; set; }

        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>
        /// The connection string.
        /// </value>
        string ConnectionString { get; set; }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>
        /// The error message.
        /// </value>
        string ErrMessage { get; set; }

        /// <summary>
        /// Gets or sets the parameter type used.
        /// </summary>
        /// <value>
        /// The parameter type used.
        /// </value>
        EConnectorParameterTypeUsed ParameterTypeUsed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        
        DialogResult DialogResult { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        DialogResult ShowDialog();
    }
}
