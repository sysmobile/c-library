﻿using System;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using SysWork.Data.Common.ValueObjects;
using SysWork.Data.DbConnector.Utilities;
using SysWork.Data.Utilities;
using SysWork.Data.DbConnector.Forms.Interfaces;

namespace SysWork.Data.Common.DbConnector
{
    /// <summary>
    /// Implementation of AbstractDbConnector Class for MySql
    /// </summary>
    /// <seealso cref="SysWork.Data.Common.DbConnector.AbstractDBConnector" />
    public class DbConnectorMySql : AbstractDBConnector
    {
        DbUtil _dbUtil = null;
        IFormGetDBParameters _frmGetParamMySQL;

        protected override DbUtil DbUtil
        {
            get
            {
                if (_dbUtil == null)
                    _dbUtil = new DbUtil(EDatabaseEngine.MySql, ConnectionString);

                return _dbUtil;
            }
        }
        public DbConnectorMySql (IFormGetDBParameters frmGetParamMySQL)
        {
            _frmGetParamMySQL = frmGetParamMySQL;
        }
        /// <summary>
        /// try to connect to the specified parameters.
        /// </summary>
        /// <exception cref="ArgumentException">The ConnectionStringName is not set.</exception>
        public override void Connect()
        {
            if (TryGetConnectionStringFromConfig)
            {
                if (string.IsNullOrEmpty(ConnectionStringName))
                    throw new ArgumentException("The ConnectionStringName is not set.");

                if (DbUtil.ExistsConnectionStringInConfig(ConnectionStringName))
                    ConnectionString = ConnectorUtilities.GetConnectionStringFromConfig(ConnectionStringName);

            }

            MySqlConnectionStringBuilder connectionSb = new MySqlConnectionStringBuilder();
            UserGotParameters = false;

            if (string.IsNullOrEmpty(ConnectionString))
            {
                connectionSb.Server = DefaultDataSource ?? "";
                connectionSb.UserID = DefaultUser ?? "";
                connectionSb.Password = DefaultPassword ?? "";
                connectionSb.Database= DefaultDatabase ?? "";
                ConnectorParameterTypeUsed = EConnectorParameterTypeUsed.ManualParameters;
            }
            else
            {
                connectionSb.ConnectionString = ConnectionString;

                if (IsEncryptedData)
                {
                    connectionSb.UserID = ConnectorUtilities.Decrypt(connectionSb.UserID);
                    connectionSb.Password = ConnectorUtilities.Decrypt(connectionSb.Password);
                    connectionSb.Server = ConnectorUtilities.Decrypt(connectionSb.Server);
                    connectionSb.Database = ConnectorUtilities.Decrypt(connectionSb.Database );
                }
                ConnectorParameterTypeUsed = EConnectorParameterTypeUsed.ConnectionString;
            }

            bool hasConnectionSuccess = false;
            string errMessage = "";

            if (!BeforeConnectShowDefaultsParameters)
            {
                hasConnectionSuccess = DbUtil.ConnectionSuccess(connectionSb.ConnectionString.ToString(), out errMessage);
                ConnectionError = errMessage;
            }

            bool needConnectionParameters = (!hasConnectionSuccess);

            while (needConnectionParameters && PromptUser)
            {
                UserGotParameters = true;


                _frmGetParamMySQL.Server  = connectionSb.Server;
                _frmGetParamMySQL.Login = connectionSb.UserID;
                _frmGetParamMySQL.Password = connectionSb.Password;
                _frmGetParamMySQL.DataBase = connectionSb.Database;
                _frmGetParamMySQL.ConnectionString = ConnectionString;

                _frmGetParamMySQL.ErrMessage = string.IsNullOrEmpty(errMessage) ? "" : "Ha ocurrido el siguiente error: \r\r" + errMessage;

                _frmGetParamMySQL.ParameterTypeUsed = ConnectorParameterTypeUsed;

                _frmGetParamMySQL.ShowDialog();

                if (_frmGetParamMySQL.DialogResult == DialogResult.OK)
                {
                    ConnectorParameterTypeUsed = _frmGetParamMySQL.ParameterTypeUsed;

                    if (!string.IsNullOrEmpty(_frmGetParamMySQL.ConnectionString))
                    {
                        ConnectionString = _frmGetParamMySQL.ConnectionString;
                        connectionSb.ConnectionString = _frmGetParamMySQL.ConnectionString;
                    }
                    else
                    {
                        connectionSb.Server = _frmGetParamMySQL.Server;
                        connectionSb.UserID = _frmGetParamMySQL.Login;
                        connectionSb.Password = _frmGetParamMySQL.Password;

                        if (!string.IsNullOrEmpty(_frmGetParamMySQL.DataBase.Trim()))
                            connectionSb.Database = _frmGetParamMySQL.DataBase;
                    }

                    hasConnectionSuccess = DbUtil.ConnectionSuccess(connectionSb.ConnectionString.ToString(), out errMessage);
                    ConnectionError = errMessage;
                }
                else
                {
                    hasConnectionSuccess = false;
                }

                needConnectionParameters = (!hasConnectionSuccess) && (_frmGetParamMySQL.DialogResult == DialogResult.OK);
            }

            ConnectionString = connectionSb.ConnectionString;
            IsConnectionSuccess = hasConnectionSuccess;

            if (IsConnectionSuccess && WriteInConfigFile)
            {
                if (IsEncryptedData)
                {
                    connectionSb.UserID = ConnectorUtilities.Encrypt(connectionSb.UserID);
                    connectionSb.Password = ConnectorUtilities.Encrypt(connectionSb.Password);
                    connectionSb.Server = ConnectorUtilities.Encrypt(connectionSb.Server);
                    connectionSb.Database = ConnectorUtilities.Encrypt(connectionSb.Database);
                }

                if (!DbUtil.ExistsConnectionStringInConfig(ConnectionStringName))
                    ConnectorUtilities.SaveConnectionStringInConfig(ConnectionStringName, connectionSb.ToString());
                else
                    if (UserGotParameters)
                    ConnectorUtilities.EditConnectionStringInConfig(ConnectionStringName, connectionSb.ToString());

            }
        }
    }
}
