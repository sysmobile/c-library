﻿using System;
using System.Data.SqlClient;
using SysWork.Data.Common.ValueObjects;
using SysWork.Data.DbConnector.Utilities;
using SysWork.Data.Utilities;
using SysWork.Data.DbConnector.Forms.Interfaces;
using System.Windows.Forms;

namespace SysWork.Data.Common.DbConnector
{
    /// <summary>
    /// Implementation of AbstractDbConnector Class for MSSqlServer
    /// </summary>
    /// <seealso cref="SysWork.Data.Common.DbConnector.AbstractDBConnector" />

    public class DbConnectorMSSqlServer : AbstractDBConnector
    {
        IFormGetDBParameters _frmGetParamSQL;

        public DbConnectorMSSqlServer(IFormGetDBParameters frmGetParamSQL)
        {
            _frmGetParamSQL = frmGetParamSQL;
        }

        DbUtil _dbUtil = null;
        protected override DbUtil DbUtil 
        { get 
            { if (_dbUtil == null)
                    _dbUtil = new DbUtil(EDatabaseEngine.MSSqlServer, ConnectionString);

                return _dbUtil;
            } 
        }

        /// <summary>
        /// Try to connect with the specified parameters.
        /// </summary>
        /// <exception cref="ArgumentException">The ConnectionStringName is not set.</exception>
        public override void Connect()
        {
            if (TryGetConnectionStringFromConfig)
            {
                if (string.IsNullOrEmpty(ConnectionStringName))
                    throw new ArgumentException("The ConnectionStringName is not set.");

                if (DbUtil.ExistsConnectionStringInConfig(ConnectionStringName))
                    ConnectionString = ConnectorUtilities.GetConnectionStringFromConfig(ConnectionStringName);

            }

            SqlConnectionStringBuilder connectionSb = new SqlConnectionStringBuilder();
            UserGotParameters = false;

            if (string.IsNullOrEmpty(ConnectionString))
            {
                connectionSb.DataSource = DefaultDataSource ?? "";
                connectionSb.UserID = DefaultUser ?? "";
                connectionSb.Password = DefaultPassword ?? "";
                connectionSb.InitialCatalog = DefaultDatabase ?? "";
                ConnectorParameterTypeUsed = EConnectorParameterTypeUsed.ManualParameters;
            }
            else
            {
                connectionSb.ConnectionString = ConnectionString;

                if (IsEncryptedData)
                {
                    connectionSb.UserID = ConnectorUtilities.Decrypt(connectionSb.UserID);
                    connectionSb.Password = ConnectorUtilities.Decrypt(connectionSb.Password);
                    connectionSb.DataSource = ConnectorUtilities.Decrypt(connectionSb.DataSource);
                    connectionSb.InitialCatalog = ConnectorUtilities.Decrypt(connectionSb.InitialCatalog);
                }
                ConnectorParameterTypeUsed = EConnectorParameterTypeUsed.ConnectionString;
            }

            bool hasConnectionSuccess = false;
            string errMessage = "";

            if (!BeforeConnectShowDefaultsParameters)
            {
                hasConnectionSuccess = DbUtil.ConnectionSuccess(connectionSb.ConnectionString.ToString(), out errMessage);
                ConnectionError = errMessage;
            }

            bool needConnectionParameters = (!hasConnectionSuccess);

            while (needConnectionParameters && PromptUser)
            {
                UserGotParameters = true;
                _frmGetParamSQL.Server = connectionSb.DataSource;
                _frmGetParamSQL.Login = connectionSb.UserID;
                _frmGetParamSQL.Password = connectionSb.Password;
                _frmGetParamSQL.DataBase = connectionSb.InitialCatalog;
                _frmGetParamSQL.ConnectionString = ConnectionString;
                _frmGetParamSQL.ErrMessage = string.IsNullOrEmpty(errMessage) ? "" : "Ha ocurrido el siguiente error: \r\r" + errMessage;
                _frmGetParamSQL.ParameterTypeUsed = ConnectorParameterTypeUsed;

                _frmGetParamSQL.ShowDialog();

                if (_frmGetParamSQL.DialogResult == DialogResult.OK)
                {
                    ConnectorParameterTypeUsed = _frmGetParamSQL.ParameterTypeUsed;

                    if (!string.IsNullOrEmpty(_frmGetParamSQL.ConnectionString))
                    {
                        ConnectionString  = _frmGetParamSQL.ConnectionString;
                        connectionSb.ConnectionString = _frmGetParamSQL.ConnectionString;
                    }
                    else
                    {
                        connectionSb.DataSource = _frmGetParamSQL.Server;
                        connectionSb.UserID = _frmGetParamSQL.Login;
                        connectionSb.Password = _frmGetParamSQL.Password;

                        if (!string.IsNullOrEmpty(_frmGetParamSQL.DataBase.Trim()))
                            connectionSb.InitialCatalog = _frmGetParamSQL.DataBase;
                    }

                    hasConnectionSuccess = DbUtil.ConnectionSuccess(connectionSb.ConnectionString.ToString(), out errMessage);
                    ConnectionError = errMessage;
                }
                else
                {
                    hasConnectionSuccess = false;
                }

                needConnectionParameters = (!hasConnectionSuccess) && (_frmGetParamSQL.DialogResult == DialogResult.OK);
            }

            ConnectionString = connectionSb.ConnectionString;
            IsConnectionSuccess = hasConnectionSuccess;

            if (IsConnectionSuccess && WriteInConfigFile)
            {
                if (IsEncryptedData)
                {
                    connectionSb.UserID = ConnectorUtilities.Encrypt(connectionSb.UserID);
                    connectionSb.Password = ConnectorUtilities.Encrypt (connectionSb.Password);
                    connectionSb.DataSource = ConnectorUtilities.Encrypt (connectionSb.DataSource);
                    connectionSb.InitialCatalog = ConnectorUtilities.Encrypt(connectionSb.InitialCatalog);
                }

                if (!DbUtil.ExistsConnectionStringInConfig(ConnectionStringName))
                    ConnectorUtilities.SaveConnectionStringInConfig(ConnectionStringName, connectionSb.ToString());
                else
                    if (UserGotParameters)
                    ConnectorUtilities.EditConnectionStringInConfig(ConnectionStringName, connectionSb.ToString());
            }
        }
    }
}
