﻿using System;
using System.Data.OleDb;
using System.Windows.Forms;
using SysWork.Data.Common.ValueObjects;
using SysWork.Data.DbConnector.Utilities;
using SysWork.Data.Utilities;
using SysWork.Data.DbConnector.Forms.Interfaces;

namespace SysWork.Data.Common.DbConnector
{
    /// <summary>
    /// Implementation of AbstractDbConnector Class for OleDb
    /// </summary>
    /// <seealso cref="SysWork.Data.Common.DbConnector.AbstractDBConnector" />
    public class DbConnectorOleDb : AbstractDBConnector
    {

        DbUtil _dbUtil = null;
        IFormGetDBParameters _frmGetParamOleDb;

        public DbConnectorOleDb(IFormGetDBParameters frmGetParamOleDb)
        {
            this._frmGetParamOleDb = frmGetParamOleDb;
        }   

        protected override DbUtil DbUtil
        {
            get
            {
                if (_dbUtil == null)
                    _dbUtil = new DbUtil(EDatabaseEngine.OleDb, ConnectionString);

                return _dbUtil;
            }
        }

        /// <summary>
        /// try to connect to the specified parameters.
        /// </summary>
        /// <exception cref="ArgumentException">The ConnectionStringName is not set.</exception>
        public override void Connect()
        {
            ConnectorParameterTypeUsed = EConnectorParameterTypeUsed.ConnectionString;

            if (TryGetConnectionStringFromConfig)
            {
                if (string.IsNullOrEmpty(ConnectionStringName))
                    throw new ArgumentException("The ConnectionStringName is not set.");

                if (DbUtil.ExistsConnectionStringInConfig(ConnectionStringName))
                    ConnectionString = ConnectorUtilities.GetConnectionStringFromConfig(ConnectionStringName);

            }

            OleDbConnectionStringBuilder connectionSb = new OleDbConnectionStringBuilder();
            UserGotParameters = false;

            if (!string.IsNullOrEmpty(ConnectionString))
            {
                connectionSb.ConnectionString = ConnectionString;
                if (IsEncryptedData)
                {
                    foreach (var key in connectionSb.Keys)
                    {
                        if (connectionSb.ContainsKey(key.ToString()))
                            if (connectionSb[key.ToString()].GetType() == typeof(string))
                                connectionSb[key.ToString()] = ConnectorUtilities.Decrypt (connectionSb[key.ToString()].ToString());
                    }
                }
            }

            bool hasConnectionSuccess = false;
            string errMessage = "";

            if (!BeforeConnectShowDefaultsParameters)
            {
                hasConnectionSuccess = DbUtil.ConnectionSuccess(connectionSb.ConnectionString.ToString(), out errMessage);
                ConnectionError = errMessage;
            }

            bool needConnectionParameters = (!hasConnectionSuccess);

            while (needConnectionParameters && PromptUser)
            {
                UserGotParameters = true;


                _frmGetParamOleDb.ConnectionString = ConnectionString;
                _frmGetParamOleDb.ErrMessage = string.IsNullOrEmpty(errMessage) ? "" : "Ha ocurrido el siguiente error: \r\r" + errMessage;
                _frmGetParamOleDb.ShowDialog();

                if (_frmGetParamOleDb.DialogResult == DialogResult.OK)
                {
                    if (!string.IsNullOrEmpty(_frmGetParamOleDb.ConnectionString))
                    {
                        ConnectionString = _frmGetParamOleDb.ConnectionString;
                        connectionSb.ConnectionString = _frmGetParamOleDb.ConnectionString;
                    }

                    hasConnectionSuccess = DbUtil.ConnectionSuccess(connectionSb.ConnectionString.ToString(), out errMessage);
                    ConnectionError = errMessage;
                }
                else
                {
                    hasConnectionSuccess = false;
                }

                needConnectionParameters = (!hasConnectionSuccess) && (_frmGetParamOleDb.DialogResult == DialogResult.OK);
            }

            ConnectionString = connectionSb.ConnectionString;
            IsConnectionSuccess = hasConnectionSuccess;

            if (IsConnectionSuccess && WriteInConfigFile)
            {
                if (IsEncryptedData)
                {
                    foreach (var key in connectionSb.Keys)
                    {
                        if (connectionSb.ContainsKey(key.ToString()))
                            if (connectionSb[key.ToString()].GetType()==typeof(string))
                                connectionSb[key.ToString()] = ConnectorUtilities.Encrypt(connectionSb[key.ToString()].ToString());
                    }
                }

                if (!DbUtil.ExistsConnectionStringInConfig(ConnectionStringName))
                    ConnectorUtilities.SaveConnectionStringInConfig(ConnectionStringName, connectionSb.ToString());
                else
                    if (UserGotParameters)
                    ConnectorUtilities.EditConnectionStringInConfig(ConnectionStringName, connectionSb.ToString());

            }
        }
    }
}
