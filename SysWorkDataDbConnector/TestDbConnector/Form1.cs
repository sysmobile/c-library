﻿using MetroFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SysWork.Data.Common.Metro.DbConnector;
using SysWork.Data.Common.WinForms.DbConnector;

namespace TestDbConnector
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var connector = new DataBaseConnector();
            connector.PromptUser = true;
            connector.Connect();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var connector = new MetroDataBaseConnector(MetroThemeStyle.Dark,MetroColorStyle.Orange);
            connector.PromptUser = true;
            connector.BeforeConnectShowDefaultsParameters = true;
            connector.Connect();
        }
    }
}
