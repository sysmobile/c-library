﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SysWork.Data.Common.Metro.DbConnector;

namespace TestMetro
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var ruta = @"c:\data\sample.sqlite";
                openFileDialog1.Title = "Abrir Base SQLite";
                openFileDialog1.Filter = "Bases de datos SQLite V3(*.db;*.sqlite;*.db3)|*.db;*.sqlite;*.db3";
                openFileDialog1.Multiselect = false;
                openFileDialog1.ValidateNames = true;
                openFileDialog1.FilterIndex = 0;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    var filename = openFileDialog1.FileName;
                    ruta = filename;
                }

            _ = $"data source={ruta};version=3;new=False;compress=True;pragma jounal_mode=WAL";

        }

    }
}
