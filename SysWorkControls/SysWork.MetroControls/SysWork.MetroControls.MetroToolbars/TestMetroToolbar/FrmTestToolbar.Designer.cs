﻿namespace TestMetroToolbar
{
    partial class FrmTestToolbar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroToolbarReportSmall1 = new SysWork.MetroControls.MetroToolbars.MetroToolbarReportSmall();
            this.metroToolbarReport1 = new SysWork.MetroControls.MetroToolbars.MetroToolbarReport();
            this.metroToolbarCRUDSmall1 = new SysWork.MetroControls.MetroToolbars.MetroToolbarCRUDSmall();
            this.metroToolbarCRUD1 = new SysWork.MetroControls.MetroToolbars.MetroToolbarCRUD();
            this.SuspendLayout();
            // 
            // metroToolbarReportSmall1
            // 
            this.metroToolbarReportSmall1.BackColor = System.Drawing.Color.White;
            this.metroToolbarReportSmall1.CleanFiltersToolTipText = "Limpiar Filtros";
            this.metroToolbarReportSmall1.ClenFiltersEnabled = true;
            this.metroToolbarReportSmall1.ClenFiltersVisible = true;
            this.metroToolbarReportSmall1.DisplayEnabled = true;
            this.metroToolbarReportSmall1.DisplayToolTipText = "Visualizar por Pantalla (F2)";
            this.metroToolbarReportSmall1.DisplayVisible = true;
            this.metroToolbarReportSmall1.ExitEnabled = true;
            this.metroToolbarReportSmall1.ExitToolTipText = "Salir (ESC)";
            this.metroToolbarReportSmall1.ExitVisible = true;
            this.metroToolbarReportSmall1.ImportExportEnabled = true;
            this.metroToolbarReportSmall1.ImportExportToolTipText = "Exportar";
            this.metroToolbarReportSmall1.ImportExportVisible = true;
            this.metroToolbarReportSmall1.Location = new System.Drawing.Point(9, 123);
            this.metroToolbarReportSmall1.Margin = new System.Windows.Forms.Padding(0);
            this.metroToolbarReportSmall1.Name = "metroToolbarReportSmall1";
            this.metroToolbarReportSmall1.PrintEnabled = true;
            this.metroToolbarReportSmall1.PrintToolTipText = "Imprimir (F3)";
            this.metroToolbarReportSmall1.PrintVisible = true;
            this.metroToolbarReportSmall1.RefreshEnabled = true;
            this.metroToolbarReportSmall1.RefreshToolTipText = "Refresh (F12)";
            this.metroToolbarReportSmall1.RefreshVisible = true;
            this.metroToolbarReportSmall1.SearchEnabled = true;
            this.metroToolbarReportSmall1.SearchToolTipText = "Buscar (F5)";
            this.metroToolbarReportSmall1.SearchVisible = true;
            this.metroToolbarReportSmall1.Size = new System.Drawing.Size(164, 22);
            this.metroToolbarReportSmall1.TabIndex = 3;
            this.metroToolbarReportSmall1.Theme = SysWork.MetroControls.MetroToolbars.DisplaySettingsTheme.Light;
            // 
            // metroToolbarReport1
            // 
            this.metroToolbarReport1.BackColor = System.Drawing.Color.White;
            this.metroToolbarReport1.CleanFiltersToolTipText = "Limpiar Filtros";
            this.metroToolbarReport1.ClenFiltersEnabled = true;
            this.metroToolbarReport1.ClenFiltersVisible = true;
            this.metroToolbarReport1.DisplayEnabled = true;
            this.metroToolbarReport1.DisplayToolTipText = "Visualizar por Pantalla (F2)";
            this.metroToolbarReport1.DisplayVisible = true;
            this.metroToolbarReport1.ExitEnabled = true;
            this.metroToolbarReport1.ExitToolTipText = "Salir (ESC)";
            this.metroToolbarReport1.ExitVisible = true;
            this.metroToolbarReport1.ImportExportEnabled = true;
            this.metroToolbarReport1.ImportExportToolTipText = "Exportar";
            this.metroToolbarReport1.ImportExportVisible = true;
            this.metroToolbarReport1.Location = new System.Drawing.Point(9, 94);
            this.metroToolbarReport1.Margin = new System.Windows.Forms.Padding(0);
            this.metroToolbarReport1.Name = "metroToolbarReport1";
            this.metroToolbarReport1.PrintEnabled = true;
            this.metroToolbarReport1.PrintToolTipText = "Imprimir (F3)";
            this.metroToolbarReport1.PrintVisible = true;
            this.metroToolbarReport1.RefreshEnabled = true;
            this.metroToolbarReport1.RefreshToolTipText = "Refresh (F12)";
            this.metroToolbarReport1.RefreshVisible = true;
            this.metroToolbarReport1.SearchEnabled = true;
            this.metroToolbarReport1.SearchToolTipText = "Buscar (F5)";
            this.metroToolbarReport1.SearchVisible = true;
            this.metroToolbarReport1.Size = new System.Drawing.Size(199, 29);
            this.metroToolbarReport1.TabIndex = 2;
            this.metroToolbarReport1.Theme = SysWork.MetroControls.MetroToolbars.DisplaySettingsTheme.Light;
            // 
            // metroToolbarCRUDSmall1
            // 
            this.metroToolbarCRUDSmall1.BackColor = System.Drawing.Color.White;
            this.metroToolbarCRUDSmall1.DeleteEnabled = true;
            this.metroToolbarCRUDSmall1.DeleteToolTipText = "Eliminar (F10)";
            this.metroToolbarCRUDSmall1.DeleteVisible = true;
            this.metroToolbarCRUDSmall1.ExitEnabled = true;
            this.metroToolbarCRUDSmall1.ExitToolTipText = "Salir (ESC)";
            this.metroToolbarCRUDSmall1.ExitVisible = true;
            this.metroToolbarCRUDSmall1.ImportExportEnabled = true;
            this.metroToolbarCRUDSmall1.ImportExportToolTipText = "Importar / Exportar";
            this.metroToolbarCRUDSmall1.ImportExportVisible = true;
            this.metroToolbarCRUDSmall1.InitializeEnabled = true;
            this.metroToolbarCRUDSmall1.InitializeToolTipText = "Inicializar Formulario (ESC)";
            this.metroToolbarCRUDSmall1.InitializeVisible = true;
            this.metroToolbarCRUDSmall1.Location = new System.Drawing.Point(9, 47);
            this.metroToolbarCRUDSmall1.Margin = new System.Windows.Forms.Padding(0);
            this.metroToolbarCRUDSmall1.Name = "metroToolbarCRUDSmall1";
            this.metroToolbarCRUDSmall1.NewEnabled = true;
            this.metroToolbarCRUDSmall1.NewToolTipText = "Nuevo (F3)";
            this.metroToolbarCRUDSmall1.NewVisible = true;
            this.metroToolbarCRUDSmall1.RefreshEnabled = true;
            this.metroToolbarCRUDSmall1.RefreshToolTipText = "Eliminar (F10)";
            this.metroToolbarCRUDSmall1.RefreshVisible = true;
            this.metroToolbarCRUDSmall1.ReportEnabled = true;
            this.metroToolbarCRUDSmall1.ReportToolTipText = "Reporte (F4)";
            this.metroToolbarCRUDSmall1.ReportVisible = true;
            this.metroToolbarCRUDSmall1.SaveEnabled = true;
            this.metroToolbarCRUDSmall1.SaveToolTipText = "Grabar (F2)";
            this.metroToolbarCRUDSmall1.SaveVisible = true;
            this.metroToolbarCRUDSmall1.SearchEnabled = true;
            this.metroToolbarCRUDSmall1.SearchToolTipText = "Buscar (F5)";
            this.metroToolbarCRUDSmall1.SearchVisible = true;
            this.metroToolbarCRUDSmall1.Size = new System.Drawing.Size(254, 22);
            this.metroToolbarCRUDSmall1.TabIndex = 1;
            this.metroToolbarCRUDSmall1.Theme = SysWork.MetroControls.MetroToolbars.DisplaySettingsTheme.Light;
            // 
            // metroToolbarCRUD1
            // 
            this.metroToolbarCRUD1.BackColor = System.Drawing.Color.White;
            this.metroToolbarCRUD1.DeleteEnabled = true;
            this.metroToolbarCRUD1.DeleteToolTipText = "Eliminar (F10)";
            this.metroToolbarCRUD1.DeleteVisible = true;
            this.metroToolbarCRUD1.ExitEnabled = true;
            this.metroToolbarCRUD1.ExitToolTipText = "Salir (ESC)";
            this.metroToolbarCRUD1.ExitVisible = true;
            this.metroToolbarCRUD1.ImportExportEnabled = true;
            this.metroToolbarCRUD1.ImportExportToolTipText = "Importar / Exportar";
            this.metroToolbarCRUD1.ImportExportVisible = true;
            this.metroToolbarCRUD1.InitializeEnabled = true;
            this.metroToolbarCRUD1.InitializeToolTipText = "Inicializar Formulario (ESC)";
            this.metroToolbarCRUD1.InitializeVisible = true;
            this.metroToolbarCRUD1.Location = new System.Drawing.Point(9, 9);
            this.metroToolbarCRUD1.Margin = new System.Windows.Forms.Padding(0);
            this.metroToolbarCRUD1.Name = "metroToolbarCRUD1";
            this.metroToolbarCRUD1.NewEnabled = true;
            this.metroToolbarCRUD1.NewToolTipText = "Nuevo (F3)";
            this.metroToolbarCRUD1.NewVisible = true;
            this.metroToolbarCRUD1.RefreshEnabled = true;
            this.metroToolbarCRUD1.RefreshToolTipText = "Eliminar (F10)";
            this.metroToolbarCRUD1.RefreshVisible = true;
            this.metroToolbarCRUD1.ReportEnabled = true;
            this.metroToolbarCRUD1.ReportToolTipText = "Reporte (F4)";
            this.metroToolbarCRUD1.ReportVisible = true;
            this.metroToolbarCRUD1.SaveEnabled = true;
            this.metroToolbarCRUD1.SaveToolTipText = "Grabar (F2)";
            this.metroToolbarCRUD1.SaveVisible = true;
            this.metroToolbarCRUD1.SearchEnabled = true;
            this.metroToolbarCRUD1.SearchToolTipText = "Buscar (F5)";
            this.metroToolbarCRUD1.SearchVisible = true;
            this.metroToolbarCRUD1.Size = new System.Drawing.Size(455, 29);
            this.metroToolbarCRUD1.TabIndex = 0;
            this.metroToolbarCRUD1.Theme = SysWork.MetroControls.MetroToolbars.DisplaySettingsTheme.Light;
            // 
            // FrmTestToolbar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.metroToolbarReportSmall1);
            this.Controls.Add(this.metroToolbarReport1);
            this.Controls.Add(this.metroToolbarCRUDSmall1);
            this.Controls.Add(this.metroToolbarCRUD1);
            this.Name = "FrmTestToolbar";
            this.Text = "FrmTestToolbar";
            this.Load += new System.EventHandler(this.FrmTestToolbar_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private SysWork.MetroControls.MetroToolbars.MetroToolbarCRUD metroToolbarCRUD1;
        private SysWork.MetroControls.MetroToolbars.MetroToolbarCRUDSmall metroToolbarCRUDSmall1;
        private SysWork.MetroControls.MetroToolbars.MetroToolbarReport metroToolbarReport1;
        private SysWork.MetroControls.MetroToolbars.MetroToolbarReportSmall metroToolbarReportSmall1;
    }
}