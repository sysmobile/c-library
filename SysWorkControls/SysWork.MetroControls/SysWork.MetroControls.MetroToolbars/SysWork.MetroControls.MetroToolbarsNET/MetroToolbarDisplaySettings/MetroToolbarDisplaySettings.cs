﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using SysWork.MetroControls.MetroToolbarsNET.Properties;

namespace SysWork.MetroControls.MetroToolbarsNET
{
    [DefaultEvent("SettingsChanged")]
    public partial class MetroToolbarDisplaySettings : UserControl
    {

        private bool _loading = true;
        private bool _fromProperty = false;
        /// <summary>
        /// Occurs when Settings are Changed.
        /// </summary>
        public event MetroToolbarDisplaySettingsChangeEventHandler SettingsChanged;
        public delegate void MetroToolbarDisplaySettingsChangeEventHandler(Object sender, MetroToolbarDisplaySettingsChangeEventArgs e);

        public bool ChangeThemeEnabled
        {
            get { return ButtonChangeTheme.Enabled; }
            set { ButtonChangeTheme.Enabled = value; }
        }
        public bool ChangeThemeVisible
        {
            get { return ButtonChangeTheme.Visible; }
            set { ButtonChangeTheme.Visible = value; }
        }

        public bool ChangeStyleEnabled
        {
            get { return ButtonChangeStyle.Enabled; }
            set { ButtonChangeStyle.Enabled = value; }
        }
        public bool ChangeStyleVisible
        {
            get { return ButtonChangeStyle.Visible; }
            set { ButtonChangeStyle.Visible = value; }
        }

        private DisplaySettingsTheme _theme = DisplaySettingsTheme.Light;
        public DisplaySettingsTheme Theme
        {
            get
            {
                return _theme;
            }
            set
            {
                _theme = value;
                _fromProperty = true;
                SetThemeValueToCombo();
                ApplyTheme();
                _fromProperty = false;
            }
        }

        private DisplaySettingsStyle _style = DisplaySettingsStyle.Blue;

        public DisplaySettingsStyle Style
        {
            get
            {
                return _style;
            }
            set
            {
                _style = value;
                _fromProperty = true;
                SetStyleValueToCombo();
                _fromProperty = false;
            }
        }

        public MetroToolbarDisplaySettings()
        {
            InitializeComponent();
            InitComboboxes();
            ApplyTheme();
            SetThemeValueToCombo();
            SetStyleValueToCombo();
        }


        private void InitComboboxes()
        {
            TsCmbTheme.Items.Clear();

            TsCmbStyles.Items.Clear();

            TsCmbTheme.Items.AddRange(Enum.GetValues(typeof(DisplaySettingsTheme))
                      .Cast<object>()
                      .ToArray());
            TsCmbStyles.Items.AddRange(Enum.GetValues(typeof(DisplaySettingsStyle))
                      .Cast<object>()
                      .ToArray());

        }

        private void ApplyTheme()
        {
            var _backColor = Color.White;
            var _textForeColor = Color.Black;
            switch (_theme)
            {
                case DisplaySettingsTheme.Default:
                case DisplaySettingsTheme.Light:
                    _backColor = Color.White;
                    _textForeColor = Color.Black;
                    break;
                case DisplaySettingsTheme.Dark:
                    _backColor = Color.FromArgb(17, 17, 17);
                    _textForeColor = Color.White;
                    break;
            }

            this.ToolbarDisplaySettings.BackColor = _backColor;
            this.BackColor = _backColor;

            this.ButtonChangeTheme.BackColor = _backColor;
            this.ButtonChangeTheme.Image = (_theme == DisplaySettingsTheme.Dark) ? Resources.changeThemeDark : Resources.changeThemeLight;

            this.ButtonChangeStyle.BackColor = _backColor;
            this.ButtonChangeStyle.Image = (_theme == DisplaySettingsTheme.Dark) ? Resources.changeStyleDark : Resources.changeStyleLight;

            this.TsCmbTheme.BackColor = _backColor;
            this.TsCmbTheme.ForeColor = _textForeColor;

            this.TsCmbStyles.BackColor = _backColor;
            this.TsCmbStyles.ForeColor = _textForeColor;

        }
        private void SetStyleValueToCombo()
        {
            TsCmbStyles.SelectedIndex = (int)_style;
        }
        private void SetThemeValueToCombo()
        {
            TsCmbTheme.SelectedIndex = (int)_theme;
        }

        protected virtual void OnMetroToolbarDisplaySettingsChanged(MetroToolbarDisplaySettingsChangeEventArgs e)
        {
            MetroToolbarDisplaySettingsChangeEventHandler handler = SettingsChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void ThrowSettingsChangeEvent()
        {
            var args = new MetroToolbarDisplaySettingsChangeEventArgs
            {
                Theme = _theme,
                Style = _style
            };

            OnMetroToolbarDisplaySettingsChanged(args);
        }


        private void ButtonChangeTheme_CheckedChanged(object sender, EventArgs e)
        {

            if (ButtonChangeStyle.Checked && ButtonChangeTheme.Checked)
                ButtonChangeStyle.Checked = false;

            TsCmbTheme.Visible = ButtonChangeTheme.Checked;
        }

        private void ButtonChangeStyle_CheckedChanged(object sender, EventArgs e)
        {
            TsCmbStyles.Visible = ButtonChangeStyle.Checked;

            if (ButtonChangeTheme.Checked && ButtonChangeStyle.Checked)
                ButtonChangeTheme.Checked = false;

        }

        private void MetroToolbarDisplaySettings_Resize(object sender, EventArgs e)
        {
            if (this.Height >= 27)
                this.Height = 25;
        }

        private void TsCmbTheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            _theme = (DisplaySettingsTheme)TsCmbTheme.SelectedItem;
            ApplyTheme();
            if (!_loading && !_fromProperty)
                ThrowSettingsChangeEvent();
        }

        private void TsCmbStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            _style = (DisplaySettingsStyle)TsCmbStyles.SelectedItem;

            if (!_loading && !_fromProperty)
                ThrowSettingsChangeEvent();
        }

        private void MetroToolbarDisplaySettings_Load(object sender, EventArgs e)
        {
            _loading = false;
        }

    }

    public class MetroToolbarDisplaySettingsChangeEventArgs : EventArgs
    {
        public DisplaySettingsTheme Theme;
        public DisplaySettingsStyle Style;
    }
}
