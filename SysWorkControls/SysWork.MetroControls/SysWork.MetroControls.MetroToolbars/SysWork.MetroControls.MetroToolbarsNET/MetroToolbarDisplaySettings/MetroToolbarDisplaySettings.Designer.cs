﻿namespace SysWork.MetroControls.MetroToolbarsNET
{
    partial class MetroToolbarDisplaySettings
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            ToolbarDisplaySettings = new System.Windows.Forms.ToolStrip();
            ButtonChangeTheme = new System.Windows.Forms.ToolStripButton();
            TsCmbTheme = new System.Windows.Forms.ToolStripComboBox();
            ButtonChangeStyle = new System.Windows.Forms.ToolStripButton();
            TsCmbStyles = new System.Windows.Forms.ToolStripComboBox();
            ToolbarDisplaySettings.SuspendLayout();
            SuspendLayout();
            // 
            // ToolbarDisplaySettings
            // 
            ToolbarDisplaySettings.BackColor = System.Drawing.Color.FromArgb(17, 17, 17);
            ToolbarDisplaySettings.Dock = System.Windows.Forms.DockStyle.None;
            ToolbarDisplaySettings.GripMargin = new System.Windows.Forms.Padding(0);
            ToolbarDisplaySettings.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            ToolbarDisplaySettings.ImageScalingSize = new System.Drawing.Size(24, 24);
            ToolbarDisplaySettings.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { ButtonChangeTheme, TsCmbTheme, ButtonChangeStyle, TsCmbStyles });
            ToolbarDisplaySettings.Location = new System.Drawing.Point(1, 1);
            ToolbarDisplaySettings.Name = "ToolbarDisplaySettings";
            ToolbarDisplaySettings.Padding = new System.Windows.Forms.Padding(0);
            ToolbarDisplaySettings.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            ToolbarDisplaySettings.Size = new System.Drawing.Size(58, 31);
            ToolbarDisplaySettings.TabIndex = 2;
            // 
            // ButtonChangeTheme
            // 
            ButtonChangeTheme.CheckOnClick = true;
            ButtonChangeTheme.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            ButtonChangeTheme.Image = Properties.Resources.changeThemeLight;
            ButtonChangeTheme.ImageTransparentColor = System.Drawing.Color.Magenta;
            ButtonChangeTheme.Name = "ButtonChangeTheme";
            ButtonChangeTheme.Size = new System.Drawing.Size(28, 28);
            ButtonChangeTheme.ToolTipText = "Cambiar Tema";
            ButtonChangeTheme.CheckedChanged += ButtonChangeTheme_CheckedChanged;
            // 
            // TsCmbTheme
            // 
            TsCmbTheme.BackColor = System.Drawing.Color.FromArgb(17, 17, 17);
            TsCmbTheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            TsCmbTheme.ForeColor = System.Drawing.Color.Black;
            TsCmbTheme.Items.AddRange(new object[] { "Light", "Dark" });
            TsCmbTheme.Name = "TsCmbTheme";
            TsCmbTheme.Size = new System.Drawing.Size(75, 31);
            TsCmbTheme.Visible = false;
            TsCmbTheme.SelectedIndexChanged += TsCmbTheme_SelectedIndexChanged;
            // 
            // ButtonChangeStyle
            // 
            ButtonChangeStyle.CheckOnClick = true;
            ButtonChangeStyle.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            ButtonChangeStyle.Image = Properties.Resources.changeStyleLight;
            ButtonChangeStyle.ImageTransparentColor = System.Drawing.Color.Magenta;
            ButtonChangeStyle.Name = "ButtonChangeStyle";
            ButtonChangeStyle.Size = new System.Drawing.Size(28, 28);
            ButtonChangeStyle.ToolTipText = "Cambiar Estilo";
            ButtonChangeStyle.CheckedChanged += ButtonChangeStyle_CheckedChanged;
            // 
            // TsCmbStyles
            // 
            TsCmbStyles.BackColor = System.Drawing.Color.FromArgb(17, 17, 17);
            TsCmbStyles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            TsCmbStyles.Items.AddRange(new object[] { "Blue", "Black", "White", "MetroColors", "Green", "Lime", "Teal", "Orange", "Brown", "Pink", "Magenta", "Purple", "Red", "Yellow" });
            TsCmbStyles.Name = "TsCmbStyles";
            TsCmbStyles.Size = new System.Drawing.Size(75, 31);
            TsCmbStyles.Visible = false;
            TsCmbStyles.SelectedIndexChanged += TsCmbStyles_SelectedIndexChanged;
            // 
            // MetroToolbarDisplaySettings
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            AutoSize = true;
            BackColor = System.Drawing.Color.FromArgb(17, 17, 17);
            Controls.Add(ToolbarDisplaySettings);
            Location = new System.Drawing.Point(-1, -1);
            Margin = new System.Windows.Forms.Padding(0);
            Name = "MetroToolbarDisplaySettings";
            Size = new System.Drawing.Size(273, 32);
            Load += MetroToolbarDisplaySettings_Load;
            Resize += MetroToolbarDisplaySettings_Resize;
            ToolbarDisplaySettings.ResumeLayout(false);
            ToolbarDisplaySettings.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.ToolStrip ToolbarDisplaySettings;
        private System.Windows.Forms.ToolStripButton ButtonChangeTheme;
        private System.Windows.Forms.ToolStripButton ButtonChangeStyle;
        private System.Windows.Forms.ToolStripComboBox TsCmbTheme;
        private System.Windows.Forms.ToolStripComboBox TsCmbStyles;
    }
}
