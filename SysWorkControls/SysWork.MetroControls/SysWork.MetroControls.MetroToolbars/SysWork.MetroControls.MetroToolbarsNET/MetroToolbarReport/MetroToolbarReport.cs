﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using SysWork.MetroControls.MetroToolbarsNET.Properties;

namespace SysWork.MetroControls.MetroToolbarsNET
{

    [DefaultEvent("ActionSelected")]
    public partial class MetroToolbarReport : UserControl
    {
        /// <summary>
        /// Occurs when an action is selected.
        /// </summary>
        public event MetroToolbarReportClickEventHandler ActionSelected;
        public delegate void MetroToolbarReportClickEventHandler(Object sender, MetroToolbarReportClickEventArgs e);

        public bool DisplayEnabled
        {
            get { return ButtonDisplay.Enabled; }
            set { ButtonDisplay.Enabled = value; }
        }
        public bool DisplayVisible
        {
            get { return ButtonDisplay.Visible; }
            set { ButtonDisplay.Visible = value; }
        }
        public string DisplayToolTipText
        {
            get { return ButtonDisplay.ToolTipText; }
            set { ButtonDisplay.ToolTipText = value; }
        }

        public bool PrintEnabled
        {
            get { return ButtonPrint.Enabled; }
            set { ButtonPrint.Enabled = value; }
        }
        public bool PrintVisible
        {
            get { return ButtonPrint.Visible; }
            set { ButtonPrint.Visible = value; }
        }
        public string PrintToolTipText
        {
            get { return ButtonPrint.ToolTipText; }
            set { ButtonPrint.ToolTipText = value; }
        }

        public bool RefreshEnabled
        {
            get { return ButtonRefresh.Enabled; }
            set { ButtonRefresh.Enabled = value; }
        }
        public bool RefreshVisible
        {
            get { return ButtonRefresh.Visible; }
            set { ButtonRefresh.Visible = value; }
        }
        public string RefreshToolTipText
        {
            get { return ButtonRefresh.ToolTipText; }
            set { ButtonRefresh.ToolTipText = value; }
        }

        public bool SearchEnabled
        {
            get { return ButtonSearch.Enabled; }
            set { ButtonSearch.Enabled = value; }
        }
        public bool SearchVisible
        {
            get { return ButtonSearch.Visible; }
            set { ButtonSearch.Visible = value; }
        }
        public string SearchToolTipText
        {
            get { return ButtonSearch.ToolTipText; }
            set { ButtonSearch.ToolTipText = value; }
        }

        public bool ImportExportEnabled
        {
            get { return ButtonImportExport.Enabled; }
            set { ButtonImportExport.Enabled = value; }
        }
        public bool ImportExportVisible
        {
            get { return ButtonImportExport.Visible; }
            set { ButtonImportExport.Visible = value; }
        }

        public string ImportExportToolTipText
        {
            get { return ButtonImportExport.ToolTipText; }
            set { ButtonImportExport.ToolTipText = value; }
        }

        public bool CleanFiltersEnabled
        {
            get { return ButtonCleanFilters.Enabled; }
            set { ButtonCleanFilters.Enabled = value; }
        }
        public bool CleanFiltersVisible
        {
            get { return ButtonCleanFilters.Visible; }
            set { ButtonCleanFilters.Visible = value; }
        }

        public string CleanFiltersToolTipText
        {
            get { return ButtonCleanFilters.ToolTipText; }
            set { ButtonCleanFilters.ToolTipText = value; }
        }

        public bool ExitEnabled
        {
            get { return ButtonExit.Enabled; }
            set { ButtonExit.Enabled = value; }
        }
        public bool ExitVisible
        {
            get { return ButtonExit.Visible; }
            set { ButtonExit.Visible = value; }
        }
        public string ExitToolTipText
        {
            get { return ButtonExit.ToolTipText; }
            set { ButtonExit.ToolTipText = value; }
        }

        private DisplaySettingsTheme _theme = DisplaySettingsTheme.Light;
        public DisplaySettingsTheme Theme
        {
            get
            {
                return _theme;
            }
            set
            {
                _theme = value;
                ApplyTheme();
            }
        }

        public MetroToolbarReport()
        {
            InitializeComponent();
            ApplyTheme();
        }

        private void ApplyTheme()
        {
            var _backColor = Color.White;
            switch (_theme)
            {
                case DisplaySettingsTheme.Default:
                case DisplaySettingsTheme.Light:
                    _backColor = Color.White;
                    break;
                case DisplaySettingsTheme.Dark:
                    _backColor = Color.FromArgb(17, 17, 17);
                    break;
            }

            this.ToolbarReport.BackColor = _backColor;
            this.BackColor = _backColor;
            this.ButtonDisplay.BackColor = _backColor;
            this.ButtonDisplay.Image = (_theme == DisplaySettingsTheme.Dark) ? Resources.monitorDark : Resources.monitorLight;

            this.ButtonPrint.BackColor = _backColor;
            this.ButtonPrint.Image = (_theme == DisplaySettingsTheme.Dark) ? Resources.printDark : Resources.printLight;

            this.ButtonRefresh.BackColor = _backColor;
            this.ButtonRefresh.Image = (_theme == DisplaySettingsTheme.Dark) ? Resources.refreshDark : Resources.refreshLight;

            this.ButtonSearch.BackColor = _backColor;
            this.ButtonSearch.Image = (_theme == DisplaySettingsTheme.Dark) ? Resources.searchDark : Resources.searchLight;

            this.ButtonImportExport.BackColor = _backColor;
            this.ButtonImportExport.Image = (_theme == DisplaySettingsTheme.Dark) ? Resources.importExportDark : Resources.importExportLight;

            this.ButtonCleanFilters.BackColor = _backColor;
            this.ButtonCleanFilters.Image = (_theme == DisplaySettingsTheme.Dark) ? Resources.clearFiltersDark : Resources.clearFiltersLight;

            this.ButtonExit.BackColor = _backColor;
            this.ButtonExit.Image = (_theme == DisplaySettingsTheme.Dark) ? Resources.closeDark : Resources.closeLight;
        }

        public void AnalizeKey(Keys keyCode)
        {
            switch (keyCode)
            {
                case Keys.F2:
                    if (ButtonDisplay.Enabled)
                        LaunchScreenEvent();
                    break;
                case Keys.F3:
                    if (ButtonPrint.Enabled)
                        LaunchPrinterEvent();
                    break;
                case Keys.F4:
                    if (ButtonCleanFilters.Enabled)
                        LaunchCleanFilterEvent();
                    break;
                case Keys.F12:
                    if (ButtonRefresh.Enabled)
                        LaunchRefreshEvent();
                    break;
                case Keys.F5:
                    if (ButtonSearch.Enabled)
                        LaunchSearchEvent();
                    break;
                case Keys.Escape:
                    if (ButtonExit.Enabled)
                        LaunchExitEvent();
                    break;
            }
        }

        protected virtual void OnMetroToolbarReportActionSelected(MetroToolbarReportClickEventArgs e)
        {
            //MetroToolbarReportClickEventHandler handler = ActionSelected;
            //if (handler != null)
            //{
            //    handler(this, e);
            //}
            ActionSelected?.Invoke(this, e);
        }

        private void LaunchScreenEvent()
        {
            OnMetroToolbarReportActionSelected(new MetroToolbarReportClickEventArgs(MetroToolbarReportAction.Screen));
        }
        private void LaunchPrinterEvent()
        {
            OnMetroToolbarReportActionSelected(new MetroToolbarReportClickEventArgs(MetroToolbarReportAction.Print));
        }

        private void LaunchRefreshEvent()
        {
            OnMetroToolbarReportActionSelected(new MetroToolbarReportClickEventArgs(MetroToolbarReportAction.Refresh));
        }

        private void LaunchSearchEvent()
        {
            OnMetroToolbarReportActionSelected(new MetroToolbarReportClickEventArgs(MetroToolbarReportAction.Search));
        }

        private void LaunchImporExportEvent()
        {
            OnMetroToolbarReportActionSelected(new MetroToolbarReportClickEventArgs(MetroToolbarReportAction.ImportExport));
        }

        private void LaunchCleanFilterEvent()
        {
            OnMetroToolbarReportActionSelected(new MetroToolbarReportClickEventArgs(MetroToolbarReportAction.CleanFilters));
        }

        private void LaunchExitEvent()
        {
            OnMetroToolbarReportActionSelected(new MetroToolbarReportClickEventArgs(MetroToolbarReportAction.Exit));
        }

        private void ButtonDisplay_Click(object sender, EventArgs e) => LaunchScreenEvent();
        private void ButtonPrint_Click(object sender, EventArgs e) => LaunchPrinterEvent();
        private void ButtonRefresh_Click(object sender, EventArgs e) => LaunchRefreshEvent();
        private void ButtonSearch_Click(object sender, EventArgs e) => LaunchSearchEvent();
        private void ButtonImportExport_Click(object sender, EventArgs e) => LaunchImporExportEvent();
        private void ButtonCleanFilters_Click(object sender, EventArgs e) => LaunchCleanFilterEvent();
        private void ButtonExit_Click(object sender, EventArgs e) => LaunchExitEvent();
        private void MetroToolbarReport_Resize(object sender, EventArgs e)
        {
            if (this.Height > 29)
                this.Height = 29;
        }
    }
}
