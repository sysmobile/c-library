﻿namespace SysWork.MetroControls.MetroToolbarsNET
{
    partial class MetroToolbarCRUDSmall
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            ToolbarCRUD = new System.Windows.Forms.ToolStrip();
            ButtonNew = new System.Windows.Forms.ToolStripButton();
            ButtonDelete = new System.Windows.Forms.ToolStripButton();
            ButtonRefresh = new System.Windows.Forms.ToolStripButton();
            ButtonSearch = new System.Windows.Forms.ToolStripButton();
            ButtonImportExport = new System.Windows.Forms.ToolStripButton();
            ButtonReport = new System.Windows.Forms.ToolStripButton();
            ButtonInitialize = new System.Windows.Forms.ToolStripButton();
            ButtonSave = new System.Windows.Forms.ToolStripButton();
            ButtonExit = new System.Windows.Forms.ToolStripButton();
            ToolbarCRUD.SuspendLayout();
            SuspendLayout();
            // 
            // ToolbarCRUD
            // 
            ToolbarCRUD.BackColor = System.Drawing.Color.FromArgb(17, 17, 17);
            ToolbarCRUD.Dock = System.Windows.Forms.DockStyle.None;
            ToolbarCRUD.GripMargin = new System.Windows.Forms.Padding(0);
            ToolbarCRUD.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            ToolbarCRUD.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { ButtonNew, ButtonDelete, ButtonRefresh, ButtonSearch, ButtonImportExport, ButtonReport, ButtonInitialize, ButtonSave, ButtonExit });
            ToolbarCRUD.Location = new System.Drawing.Point(0, 0);
            ToolbarCRUD.Name = "ToolbarCRUD";
            ToolbarCRUD.Padding = new System.Windows.Forms.Padding(0);
            ToolbarCRUD.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            ToolbarCRUD.Size = new System.Drawing.Size(240, 25);
            ToolbarCRUD.TabIndex = 2;
            ToolbarCRUD.Text = "toolStrip1";
            // 
            // ButtonNew
            // 
            ButtonNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            ButtonNew.Image = Properties.Resources.newLight;
            ButtonNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            ButtonNew.Name = "ButtonNew";
            ButtonNew.Size = new System.Drawing.Size(23, 22);
            ButtonNew.Text = "TsBtnNew";
            ButtonNew.ToolTipText = "Nuevo (F3)";
            ButtonNew.Click += ButtonNew_Click;
            // 
            // ButtonDelete
            // 
            ButtonDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            ButtonDelete.Image = Properties.Resources.deleteLight;
            ButtonDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            ButtonDelete.Name = "ButtonDelete";
            ButtonDelete.Size = new System.Drawing.Size(23, 22);
            ButtonDelete.Text = "toolStripButton1";
            ButtonDelete.ToolTipText = "Eliminar (F10)";
            ButtonDelete.Click += ButtonDelete_Click;
            // 
            // ButtonRefresh
            // 
            ButtonRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            ButtonRefresh.Image = Properties.Resources.refreshLight;
            ButtonRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            ButtonRefresh.Name = "ButtonRefresh";
            ButtonRefresh.Size = new System.Drawing.Size(23, 22);
            ButtonRefresh.Text = "toolStripButton2";
            ButtonRefresh.ToolTipText = "Refresh (F12)";
            ButtonRefresh.Click += ButtonRefresh_Click;
            // 
            // ButtonSearch
            // 
            ButtonSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            ButtonSearch.Image = Properties.Resources.searchLight;
            ButtonSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            ButtonSearch.Name = "ButtonSearch";
            ButtonSearch.Size = new System.Drawing.Size(23, 22);
            ButtonSearch.Text = "toolStripButton3";
            ButtonSearch.ToolTipText = "Buscar (F5)";
            ButtonSearch.Click += ButtonSearch_Click;
            // 
            // ButtonImportExport
            // 
            ButtonImportExport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            ButtonImportExport.Image = Properties.Resources.importExportLight;
            ButtonImportExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            ButtonImportExport.Name = "ButtonImportExport";
            ButtonImportExport.Size = new System.Drawing.Size(23, 22);
            ButtonImportExport.Text = "toolStripButton4";
            ButtonImportExport.ToolTipText = "Importar / Exportar";
            ButtonImportExport.Click += ButtonImportExport_Click;
            // 
            // ButtonReport
            // 
            ButtonReport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            ButtonReport.Image = Properties.Resources.reportLight;
            ButtonReport.ImageTransparentColor = System.Drawing.Color.Magenta;
            ButtonReport.Name = "ButtonReport";
            ButtonReport.Size = new System.Drawing.Size(23, 22);
            ButtonReport.Text = "toolStripButton6";
            ButtonReport.ToolTipText = "Reporte (F4)";
            ButtonReport.Click += ButtonReport_Click;
            // 
            // ButtonInitialize
            // 
            ButtonInitialize.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            ButtonInitialize.Image = Properties.Resources.initializeLight;
            ButtonInitialize.ImageTransparentColor = System.Drawing.Color.Magenta;
            ButtonInitialize.Name = "ButtonInitialize";
            ButtonInitialize.Size = new System.Drawing.Size(23, 22);
            ButtonInitialize.Text = "toolStripButton5";
            ButtonInitialize.ToolTipText = "Inicializar Formulario (ESC)";
            ButtonInitialize.Click += ButtonInitialize_Click;
            // 
            // ButtonSave
            // 
            ButtonSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            ButtonSave.Image = Properties.Resources.saveLight;
            ButtonSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            ButtonSave.Name = "ButtonSave";
            ButtonSave.Size = new System.Drawing.Size(23, 22);
            ButtonSave.Text = "toolStripButton7";
            ButtonSave.ToolTipText = "Grabar (F2)";
            ButtonSave.Click += ButtonSave_Click;
            // 
            // ButtonExit
            // 
            ButtonExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            ButtonExit.Image = Properties.Resources.closeLight;
            ButtonExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            ButtonExit.Name = "ButtonExit";
            ButtonExit.Size = new System.Drawing.Size(23, 22);
            ButtonExit.Text = "toolStripButton8";
            ButtonExit.ToolTipText = "Salir (ESC)";
            ButtonExit.Click += ButtonExit_Click;
            // 
            // MetroToolbarCRUDSmall
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BackColor = System.Drawing.Color.FromArgb(17, 17, 17);
            Controls.Add(ToolbarCRUD);
            Location = new System.Drawing.Point(-1, -1);
            Margin = new System.Windows.Forms.Padding(0);
            Name = "MetroToolbarCRUDSmall";
            Size = new System.Drawing.Size(252, 22);
            Resize += MetroToolbarCRUD_Resize;
            ToolbarCRUD.ResumeLayout(false);
            ToolbarCRUD.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.ToolStrip ToolbarCRUD;
        private System.Windows.Forms.ToolStripButton ButtonNew;
        private System.Windows.Forms.ToolStripButton ButtonDelete;
        private System.Windows.Forms.ToolStripButton ButtonRefresh;
        private System.Windows.Forms.ToolStripButton ButtonSearch;
        private System.Windows.Forms.ToolStripButton ButtonImportExport;
        private System.Windows.Forms.ToolStripButton ButtonInitialize;
        private System.Windows.Forms.ToolStripButton ButtonReport;
        private System.Windows.Forms.ToolStripButton ButtonSave;
        private System.Windows.Forms.ToolStripButton ButtonExit;
    }
}
