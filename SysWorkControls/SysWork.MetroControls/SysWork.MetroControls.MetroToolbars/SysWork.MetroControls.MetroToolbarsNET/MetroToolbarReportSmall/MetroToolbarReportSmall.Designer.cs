﻿namespace SysWork.MetroControls.MetroToolbarsNET
{
    partial class MetroToolbarReportSmall
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            ToolbarReport = new System.Windows.Forms.ToolStrip();
            ButtonDisplay = new System.Windows.Forms.ToolStripButton();
            ButtonPrint = new System.Windows.Forms.ToolStripButton();
            ButtonImportExport = new System.Windows.Forms.ToolStripButton();
            ButtonRefresh = new System.Windows.Forms.ToolStripButton();
            ButtonSearch = new System.Windows.Forms.ToolStripButton();
            ButtonCleanFilters = new System.Windows.Forms.ToolStripButton();
            ButtonExit = new System.Windows.Forms.ToolStripButton();
            ToolbarReport.SuspendLayout();
            SuspendLayout();
            // 
            // ToolbarReport
            // 
            ToolbarReport.BackColor = System.Drawing.Color.FromArgb(17, 17, 17);
            ToolbarReport.Dock = System.Windows.Forms.DockStyle.None;
            ToolbarReport.GripMargin = new System.Windows.Forms.Padding(0);
            ToolbarReport.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            ToolbarReport.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { ButtonDisplay, ButtonPrint, ButtonImportExport, ButtonRefresh, ButtonSearch, ButtonCleanFilters, ButtonExit });
            ToolbarReport.Location = new System.Drawing.Point(0, 0);
            ToolbarReport.Name = "ToolbarReport";
            ToolbarReport.Padding = new System.Windows.Forms.Padding(0);
            ToolbarReport.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            ToolbarReport.Size = new System.Drawing.Size(163, 25);
            ToolbarReport.TabIndex = 2;
            ToolbarReport.Text = "toolStrip1";
            // 
            // ButtonDisplay
            // 
            ButtonDisplay.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            ButtonDisplay.Image = Properties.Resources.monitorLight;
            ButtonDisplay.ImageTransparentColor = System.Drawing.Color.Magenta;
            ButtonDisplay.Name = "ButtonDisplay";
            ButtonDisplay.Size = new System.Drawing.Size(23, 22);
            ButtonDisplay.ToolTipText = "Visualizar por Pantalla (F2)";
            ButtonDisplay.Click += ButtonDisplay_Click;
            // 
            // ButtonPrint
            // 
            ButtonPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            ButtonPrint.Image = Properties.Resources.printLight;
            ButtonPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            ButtonPrint.Name = "ButtonPrint";
            ButtonPrint.Size = new System.Drawing.Size(23, 22);
            ButtonPrint.ToolTipText = "Imprimir (F3)";
            ButtonPrint.Click += ButtonPrint_Click;
            // 
            // ButtonImportExport
            // 
            ButtonImportExport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            ButtonImportExport.Image = Properties.Resources.importExportLight;
            ButtonImportExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            ButtonImportExport.Name = "ButtonImportExport";
            ButtonImportExport.Size = new System.Drawing.Size(23, 22);
            ButtonImportExport.Text = "toolStripButton4";
            ButtonImportExport.ToolTipText = "Exportar";
            ButtonImportExport.Click += ButtonImportExport_Click;
            // 
            // ButtonRefresh
            // 
            ButtonRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            ButtonRefresh.Image = Properties.Resources.refreshLight;
            ButtonRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            ButtonRefresh.Name = "ButtonRefresh";
            ButtonRefresh.Size = new System.Drawing.Size(23, 22);
            ButtonRefresh.Text = "toolStripButton2";
            ButtonRefresh.ToolTipText = "Refresh (F12)";
            ButtonRefresh.Click += ButtonRefresh_Click;
            // 
            // ButtonSearch
            // 
            ButtonSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            ButtonSearch.Image = Properties.Resources.searchLight;
            ButtonSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            ButtonSearch.Name = "ButtonSearch";
            ButtonSearch.Size = new System.Drawing.Size(23, 22);
            ButtonSearch.Text = "toolStripButton3";
            ButtonSearch.ToolTipText = "Buscar (F5)";
            ButtonSearch.Click += ButtonSearch_Click;
            // 
            // ButtonCleanFilters
            // 
            ButtonCleanFilters.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            ButtonCleanFilters.Image = Properties.Resources.clearFiltersLight;
            ButtonCleanFilters.ImageTransparentColor = System.Drawing.Color.Magenta;
            ButtonCleanFilters.Name = "ButtonCleanFilters";
            ButtonCleanFilters.Size = new System.Drawing.Size(23, 22);
            ButtonCleanFilters.Text = "toolStripButton6";
            ButtonCleanFilters.ToolTipText = "Limpiar Filtros";
            ButtonCleanFilters.Click += ButtonCleanFilters_Click;
            // 
            // ButtonExit
            // 
            ButtonExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            ButtonExit.Image = Properties.Resources.closeLight;
            ButtonExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            ButtonExit.Name = "ButtonExit";
            ButtonExit.Size = new System.Drawing.Size(23, 22);
            ButtonExit.Text = "toolStripButton8";
            ButtonExit.ToolTipText = "Salir (ESC)";
            ButtonExit.Click += ButtonExit_Click;
            // 
            // MetroToolbarReportSmall
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BackColor = System.Drawing.Color.FromArgb(17, 17, 17);
            Controls.Add(ToolbarReport);
            Location = new System.Drawing.Point(-1, -1);
            Margin = new System.Windows.Forms.Padding(0);
            Name = "MetroToolbarReportSmall";
            Size = new System.Drawing.Size(191, 22);
            Resize += MetroToolbarReport_Resize;
            ToolbarReport.ResumeLayout(false);
            ToolbarReport.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.ToolStrip ToolbarReport;
        private System.Windows.Forms.ToolStripButton ButtonDisplay;
        private System.Windows.Forms.ToolStripButton ButtonPrint;
        private System.Windows.Forms.ToolStripButton ButtonRefresh;
        private System.Windows.Forms.ToolStripButton ButtonSearch;
        private System.Windows.Forms.ToolStripButton ButtonImportExport;
        private System.Windows.Forms.ToolStripButton ButtonCleanFilters;
        private System.Windows.Forms.ToolStripButton ButtonExit;
    }
}
