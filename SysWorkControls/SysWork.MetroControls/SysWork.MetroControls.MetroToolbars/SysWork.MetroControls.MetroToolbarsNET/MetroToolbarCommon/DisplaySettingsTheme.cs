﻿namespace SysWork.MetroControls.MetroToolbarsNET
{
    public enum DisplaySettingsTheme
    {
        Default = 0,
        Light,
        Dark
    }
}
