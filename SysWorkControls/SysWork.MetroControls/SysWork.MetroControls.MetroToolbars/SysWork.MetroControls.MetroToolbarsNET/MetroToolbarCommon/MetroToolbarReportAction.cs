﻿namespace SysWork.MetroControls.MetroToolbarsNET
{
    /// <summary>
    /// Action from MetroToolbarCRUD
    /// </summary>
    public enum MetroToolbarReportAction
    {
        Screen,
        Print,
        Refresh,
        Search,
        ImportExport,
        CleanFilters,
        Exit
    }
}
