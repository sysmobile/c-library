﻿using System;

namespace SysWork.MetroControls.MetroToolbarsNET
{
    public class MetroToolbarReportClickEventArgs : EventArgs
    {
        public MetroToolbarReportAction Action;

        public MetroToolbarReportClickEventArgs()
        {

        }
        public MetroToolbarReportClickEventArgs(MetroToolbarReportAction action)
        {
            Action = action;
        }   
    }
}
