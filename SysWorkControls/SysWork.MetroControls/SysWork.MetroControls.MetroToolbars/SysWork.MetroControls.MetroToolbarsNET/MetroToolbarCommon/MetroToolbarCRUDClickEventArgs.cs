﻿using System;

namespace SysWork.MetroControls.MetroToolbarsNET
{
    public class MetroToolbarCRUDClickEventArgs : EventArgs
    {
        public MetroToolbarCRUDAction Action;
    }
}