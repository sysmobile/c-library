﻿namespace SysWork.MetroControls.MetroToolbarsNET
{
    public enum DisplaySettingsStyle
    {
        Default,
        Black,
        White,
        Silver,
        Blue,
        Green,
        Lime,
        Teal,
        Orange,
        Brown,
        Pink,
        Magenta,
        Purple,
        Red,
        Yellow,
        Custom
    }
}
