﻿namespace SysWork.MetroControls.MetroToolbars
{
    public enum DisplaySettingsTheme
    {
        Default = 0,
        Light,
        Dark
    }
}
