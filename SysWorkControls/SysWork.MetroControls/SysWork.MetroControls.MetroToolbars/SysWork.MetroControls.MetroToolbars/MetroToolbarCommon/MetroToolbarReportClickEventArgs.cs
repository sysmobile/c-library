﻿using System;

namespace SysWork.MetroControls.MetroToolbars
{
    public class MetroToolbarReportClickEventArgs : EventArgs
    {
        public MetroToolbarReportAction Action;

        public MetroToolbarReportClickEventArgs()
        {

        }
        public MetroToolbarReportClickEventArgs(MetroToolbarReportAction action)
        {
            Action = action;
        }   
    }
}
