﻿using System;

namespace SysWork.MetroControls.MetroToolbars
{
    public class MetroToolbarCRUDClickEventArgs : EventArgs
    {
        public MetroToolbarCRUDAction Action;
    }
}