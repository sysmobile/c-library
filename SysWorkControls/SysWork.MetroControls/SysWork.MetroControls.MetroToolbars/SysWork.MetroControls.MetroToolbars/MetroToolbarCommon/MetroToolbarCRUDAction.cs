﻿namespace SysWork.MetroControls.MetroToolbars
{
    /// <summary>
    /// Action from MetroToolbarCRUD
    /// </summary>
    public enum MetroToolbarCRUDAction
    {
        New,
        Delete,
        Refresh,
        Search,
        ImportExport,
        Report,
        Initialize,
        Save,
        Exit
    }
}