﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using SysWork.MetroControls.MetroToolbars.Properties;

namespace SysWork.MetroControls.MetroToolbars
{

    [DefaultEvent("ActionSelected")]
    public partial class MetroToolbarCRUD : UserControl
    {
        /// <summary>
        /// Occurs when an action is selected.
        /// </summary>
        public event MetroToolbarCRUDClickEventHandler ActionSelected;
        public delegate void MetroToolbarCRUDClickEventHandler(Object sender, MetroToolbarCRUDClickEventArgs e);

        public bool NewEnabled
        {
            get { return ButtonNew.Enabled; }
            set { ButtonNew.Enabled = value; }
        }
        public bool NewVisible
        {
            get { return ButtonNew.Visible; }
            set { ButtonNew.Visible = value; }
        }
        public string NewToolTipText
        {
            get { return ButtonNew.ToolTipText; }
            set { ButtonNew.ToolTipText = value; }
        }
        public bool DeleteEnabled
        {
            get { return ButtonDelete.Enabled; }
            set { ButtonDelete.Enabled = value; }
        }
        public bool DeleteVisible
        {
            get { return ButtonDelete.Visible; }
            set { ButtonDelete.Visible = value; }
        }
        public string DeleteToolTipText
        {
            get { return ButtonDelete.ToolTipText; }
            set { ButtonDelete.ToolTipText = value; }
        }

        public bool RefreshEnabled
        {
            get { return ButtonRefresh.Enabled; }
            set { ButtonRefresh.Enabled = value; }
        }
        public bool RefreshVisible
        {
            get { return ButtonRefresh.Visible; }
            set { ButtonRefresh.Visible = value; }
        }
        public string RefreshToolTipText
        {
            get { return ButtonRefresh.ToolTipText; }
            set { ButtonRefresh.ToolTipText = value; }
        }

        public bool SearchEnabled
        {
            get { return ButtonSearch.Enabled; }
            set { ButtonSearch.Enabled = value; }
        }
        public bool SearchVisible
        {
            get { return ButtonSearch.Visible; }
            set { ButtonSearch.Visible = value; }
        }
        public string SearchToolTipText
        {
            get { return ButtonSearch.ToolTipText; }
            set { ButtonSearch.ToolTipText = value; }
        }

        public bool ImportExportEnabled
        {
            get { return ButtonImportExport.Enabled; }
            set { ButtonImportExport.Enabled = value; }
        }
        public bool ImportExportVisible
        {
            get { return ButtonImportExport.Visible; }
            set { ButtonImportExport.Visible = value; }
        }
        public string ImportExportToolTipText
        {
            get { return ButtonImportExport.ToolTipText; }
            set { ButtonImportExport.ToolTipText = value; }
        }
        public bool ReportEnabled
        {
            get { return ButtonReport.Enabled; }
            set { ButtonReport.Enabled = value; }
        }
        public bool ReportVisible
        {
            get { return ButtonReport.Visible; }
            set { ButtonReport.Visible = value; }
        }
        public string ReportToolTipText
        {
            get { return ButtonReport.ToolTipText; }
            set { ButtonReport.ToolTipText = value; }
        }
        public bool InitializeEnabled
        {
            get { return ButtonInitialize.Enabled; }
            set { ButtonInitialize.Enabled = value; }
        }
        public bool InitializeVisible
        {
            get { return ButtonInitialize.Visible; }
            set { ButtonInitialize.Visible = value; }
        }
        public string InitializeToolTipText
        {
            get { return ButtonInitialize.ToolTipText; }
            set { ButtonInitialize.ToolTipText = value; }
        }
        public bool SaveEnabled
        {
            get { return ButtonSave.Enabled; }
            set { ButtonSave.Enabled = value; }
        }
        public bool SaveVisible
        {
            get { return ButtonSave.Visible; }
            set { ButtonSave.Visible = value; }
        }
        public string SaveToolTipText
        {
            get { return ButtonSave.ToolTipText; }
            set { ButtonSave.ToolTipText = value; }
        }

        public bool ExitEnabled
        {
            get { return ButtonExit.Enabled; }
            set { ButtonExit.Enabled = value; }
        }
        public bool ExitVisible
        {
            get { return ButtonExit.Visible; }
            set { ButtonExit.Visible = value; }
        }
        public string ExitToolTipText
        {
            get { return ButtonExit.ToolTipText; }
            set { ButtonExit.ToolTipText = value; }
        }

        private DisplaySettingsTheme _theme = DisplaySettingsTheme.Light;
        
        public DisplaySettingsTheme Theme
        {
            get
            {
                return _theme;
            }
            set
            {
                _theme = value;
                ApplyTheme();
            }
        }


        public MetroToolbarCRUD()
        {
            InitializeComponent();
            ApplyTheme();
        }

        private void ApplyTheme()
        {
            var _backColor = Color.White;
            switch (_theme)
            {
                case DisplaySettingsTheme.Default:
                case DisplaySettingsTheme.Light:
                    _backColor = Color.White;
                    break;
                case DisplaySettingsTheme.Dark:
                    _backColor = Color.FromArgb(17, 17, 17);
                    break;
            }

            this.ToolbarCRUD.BackColor = _backColor;
            this.BackColor = _backColor;
            this.ButtonNew.BackColor = _backColor;
            this.ButtonNew.Image = (_theme == DisplaySettingsTheme.Dark) ? Resources.newDark : Resources.newLight;

            this.ButtonDelete.BackColor = _backColor;
            this.ButtonDelete.Image = (_theme == DisplaySettingsTheme.Dark) ? Resources.deleteDark : Resources.deleteLight;

            this.ButtonRefresh.BackColor = _backColor;
            this.ButtonRefresh.Image = (_theme == DisplaySettingsTheme.Dark) ? Resources.refreshDark : Resources.refreshLight;

            this.ButtonSearch.BackColor = _backColor;
            this.ButtonSearch.Image = (_theme == DisplaySettingsTheme.Dark) ? Resources.searchDark : Resources.searchLight;

            this.ButtonImportExport.BackColor = _backColor;
            this.ButtonImportExport.Image = (_theme == DisplaySettingsTheme.Dark) ? Resources.importExportDark : Resources.importExportLight;

            this.ButtonReport.BackColor = _backColor;
            this.ButtonReport.Image = (_theme == DisplaySettingsTheme.Dark) ? Resources.reportDark : Resources.reportLight;

            this.ButtonInitialize.BackColor = _backColor;
            this.ButtonInitialize.Image = (_theme == DisplaySettingsTheme.Dark) ? Resources.initializeDark : Resources.initializeLight;

            this.ButtonSave.BackColor = _backColor;
            this.ButtonSave.Image = (_theme == DisplaySettingsTheme.Dark) ? Resources.saveDark : Resources.saveLight;

            this.ButtonExit.BackColor = _backColor;
            this.ButtonExit.Image = (_theme == DisplaySettingsTheme.Dark) ? Resources.closeDark : Resources.closeLight;
        }

        public void AnalizeKey(Keys keyCode)
        {

            switch (keyCode)
            {
                case Keys.F3:
                    if (ButtonNew.Enabled)
                        ThrowNewEvent();
                    break;
                case Keys.F4:
                    if (ButtonReport.Enabled)
                        ThrowReportEvent();
                    break;
                case Keys.F10:
                    if (ButtonDelete.Enabled)
                        ThrowDeleteEvent();
                    break;
                case Keys.F12:
                    if (ButtonRefresh.Enabled)
                        ThrowRefreshEvent();
                    break;
                case Keys.F5:
                    if (ButtonSearch.Enabled)
                        ThrowSearchEvent();
                    break;

                case Keys.F2:
                    if (ButtonSave.Enabled)
                        ThrowSaveEvent();
                    break;

                case Keys.Escape:
                    if (ButtonInitialize.Enabled)
                        ThrowInitializeEvent();
                    else if (ButtonExit.Enabled)
                        ThrowExitEvent();

                    break;
            }
        }

        protected virtual void OnMetroToolbarCRUDActionSelected(MetroToolbarCRUDClickEventArgs e)
        {
            MetroToolbarCRUDClickEventHandler handler = ActionSelected;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void ThrowNewEvent()
        {
            var args = new MetroToolbarCRUDClickEventArgs();
            args.Action = MetroToolbarCRUDAction.New;
            OnMetroToolbarCRUDActionSelected(args);
        }
        private void ThrowDeleteEvent()
        {
            var args = new MetroToolbarCRUDClickEventArgs();
            args.Action = MetroToolbarCRUDAction.Delete;

            OnMetroToolbarCRUDActionSelected(args);
        }

        private void ThrowRefreshEvent()
        {
            var args = new MetroToolbarCRUDClickEventArgs();
            args.Action = MetroToolbarCRUDAction.Refresh;
            OnMetroToolbarCRUDActionSelected(args);
        }

        private void ThrowSearchEvent()
        {
            var args = new MetroToolbarCRUDClickEventArgs();
            args.Action = MetroToolbarCRUDAction.Search;
            OnMetroToolbarCRUDActionSelected(args);
        }

        private void ThrowImporExportEvent()
        {
            var args = new MetroToolbarCRUDClickEventArgs();
            args.Action = MetroToolbarCRUDAction.ImportExport;
            OnMetroToolbarCRUDActionSelected(args);
        }

        private void ThrowReportEvent()
        {
            var args = new MetroToolbarCRUDClickEventArgs();
            args.Action = MetroToolbarCRUDAction.Report;
            OnMetroToolbarCRUDActionSelected(args);
        }

        private void ThrowInitializeEvent()
        {
            var args = new MetroToolbarCRUDClickEventArgs();
            args.Action = MetroToolbarCRUDAction.Initialize;
            OnMetroToolbarCRUDActionSelected(args);
        }

        private void ThrowSaveEvent()
        {
            var args = new MetroToolbarCRUDClickEventArgs();
            args.Action = MetroToolbarCRUDAction.Save;
            OnMetroToolbarCRUDActionSelected(args);
        }

        private void ThrowExitEvent()
        {
            var args = new MetroToolbarCRUDClickEventArgs();
            args.Action = MetroToolbarCRUDAction.Exit;
            OnMetroToolbarCRUDActionSelected(args);
        }

        private void ButtonNew_Click(object sender, EventArgs e)=>ThrowNewEvent();
        private void ButtonDelete_Click(object sender, EventArgs e)=>ThrowDeleteEvent();
        private void ButtonRefresh_Click(object sender, EventArgs e)=>ThrowRefreshEvent();
        private void ButtonSearch_Click(object sender, EventArgs e)=>ThrowSearchEvent();
        private void ButtonImportExport_Click(object sender, EventArgs e)=>ThrowImporExportEvent();
        private void ButtonReport_Click(object sender, EventArgs e)=>ThrowReportEvent();
        private void ButtonInitialize_Click(object sender, EventArgs e)=>ThrowInitializeEvent();
        private void ButtonSave_Click(object sender, EventArgs e) => ThrowSaveEvent();
        private void ButtonExit_Click(object sender, EventArgs e) => ThrowExitEvent();
        private void MetroToolbarCRUD_Resize(object sender, EventArgs e)
        {
            if (this.Height > 29)
                this.Height = 29;
        }
    }
}