﻿namespace SysWork.MetroControls.MetroToolbars
{
    partial class MetroToolbarDisplaySettings
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.ToolbarDisplaySettings = new System.Windows.Forms.ToolStrip();
            this.ButtonChangeTheme = new System.Windows.Forms.ToolStripButton();
            this.TsCmbTheme = new System.Windows.Forms.ToolStripComboBox();
            this.ButtonChangeStyle = new System.Windows.Forms.ToolStripButton();
            this.TsCmbStyles = new System.Windows.Forms.ToolStripComboBox();
            this.ToolbarDisplaySettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolbarDisplaySettings
            // 
            this.ToolbarDisplaySettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.ToolbarDisplaySettings.Dock = System.Windows.Forms.DockStyle.None;
            this.ToolbarDisplaySettings.GripMargin = new System.Windows.Forms.Padding(0);
            this.ToolbarDisplaySettings.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ToolbarDisplaySettings.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.ToolbarDisplaySettings.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ButtonChangeTheme,
            this.TsCmbTheme,
            this.ButtonChangeStyle,
            this.TsCmbStyles});
            this.ToolbarDisplaySettings.Location = new System.Drawing.Point(0, 0);
            this.ToolbarDisplaySettings.Name = "ToolbarDisplaySettings";
            this.ToolbarDisplaySettings.Padding = new System.Windows.Forms.Padding(0);
            this.ToolbarDisplaySettings.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.ToolbarDisplaySettings.Size = new System.Drawing.Size(58, 31);
            this.ToolbarDisplaySettings.TabIndex = 2;
            // 
            // ButtonChangeTheme
            // 
            this.ButtonChangeTheme.CheckOnClick = true;
            this.ButtonChangeTheme.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButtonChangeTheme.Image = global::SysWork.MetroControls.MetroToolbars.Properties.Resources.changeThemeLight;
            this.ButtonChangeTheme.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButtonChangeTheme.Name = "ButtonChangeTheme";
            this.ButtonChangeTheme.Size = new System.Drawing.Size(28, 28);
            this.ButtonChangeTheme.ToolTipText = "Cambiar Tema";
            this.ButtonChangeTheme.CheckedChanged += new System.EventHandler(this.ButtonChangeTheme_CheckedChanged);
            // 
            // TsCmbTheme
            // 
            this.TsCmbTheme.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.TsCmbTheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TsCmbTheme.ForeColor = System.Drawing.Color.Black;
            this.TsCmbTheme.Items.AddRange(new object[] {
            "Light",
            "Dark"});
            this.TsCmbTheme.Name = "TsCmbTheme";
            this.TsCmbTheme.Size = new System.Drawing.Size(75, 31);
            this.TsCmbTheme.Visible = false;
            this.TsCmbTheme.SelectedIndexChanged += new System.EventHandler(this.TsCmbTheme_SelectedIndexChanged);
            // 
            // ButtonChangeStyle
            // 
            this.ButtonChangeStyle.CheckOnClick = true;
            this.ButtonChangeStyle.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButtonChangeStyle.Image = global::SysWork.MetroControls.MetroToolbars.Properties.Resources.changeStyleLight;
            this.ButtonChangeStyle.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButtonChangeStyle.Name = "ButtonChangeStyle";
            this.ButtonChangeStyle.Size = new System.Drawing.Size(28, 28);
            this.ButtonChangeStyle.ToolTipText = "Cambiar Estilo";
            this.ButtonChangeStyle.CheckedChanged += new System.EventHandler(this.ButtonChangeStyle_CheckedChanged);
            // 
            // TsCmbStyles
            // 
            this.TsCmbStyles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.TsCmbStyles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TsCmbStyles.Items.AddRange(new object[] {
            "Blue",
            "Black",
            "White",
            "MetroColors",
            "Green",
            "Lime",
            "Teal",
            "Orange",
            "Brown",
            "Pink",
            "Magenta",
            "Purple",
            "Red",
            "Yellow"});
            this.TsCmbStyles.Name = "TsCmbStyles";
            this.TsCmbStyles.Size = new System.Drawing.Size(75, 31);
            this.TsCmbStyles.Visible = false;
            this.TsCmbStyles.SelectedIndexChanged += new System.EventHandler(this.TsCmbStyles_SelectedIndexChanged);
            // 
            // MetroToolbarDisplaySettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.Controls.Add(this.ToolbarDisplaySettings);
            this.Location = new System.Drawing.Point(-1, -1);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "MetroToolbarDisplaySettings";
            this.Size = new System.Drawing.Size(243, 31);
            this.Load += new System.EventHandler(this.MetroToolbarDisplaySettings_Load);
            this.Resize += new System.EventHandler(this.MetroToolbarDisplaySettings_Resize);
            this.ToolbarDisplaySettings.ResumeLayout(false);
            this.ToolbarDisplaySettings.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip ToolbarDisplaySettings;
        private System.Windows.Forms.ToolStripButton ButtonChangeTheme;
        private System.Windows.Forms.ToolStripButton ButtonChangeStyle;
        private System.Windows.Forms.ToolStripComboBox TsCmbTheme;
        private System.Windows.Forms.ToolStripComboBox TsCmbStyles;
    }
}
