﻿using MetroFramework;
using MetroFramework.Components;
using MetroFramework.Forms;
using System.Collections.Generic;

namespace SysWork.MetroControls.MetroToolbars
{
    public class DisplaySettingsManager
    {
        private DisplaySettingsTheme _metroTheme;
        public DisplaySettingsTheme MetroTheme
        {
            get { return _metroTheme; }
            set { _metroTheme = value; }
        }

        private DisplaySettingsStyle _metroStyle;
        public DisplaySettingsStyle MetroStyle
        {
            get { return _metroStyle; }
            set { _metroStyle = value; }
        }

        public List<MetroToolbarCRUD> MetroToolbarsCRUD { get; set; } = null;
        public List<MetroToolbarCRUDSmall> MetroToolbarsCRUDSmall { get; set; } = null;
        public List<MetroToolbarReport> MetroToolbarsReport { get; set; } = null;
        public List<MetroToolbarReportSmall> MetroToolbarsReportSmall { get; set; } = null;
        public List<MetroToolbarDisplaySettings> MetroToolbarsDisplaySettings { get; set; } = null;
        public MetroRendererManager MetroRendererManager { get; set; } = null;
        public MetroStyleExtender MetroStyleExtender { get; set; } = null;

        private readonly MetroStyleManager _metroStyleManager;
        private readonly MetroForm _metroForm;

        public DisplaySettingsManager(MetroForm metroForm, MetroStyleManager metroStyleManager)
        {
            _metroStyleManager = metroStyleManager;
            _metroForm = metroForm;

            MetroToolbarsCRUD = new List<MetroToolbarCRUD>();
            MetroToolbarsReport = new List<MetroToolbarReport>();
            MetroToolbarsDisplaySettings = new List<MetroToolbarDisplaySettings>();
            MetroToolbarsCRUDSmall = new List<MetroToolbarCRUDSmall>();
            MetroToolbarsReportSmall = new List<MetroToolbarReportSmall>();

            _metroStyleManager.Owner = _metroForm;
        }

        public void Apply()
        {
            foreach (var metroToolbarCRUD in MetroToolbarsCRUD)
                metroToolbarCRUD.Theme = _metroTheme;
            
            foreach (var metroToolbarCRUDSmall in MetroToolbarsCRUDSmall)
                metroToolbarCRUDSmall.Theme = _metroTheme;

            foreach (var metroToolbarsReport in MetroToolbarsReport)
                metroToolbarsReport.Theme = _metroTheme;
            
            foreach (var metroToolbarsReportSmall in MetroToolbarsReportSmall)
                metroToolbarsReportSmall.Theme = _metroTheme;

            foreach (MetroToolbarDisplaySettings metroToolbarsDisplaySettings in MetroToolbarsDisplaySettings)
            {
                metroToolbarsDisplaySettings.Theme = _metroTheme;
                metroToolbarsDisplaySettings.Style = _metroStyle;
            }

            _metroStyleManager.Theme = (MetroThemeStyle)_metroTheme;
            _metroStyleManager.Style = (MetroColorStyle)_metroStyle;

            _metroForm.Theme = _metroStyleManager.Theme;
            _metroForm.Style = _metroStyleManager.Style;

            if (MetroRendererManager != null)
            {
                MetroRendererManager.Theme = _metroStyleManager.Theme;
                MetroRendererManager.Style = _metroStyleManager.Style;
            }
            if (MetroStyleExtender != null)
            {
                MetroStyleExtender.Theme = _metroStyleManager.Theme;
                MetroStyleExtender.Style = _metroStyleManager.Style;
            }

            _metroForm.Refresh();
        }
    }
}