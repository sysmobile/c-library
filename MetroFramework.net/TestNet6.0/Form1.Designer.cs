﻿namespace TestNet6._0
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            DataGridViewCellStyle dataGridViewCellStyle4 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle5 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle6 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle3 = new DataGridViewCellStyle();
            metroLabel1 = new MetroFramework.Controls.MetroLabel();
            metroTextBox1 = new MetroFramework.Controls.MetroTextBox();
            metroProgressBar1 = new MetroFramework.Controls.MetroProgressBar();
            metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            tabPage3 = new MetroFramework.Controls.MetroTabPage();
            metroTextBoxTime5 = new MetroFramework.Controls.MetroTextBoxTime();
            metroTextBoxTime4 = new MetroFramework.Controls.MetroTextBoxTime();
            metroTextBoxTime3 = new MetroFramework.Controls.MetroTextBoxTime();
            metroTextBoxTime2 = new MetroFramework.Controls.MetroTextBoxTime();
            metroTextBox2 = new MetroFramework.Controls.MetroTextBox();
            tabPage1 = new MetroFramework.Controls.MetroTabPage();
            htmlLabel1 = new MetroFramework.Drawing.Html.HtmlLabel();
            metroButtonStyled1 = new MetroFramework.Controls.MetroButtonStyled();
            metroStyleManager1 = new MetroFramework.Components.MetroStyleManager(components);
            tabPage2 = new MetroFramework.Controls.MetroTabPage();
            button1 = new Button();
            metroButton1 = new MetroFramework.Controls.MetroButton();
            metroMonthCalendar1 = new MetroFramework.Controls.MetroMonthCalendar();
            metroLabel2 = new MetroFramework.Controls.MetroLabel();
            metroDomainUpDown1 = new MetroFramework.Controls.MetroDomainUpDown();
            metroComboBox1 = new MetroFramework.Controls.MetroComboBox();
            metroTextBoxTime1 = new MetroFramework.Controls.MetroTextBoxTime();
            metroStyleExtender1 = new MetroFramework.Components.MetroStyleExtender(components);
            metroTextBoxNumeric1 = new MetroFramework.Controls.MetroTextBoxNumeric();
            metroGrid1 = new MetroFramework.Controls.MetroGrid();
            metroTextBoxDate1 = new MetroFramework.Controls.MetroTextBoxDate();
            metroTextBoxDate2 = new MetroFramework.Controls.MetroTextBoxDate();
            metroGrid2 = new MetroFramework.Controls.MetroGrid();
            metroTextBoxTime6 = new MetroFramework.Controls.MetroTextBoxTime();
            metroTabControl1.SuspendLayout();
            tabPage3.SuspendLayout();
            tabPage1.SuspendLayout();
            htmlLabel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)metroStyleManager1).BeginInit();
            tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)metroGrid1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)metroGrid2).BeginInit();
            SuspendLayout();
            // 
            // metroLabel1
            // 
            metroLabel1.AutoSize = true;
            metroLabel1.Location = new Point(179, 171);
            metroLabel1.Name = "metroLabel1";
            metroLabel1.Size = new Size(81, 19);
            metroLabel1.TabIndex = 0;
            metroLabel1.Text = "metroLabel1";
            // 
            // metroTextBox1
            // 
            // 
            // 
            // 
            metroTextBox1.CustomButton.Image = null;
            metroTextBox1.CustomButton.Location = new Point(85, 1);
            metroTextBox1.CustomButton.Name = "";
            metroTextBox1.CustomButton.Size = new Size(11, 11);
            metroTextBox1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            metroTextBox1.CustomButton.TabIndex = 1;
            metroTextBox1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            metroTextBox1.CustomButton.UseSelectable = true;
            metroTextBox1.CustomButton.Visible = false;
            metroTextBox1.Lines = new string[] { "metroTextBox1" };
            metroTextBox1.Location = new Point(299, 175);
            metroTextBox1.MaxLength = 32767;
            metroTextBox1.Name = "metroTextBox1";
            metroTextBox1.PasswordChar = '\0';
            metroTextBox1.ScrollBars = ScrollBars.None;
            metroTextBox1.SelectedText = "";
            metroTextBox1.SelectionLength = 0;
            metroTextBox1.SelectionStart = 0;
            metroTextBox1.ShortcutsEnabled = true;
            metroTextBox1.Size = new Size(97, 13);
            metroTextBox1.TabIndex = 1;
            metroTextBox1.Text = "metroTextBox1";
            metroTextBox1.UseSelectable = true;
            metroTextBox1.WaterMarkColor = Color.FromArgb(109, 109, 109);
            metroTextBox1.WaterMarkFont = new Font("Segoe UI", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            // 
            // metroProgressBar1
            // 
            metroProgressBar1.Location = new Point(300, 81);
            metroProgressBar1.Name = "metroProgressBar1";
            metroProgressBar1.Size = new Size(139, 19);
            metroProgressBar1.TabIndex = 3;
            // 
            // metroTabControl1
            // 
            metroTabControl1.Controls.Add(tabPage3);
            metroTabControl1.Controls.Add(tabPage1);
            metroTabControl1.Controls.Add(tabPage2);
            metroTabControl1.Location = new Point(23, 63);
            metroTabControl1.Name = "metroTabControl1";
            metroTabControl1.Padding = new Point(6, 8);
            metroTabControl1.SelectedIndex = 0;
            metroTabControl1.Size = new Size(513, 290);
            metroTabControl1.TabIndex = 4;
            metroTabControl1.UseSelectable = true;
            // 
            // tabPage3
            // 
            tabPage3.Controls.Add(metroTextBoxTime6);
            tabPage3.Controls.Add(metroTextBoxTime5);
            tabPage3.Controls.Add(metroTextBoxTime4);
            tabPage3.Controls.Add(metroTextBoxTime3);
            tabPage3.Controls.Add(metroTextBoxTime2);
            tabPage3.Controls.Add(metroTextBox2);
            tabPage3.HorizontalScrollbarBarColor = true;
            tabPage3.HorizontalScrollbarHighlightOnWheel = false;
            tabPage3.HorizontalScrollbarSize = 10;
            tabPage3.Location = new Point(4, 38);
            tabPage3.Name = "tabPage3";
            tabPage3.Size = new Size(505, 248);
            tabPage3.TabIndex = 2;
            tabPage3.Text = "tabPage3";
            tabPage3.VerticalScrollbarBarColor = true;
            tabPage3.VerticalScrollbarHighlightOnWheel = false;
            tabPage3.VerticalScrollbarSize = 10;
            // 
            // metroTextBoxTime5
            // 
            // 
            // 
            // 
            metroTextBoxTime5.CustomButton.Image = null;
            metroTextBoxTime5.CustomButton.Location = new Point(112, 2);
            metroTextBoxTime5.CustomButton.Name = "";
            metroTextBoxTime5.CustomButton.Size = new Size(19, 19);
            metroTextBoxTime5.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            metroTextBoxTime5.CustomButton.TabIndex = 1;
            metroTextBoxTime5.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            metroTextBoxTime5.CustomButton.UseSelectable = true;
            metroTextBoxTime5.CustomButton.Visible = false;
            metroTextBoxTime5.Location = new Point(78, 113);
            metroTextBoxTime5.Name = "metroTextBoxTime5";
            metroTextBoxTime5.PasswordChar = '\0';
            metroTextBoxTime5.PromptText = "12:50";
            metroTextBoxTime5.ScrollBars = ScrollBars.None;
            metroTextBoxTime5.SelectedText = "";
            metroTextBoxTime5.SelectionLength = 0;
            metroTextBoxTime5.SelectionStart = 0;
            metroTextBoxTime5.ShortcutsEnabled = true;
            metroTextBoxTime5.Size = new Size(134, 24);
            metroTextBoxTime5.TabIndex = 6;
            metroTextBoxTime5.UseSelectable = true;
            metroTextBoxTime5.WaterMark = "12:50";
            metroTextBoxTime5.WaterMarkColor = Color.FromArgb(109, 109, 109);
            metroTextBoxTime5.WaterMarkFont = new Font("Segoe UI", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            // 
            // metroTextBoxTime4
            // 
            // 
            // 
            // 
            metroTextBoxTime4.CustomButton.Image = null;
            metroTextBoxTime4.CustomButton.Location = new Point(83, 2);
            metroTextBoxTime4.CustomButton.Name = "";
            metroTextBoxTime4.CustomButton.Size = new Size(21, 21);
            metroTextBoxTime4.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            metroTextBoxTime4.CustomButton.TabIndex = 1;
            metroTextBoxTime4.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            metroTextBoxTime4.CustomButton.UseSelectable = true;
            metroTextBoxTime4.CustomButton.Visible = false;
            metroTextBoxTime4.Location = new Point(105, 20);
            metroTextBoxTime4.Name = "metroTextBoxTime4";
            metroTextBoxTime4.PasswordChar = '\0';
            metroTextBoxTime4.PromptText = "00:00";
            metroTextBoxTime4.ScrollBars = ScrollBars.None;
            metroTextBoxTime4.SelectedText = "";
            metroTextBoxTime4.SelectionLength = 0;
            metroTextBoxTime4.SelectionStart = 0;
            metroTextBoxTime4.ShortcutsEnabled = true;
            metroTextBoxTime4.Size = new Size(107, 26);
            metroTextBoxTime4.TabIndex = 5;
            metroTextBoxTime4.UseSelectable = true;
            metroTextBoxTime4.WaterMark = "00:00";
            metroTextBoxTime4.WaterMarkColor = Color.FromArgb(109, 109, 109);
            metroTextBoxTime4.WaterMarkFont = new Font("Segoe UI", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            metroTextBoxTime4.Click += metroTextBoxTime4_Click;
            // 
            // metroTextBoxTime3
            // 
            // 
            // 
            // 
            metroTextBoxTime3.CustomButton.Image = null;
            metroTextBoxTime3.CustomButton.Location = new Point(114, 2);
            metroTextBoxTime3.CustomButton.Name = "";
            metroTextBoxTime3.CustomButton.Size = new Size(19, 19);
            metroTextBoxTime3.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            metroTextBoxTime3.CustomButton.TabIndex = 1;
            metroTextBoxTime3.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            metroTextBoxTime3.CustomButton.UseSelectable = true;
            metroTextBoxTime3.CustomButton.Visible = false;
            metroTextBoxTime3.Location = new Point(284, 110);
            metroTextBoxTime3.Name = "metroTextBoxTime3";
            metroTextBoxTime3.PasswordChar = '\0';
            metroTextBoxTime3.PromptText = "metroTextBoxTime3";
            metroTextBoxTime3.ScrollBars = ScrollBars.None;
            metroTextBoxTime3.SelectedText = "";
            metroTextBoxTime3.SelectionLength = 0;
            metroTextBoxTime3.SelectionStart = 0;
            metroTextBoxTime3.ShortcutsEnabled = true;
            metroTextBoxTime3.Size = new Size(136, 24);
            metroTextBoxTime3.TabIndex = 4;
            metroTextBoxTime3.UseSelectable = true;
            metroTextBoxTime3.WaterMark = "metroTextBoxTime3";
            metroTextBoxTime3.WaterMarkColor = Color.FromArgb(109, 109, 109);
            metroTextBoxTime3.WaterMarkFont = new Font("Segoe UI", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            metroTextBoxTime3.Click += metroTextBoxTime3_Click;
            // 
            // metroTextBoxTime2
            // 
            // 
            // 
            // 
            metroTextBoxTime2.CustomButton.Image = null;
            metroTextBoxTime2.CustomButton.Location = new Point(45, 1);
            metroTextBoxTime2.CustomButton.Name = "";
            metroTextBoxTime2.CustomButton.Size = new Size(21, 21);
            metroTextBoxTime2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            metroTextBoxTime2.CustomButton.TabIndex = 1;
            metroTextBoxTime2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            metroTextBoxTime2.CustomButton.UseSelectable = true;
            metroTextBoxTime2.CustomButton.Visible = false;
            metroTextBoxTime2.Location = new Point(302, 44);
            metroTextBoxTime2.Name = "metroTextBoxTime2";
            metroTextBoxTime2.PasswordChar = '\0';
            metroTextBoxTime2.PromptText = "metroTextBoxTime2";
            metroTextBoxTime2.ScrollBars = ScrollBars.None;
            metroTextBoxTime2.SelectedText = "";
            metroTextBoxTime2.SelectionLength = 0;
            metroTextBoxTime2.SelectionStart = 0;
            metroTextBoxTime2.ShortcutsEnabled = true;
            metroTextBoxTime2.Size = new Size(67, 23);
            metroTextBoxTime2.TabIndex = 3;
            metroTextBoxTime2.UseSelectable = true;
            metroTextBoxTime2.WaterMark = "metroTextBoxTime2";
            metroTextBoxTime2.WaterMarkColor = Color.FromArgb(109, 109, 109);
            metroTextBoxTime2.WaterMarkFont = new Font("Segoe UI", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            // 
            // metroTextBox2
            // 
            // 
            // 
            // 
            metroTextBox2.CustomButton.Image = null;
            metroTextBox2.CustomButton.Location = new Point(134, 2);
            metroTextBox2.CustomButton.Name = "";
            metroTextBox2.CustomButton.Size = new Size(21, 21);
            metroTextBox2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            metroTextBox2.CustomButton.TabIndex = 1;
            metroTextBox2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            metroTextBox2.CustomButton.UseSelectable = true;
            metroTextBox2.CustomButton.Visible = false;
            metroTextBox2.Lines = new string[] { "metroTextBox2" };
            metroTextBox2.Location = new Point(68, 72);
            metroTextBox2.MaxLength = 32767;
            metroTextBox2.Name = "metroTextBox2";
            metroTextBox2.PasswordChar = '\0';
            metroTextBox2.ScrollBars = ScrollBars.None;
            metroTextBox2.SelectedText = "";
            metroTextBox2.SelectionLength = 0;
            metroTextBox2.SelectionStart = 0;
            metroTextBox2.ShortcutsEnabled = true;
            metroTextBox2.Size = new Size(158, 26);
            metroTextBox2.TabIndex = 2;
            metroTextBox2.Text = "metroTextBox2";
            metroTextBox2.UseSelectable = true;
            metroTextBox2.WaterMarkColor = Color.FromArgb(109, 109, 109);
            metroTextBox2.WaterMarkFont = new Font("Segoe UI", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            // 
            // tabPage1
            // 
            tabPage1.Controls.Add(htmlLabel1);
            tabPage1.HorizontalScrollbarBarColor = true;
            tabPage1.HorizontalScrollbarHighlightOnWheel = false;
            tabPage1.HorizontalScrollbarSize = 10;
            tabPage1.Location = new Point(4, 35);
            tabPage1.Name = "tabPage1";
            tabPage1.Size = new Size(505, 251);
            tabPage1.TabIndex = 0;
            tabPage1.Text = "tabPage1";
            tabPage1.VerticalScrollbarBarColor = true;
            tabPage1.VerticalScrollbarHighlightOnWheel = false;
            tabPage1.VerticalScrollbarSize = 10;
            // 
            // htmlLabel1
            // 
            htmlLabel1.AutoScroll = true;
            htmlLabel1.AutoScrollMinSize = new Size(69, 26);
            htmlLabel1.AutoSize = false;
            htmlLabel1.BackColor = SystemColors.Window;
            htmlLabel1.Controls.Add(metroButtonStyled1);
            htmlLabel1.Location = new Point(52, 42);
            htmlLabel1.Name = "htmlLabel1";
            htmlLabel1.Size = new Size(289, 101);
            htmlLabel1.TabIndex = 2;
            htmlLabel1.Text = "htmlLabel1";
            // 
            // metroButtonStyled1
            // 
            metroButtonStyled1.FlatAppearance = false;
            metroButtonStyled1.Font = new Font("Segoe UI", 13F, FontStyle.Regular, GraphicsUnit.Pixel);
            metroButtonStyled1.FontSize = MetroFramework.MetroButtonSize.Medium;
            metroButtonStyled1.FontWeight = MetroFramework.MetroButtonWeight.Regular;
            metroButtonStyled1.Highlight = false;
            metroButtonStyled1.Location = new Point(61, 36);
            metroButtonStyled1.Name = "metroButtonStyled1";
            metroButtonStyled1.Size = new Size(116, 74);
            metroButtonStyled1.Style = MetroFramework.MetroColorStyle.Teal;
            metroButtonStyled1.StyleManager = metroStyleManager1;
            metroButtonStyled1.TabIndex = 0;
            metroButtonStyled1.Text = "metroButtonStyled1";
            metroButtonStyled1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroStyleManager1
            // 
            metroStyleManager1.Owner = this;
            metroStyleManager1.Style = MetroFramework.MetroColorStyle.Teal;
            metroStyleManager1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // tabPage2
            // 
            tabPage2.Controls.Add(button1);
            tabPage2.Controls.Add(metroButton1);
            tabPage2.Controls.Add(metroMonthCalendar1);
            tabPage2.Controls.Add(metroLabel2);
            tabPage2.Controls.Add(metroDomainUpDown1);
            tabPage2.Controls.Add(metroComboBox1);
            tabPage2.HorizontalScrollbarBarColor = true;
            tabPage2.HorizontalScrollbarHighlightOnWheel = false;
            tabPage2.HorizontalScrollbarSize = 10;
            tabPage2.Location = new Point(4, 35);
            tabPage2.Name = "tabPage2";
            tabPage2.Size = new Size(505, 251);
            tabPage2.TabIndex = 1;
            tabPage2.Text = "tabPage2";
            tabPage2.VerticalScrollbarBarColor = true;
            tabPage2.VerticalScrollbarHighlightOnWheel = false;
            tabPage2.VerticalScrollbarSize = 10;
            tabPage2.Click += tabPage2_Click;
            // 
            // button1
            // 
            button1.Location = new Point(24, 88);
            button1.Name = "button1";
            button1.Size = new Size(47, 31);
            button1.TabIndex = 7;
            button1.Text = "button1";
            button1.UseVisualStyleBackColor = true;
            // 
            // metroButton1
            // 
            metroButton1.Location = new Point(418, 65);
            metroButton1.Name = "metroButton1";
            metroButton1.Size = new Size(55, 70);
            metroButton1.TabIndex = 6;
            metroButton1.Text = "metroButton1";
            metroButton1.UseSelectable = true;
            // 
            // metroMonthCalendar1
            // 
            metroMonthCalendar1.BackColor = Color.FromArgb(34, 34, 34);
            metroMonthCalendar1.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            metroMonthCalendar1.ForeColor = Color.FromArgb(204, 204, 204);
            metroMonthCalendar1.Location = new Point(79, 38);
            metroMonthCalendar1.Name = "metroMonthCalendar1";
            metroMonthCalendar1.Style = MetroFramework.MetroColorStyle.Teal;
            metroMonthCalendar1.StyleManager = metroStyleManager1;
            metroMonthCalendar1.TabIndex = 5;
            metroMonthCalendar1.Theme = MetroFramework.MetroThemeStyle.Dark;
            metroMonthCalendar1.TitleBackColor = Color.FromArgb(0, 170, 173);
            metroMonthCalendar1.TitleForeColor = Color.FromArgb(17, 17, 17);
            metroMonthCalendar1.TrailingForeColor = Color.FromArgb(17, 17, 17);
            // 
            // metroLabel2
            // 
            metroLabel2.AutoSize = true;
            metroLabel2.Location = new Point(69, 139);
            metroLabel2.Name = "metroLabel2";
            metroLabel2.Size = new Size(83, 19);
            metroLabel2.TabIndex = 4;
            metroLabel2.Text = "metroLabel2";
            // 
            // metroDomainUpDown1
            // 
            metroDomainUpDown1.BackColor = Color.FromArgb(255, 255, 255);
            metroDomainUpDown1.CustomDrawButtons = false;
            metroDomainUpDown1.Font = new Font("Segoe UI Light", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            metroDomainUpDown1.FontSize = MetroFramework.MetroLabelSize.Medium;
            metroDomainUpDown1.FontWeight = MetroFramework.MetroLabelWeight.Light;
            metroDomainUpDown1.Location = new Point(79, 91);
            metroDomainUpDown1.Name = "metroDomainUpDown1";
            metroDomainUpDown1.Size = new Size(130, 26);
            metroDomainUpDown1.Style = MetroFramework.MetroColorStyle.Teal;
            metroDomainUpDown1.StyleManager = metroStyleManager1;
            metroDomainUpDown1.TabIndex = 3;
            metroDomainUpDown1.Text = "metroDomainUpDown1";
            metroDomainUpDown1.Theme = MetroFramework.MetroThemeStyle.Dark;
            metroDomainUpDown1.UseAlternateColors = false;
            metroDomainUpDown1.UseStyleColors = false;
            // 
            // metroComboBox1
            // 
            metroComboBox1.FormattingEnabled = true;
            metroComboBox1.ItemHeight = 23;
            metroComboBox1.Location = new Point(3, 15);
            metroComboBox1.Name = "metroComboBox1";
            metroComboBox1.Size = new Size(287, 29);
            metroComboBox1.TabIndex = 2;
            metroComboBox1.UseSelectable = true;
            // 
            // metroTextBoxTime1
            // 
            // 
            // 
            // 
            metroTextBoxTime1.CustomButton.Image = null;
            metroTextBoxTime1.CustomButton.Location = new Point(68, 2);
            metroTextBoxTime1.CustomButton.Name = "";
            metroTextBoxTime1.CustomButton.Size = new Size(15, 15);
            metroTextBoxTime1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            metroTextBoxTime1.CustomButton.TabIndex = 1;
            metroTextBoxTime1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            metroTextBoxTime1.CustomButton.UseSelectable = true;
            metroTextBoxTime1.CustomButton.Visible = false;
            metroTextBoxTime1.Location = new Point(30, 418);
            metroTextBoxTime1.Name = "metroTextBoxTime1";
            metroTextBoxTime1.PasswordChar = '\0';
            metroTextBoxTime1.ScrollBars = ScrollBars.None;
            metroTextBoxTime1.SelectedText = "";
            metroTextBoxTime1.SelectionLength = 0;
            metroTextBoxTime1.SelectionStart = 0;
            metroTextBoxTime1.ShortcutsEnabled = true;
            metroTextBoxTime1.Size = new Size(86, 20);
            metroTextBoxTime1.TabIndex = 3;
            metroTextBoxTime1.UseSelectable = true;
            metroTextBoxTime1.WaterMarkColor = Color.FromArgb(109, 109, 109);
            metroTextBoxTime1.WaterMarkFont = new Font("Segoe UI", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            // 
            // metroTextBoxNumeric1
            // 
            // 
            // 
            // 
            metroTextBoxNumeric1.CustomButton.Image = null;
            metroTextBoxNumeric1.CustomButton.Location = new Point(108, 1);
            metroTextBoxNumeric1.CustomButton.Name = "";
            metroTextBoxNumeric1.CustomButton.Size = new Size(23, 23);
            metroTextBoxNumeric1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            metroTextBoxNumeric1.CustomButton.TabIndex = 1;
            metroTextBoxNumeric1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            metroTextBoxNumeric1.CustomButton.UseSelectable = true;
            metroTextBoxNumeric1.CustomButton.Visible = false;
            metroTextBoxNumeric1.Location = new Point(30, 374);
            metroTextBoxNumeric1.MaxLength = 32767;
            metroTextBoxNumeric1.Name = "metroTextBoxNumeric1";
            metroTextBoxNumeric1.PasswordChar = '\0';
            metroTextBoxNumeric1.ScrollBars = ScrollBars.None;
            metroTextBoxNumeric1.SelectedText = "";
            metroTextBoxNumeric1.SelectionLength = 0;
            metroTextBoxNumeric1.SelectionStart = 0;
            metroTextBoxNumeric1.ShortcutsEnabled = true;
            metroTextBoxNumeric1.Size = new Size(132, 25);
            metroTextBoxNumeric1.TabIndex = 5;
            metroTextBoxNumeric1.UseSelectable = true;
            metroTextBoxNumeric1.WaterMarkColor = Color.FromArgb(109, 109, 109);
            metroTextBoxNumeric1.WaterMarkFont = new Font("Segoe UI", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            // 
            // metroGrid1
            // 
            metroGrid1.AllowUserToResizeRows = false;
            metroGrid1.BackgroundColor = Color.FromArgb(255, 255, 255);
            metroGrid1.CellBorderStyle = DataGridViewCellBorderStyle.None;
            metroGrid1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = Color.FromArgb(0, 174, 219);
            dataGridViewCellStyle4.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            dataGridViewCellStyle4.ForeColor = Color.FromArgb(255, 255, 255);
            dataGridViewCellStyle4.SelectionBackColor = Color.FromArgb(0, 198, 247);
            dataGridViewCellStyle4.SelectionForeColor = Color.FromArgb(17, 17, 17);
            dataGridViewCellStyle4.WrapMode = DataGridViewTriState.True;
            metroGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            metroGrid1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = Color.FromArgb(255, 255, 255);
            dataGridViewCellStyle5.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            dataGridViewCellStyle5.ForeColor = Color.FromArgb(136, 136, 136);
            dataGridViewCellStyle5.SelectionBackColor = Color.FromArgb(0, 198, 247);
            dataGridViewCellStyle5.SelectionForeColor = Color.FromArgb(17, 17, 17);
            dataGridViewCellStyle5.WrapMode = DataGridViewTriState.False;
            metroGrid1.DefaultCellStyle = dataGridViewCellStyle5;
            metroGrid1.EnableHeadersVisualStyles = false;
            metroGrid1.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            metroGrid1.GridColor = Color.FromArgb(255, 255, 255);
            metroGrid1.Location = new Point(542, 101);
            metroGrid1.Name = "metroGrid1";
            metroGrid1.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = Color.FromArgb(0, 174, 219);
            dataGridViewCellStyle6.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = Color.FromArgb(255, 255, 255);
            dataGridViewCellStyle6.SelectionBackColor = Color.FromArgb(0, 198, 247);
            dataGridViewCellStyle6.SelectionForeColor = Color.FromArgb(17, 17, 17);
            dataGridViewCellStyle6.WrapMode = DataGridViewTriState.True;
            metroGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            metroGrid1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            metroGrid1.RowTemplate.Height = 25;
            metroGrid1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            metroGrid1.Size = new Size(194, 170);
            metroGrid1.TabIndex = 6;
            // 
            // metroTextBoxDate1
            // 
            // 
            // 
            // 
            metroTextBoxDate1.CustomButton.Image = null;
            metroTextBoxDate1.CustomButton.Location = new Point(134, 1);
            metroTextBoxDate1.CustomButton.Name = "";
            metroTextBoxDate1.CustomButton.Size = new Size(21, 21);
            metroTextBoxDate1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            metroTextBoxDate1.CustomButton.TabIndex = 1;
            metroTextBoxDate1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            metroTextBoxDate1.CustomButton.UseSelectable = true;
            metroTextBoxDate1.CustomButton.Visible = false;
            metroTextBoxDate1.Location = new Point(283, 382);
            metroTextBoxDate1.Name = "metroTextBoxDate1";
            metroTextBoxDate1.PasswordChar = '\0';
            metroTextBoxDate1.ScrollBars = ScrollBars.None;
            metroTextBoxDate1.SelectedText = "";
            metroTextBoxDate1.SelectionLength = 0;
            metroTextBoxDate1.SelectionStart = 0;
            metroTextBoxDate1.ShortcutsEnabled = true;
            metroTextBoxDate1.Size = new Size(156, 23);
            metroTextBoxDate1.TabIndex = 7;
            metroTextBoxDate1.UseSelectable = true;
            metroTextBoxDate1.WaterMarkColor = Color.FromArgb(109, 109, 109);
            metroTextBoxDate1.WaterMarkFont = new Font("Segoe UI", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            // 
            // metroTextBoxDate2
            // 
            // 
            // 
            // 
            metroTextBoxDate2.CustomButton.Image = null;
            metroTextBoxDate2.CustomButton.Location = new Point(67, 1);
            metroTextBoxDate2.CustomButton.Name = "";
            metroTextBoxDate2.CustomButton.Size = new Size(27, 27);
            metroTextBoxDate2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            metroTextBoxDate2.CustomButton.TabIndex = 1;
            metroTextBoxDate2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            metroTextBoxDate2.CustomButton.UseSelectable = true;
            metroTextBoxDate2.CustomButton.Visible = false;
            metroTextBoxDate2.Location = new Point(225, 416);
            metroTextBoxDate2.Name = "metroTextBoxDate2";
            metroTextBoxDate2.PasswordChar = '\0';
            metroTextBoxDate2.PromptText = "dd/MM/aaaaa";
            metroTextBoxDate2.ScrollBars = ScrollBars.None;
            metroTextBoxDate2.SelectedText = "";
            metroTextBoxDate2.SelectionLength = 0;
            metroTextBoxDate2.SelectionStart = 0;
            metroTextBoxDate2.ShortcutsEnabled = true;
            metroTextBoxDate2.Size = new Size(95, 29);
            metroTextBoxDate2.TabIndex = 8;
            metroTextBoxDate2.UseSelectable = true;
            metroTextBoxDate2.WaterMark = "dd/MM/aaaaa";
            metroTextBoxDate2.WaterMarkColor = Color.FromArgb(109, 109, 109);
            metroTextBoxDate2.WaterMarkFont = new Font("Segoe UI", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            // 
            // metroGrid2
            // 
            metroGrid2.AllowUserToResizeRows = false;
            metroGrid2.BackgroundColor = Color.FromArgb(255, 255, 255);
            metroGrid2.CellBorderStyle = DataGridViewCellBorderStyle.None;
            metroGrid2.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = Color.FromArgb(0, 174, 219);
            dataGridViewCellStyle1.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = Color.FromArgb(255, 255, 255);
            dataGridViewCellStyle1.SelectionBackColor = Color.FromArgb(0, 198, 247);
            dataGridViewCellStyle1.SelectionForeColor = Color.FromArgb(17, 17, 17);
            dataGridViewCellStyle1.WrapMode = DataGridViewTriState.True;
            metroGrid2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            metroGrid2.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = Color.FromArgb(255, 255, 255);
            dataGridViewCellStyle2.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = Color.FromArgb(136, 136, 136);
            dataGridViewCellStyle2.SelectionBackColor = Color.FromArgb(0, 198, 247);
            dataGridViewCellStyle2.SelectionForeColor = Color.FromArgb(17, 17, 17);
            dataGridViewCellStyle2.WrapMode = DataGridViewTriState.False;
            metroGrid2.DefaultCellStyle = dataGridViewCellStyle2;
            metroGrid2.EnableHeadersVisualStyles = false;
            metroGrid2.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            metroGrid2.GridColor = Color.FromArgb(255, 255, 255);
            metroGrid2.Location = new Point(487, 339);
            metroGrid2.Name = "metroGrid2";
            metroGrid2.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = Color.FromArgb(0, 174, 219);
            dataGridViewCellStyle3.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = Color.FromArgb(255, 255, 255);
            dataGridViewCellStyle3.SelectionBackColor = Color.FromArgb(0, 198, 247);
            dataGridViewCellStyle3.SelectionForeColor = Color.FromArgb(17, 17, 17);
            dataGridViewCellStyle3.WrapMode = DataGridViewTriState.True;
            metroGrid2.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            metroGrid2.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            metroGrid2.RowTemplate.Height = 25;
            metroGrid2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            metroGrid2.Size = new Size(135, 89);
            metroGrid2.TabIndex = 9;
            metroGrid2.CellContentClick += metroGrid2_CellContentClick;
            // 
            // metroTextBoxTime6
            // 
            // 
            // 
            // 
            metroTextBoxTime6.CustomButton.Image = null;
            metroTextBoxTime6.CustomButton.Location = new Point(74, 2);
            metroTextBoxTime6.CustomButton.Name = "";
            metroTextBoxTime6.CustomButton.Size = new Size(15, 15);
            metroTextBoxTime6.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            metroTextBoxTime6.CustomButton.TabIndex = 1;
            metroTextBoxTime6.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            metroTextBoxTime6.CustomButton.UseSelectable = true;
            metroTextBoxTime6.CustomButton.Visible = false;
            metroTextBoxTime6.Location = new Point(393, 26);
            metroTextBoxTime6.Name = "metroTextBoxTime6";
            metroTextBoxTime6.PasswordChar = '\0';
            metroTextBoxTime6.ScrollBars = ScrollBars.None;
            metroTextBoxTime6.SelectedText = "";
            metroTextBoxTime6.SelectionLength = 0;
            metroTextBoxTime6.SelectionStart = 0;
            metroTextBoxTime6.ShortcutsEnabled = true;
            metroTextBoxTime6.Size = new Size(92, 20);
            metroTextBoxTime6.TabIndex = 7;
            metroTextBoxTime6.UseSelectable = true;
            metroTextBoxTime6.WaterMarkColor = Color.FromArgb(109, 109, 109);
            metroTextBoxTime6.WaterMarkFont = new Font("Segoe UI", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle;
            ClientSize = new Size(775, 480);
            Controls.Add(metroGrid2);
            Controls.Add(metroTextBoxDate2);
            Controls.Add(metroTextBoxDate1);
            Controls.Add(metroGrid1);
            Controls.Add(metroTextBoxTime1);
            Controls.Add(metroTextBoxNumeric1);
            Controls.Add(metroTabControl1);
            Controls.Add(metroProgressBar1);
            Controls.Add(metroTextBox1);
            Controls.Add(metroLabel1);
            Name = "Form1";
            ShadowType = MetroFramework.Forms.MetroFormShadowType.None;
            Text = "Form1";
            Theme = MetroFramework.MetroThemeStyle.Dark;
            WindowState = FormWindowState.Maximized;
            Load += Form1_Load;
            metroTabControl1.ResumeLayout(false);
            tabPage3.ResumeLayout(false);
            tabPage1.ResumeLayout(false);
            htmlLabel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)metroStyleManager1).EndInit();
            tabPage2.ResumeLayout(false);
            tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)metroGrid1).EndInit();
            ((System.ComponentModel.ISupportInitialize)metroGrid2).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox metroTextBox1;
        private MetroFramework.Controls.MetroProgressBar metroProgressBar1;
        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage tabPage1;
        private MetroFramework.Controls.MetroTabPage tabPage2;
        private MetroFramework.Controls.MetroTabPage tabPage3;
        private MetroFramework.Drawing.Html.HtmlLabel htmlLabel1;
        private MetroFramework.Controls.MetroButtonStyled metroButtonStyled1;
        private MetroFramework.Controls.MetroMonthCalendar metroMonthCalendar1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroDomainUpDown metroDomainUpDown1;
        private MetroFramework.Controls.MetroComboBox metroComboBox1;
        private MetroFramework.Components.MetroStyleManager metroStyleManager1;
        private Button button1;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Components.MetroStyleExtender metroStyleExtender1;
        private MetroFramework.Controls.MetroTextBox metroTextBox2;
        private MetroFramework.Controls.MetroTextBoxTime metroTextBoxTime1;
        private MetroFramework.Controls.MetroGrid metroGrid1;
        private MetroFramework.Controls.MetroTextBoxNumeric metroTextBoxNumeric1;
        private MetroFramework.Controls.MetroTextBoxDate metroTextBoxDate1;
        private MetroFramework.Controls.MetroTextBoxDate metroTextBoxDate2;
        private MetroFramework.Controls.MetroGrid metroGrid2;
        private MetroFramework.Controls.MetroTextBoxTime metroTextBoxTime2;
        private MetroFramework.Controls.MetroTextBoxTime metroTextBoxTime3;
        private MetroFramework.Controls.MetroTextBoxTime metroTextBoxTime4;
        private MetroFramework.Controls.MetroTextBoxTime metroTextBoxTime5;
        private MetroFramework.Controls.MetroTextBoxTime metroTextBoxTime6;
    }
}