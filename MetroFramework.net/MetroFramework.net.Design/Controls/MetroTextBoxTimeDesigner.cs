﻿/**
 * MetroFramework - Modern UI for WinForms
 * 
 * The MIT License (MIT)
 * Copyright (c) 2011 Sven Walter, http://github.com/viperneo
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of 
 * this software and associated documentation files (the "Software"), to deal in the 
 * Software without restriction, including without limitation the rights to use, copy, 
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the 
 * following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using MetroFramework.Controls;
using System;
using System.Collections;
using System.Windows.Forms.Design;

namespace MetroFramework.Design.Controls
{
    internal class MetroTextBoxTimeDesigner : ControlDesigner
    {
        public override SelectionRules SelectionRules
        {
            get
            {
                return base.SelectionRules;
            }
        }

        protected override void PreFilterProperties(IDictionary properties)
        {

            properties.Remove("AutoCompleteSource");
            properties.Remove("AutoCompleteMode");
            properties.Remove("AutoCompleteCustomSource");
            properties.Remove("CharacterCasing");
            properties.Remove("Lines");
            properties.Remove("MaxLength");
            properties.Remove("Multiline");
            properties.Remove("PasswordChar");
            properties.Remove("PromptText");
            properties.Remove("ScrollBars");

            properties.Remove("BackgroundImage");
            properties.Remove("ForeColor");
            properties.Remove("ImeMode");
            properties.Remove("Padding");
            properties.Remove("BackgroundImageLayout");
            properties.Remove("BackColor");
            properties.Remove("Font");


            base.PreFilterProperties(properties);
        }

        public override void InitializeNewComponent(IDictionary defaultValues)
        {
            base.InitializeNewComponent(defaultValues);

            MetroTextBoxTime control = this.Component as MetroTextBoxTime;
            control.Text = "";
            control.WaterMark = DateTime.Today.ToString("HH:mm");
            control.Size = new System.Drawing.Size(65, 26);
            control.MinimumSize = control.Size;
        }
    }
}
