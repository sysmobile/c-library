﻿using MetroFramework.Design;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;

namespace MetroFramework.Controls
{
    [Designer(typeof(MetroTextBoxTimeDesigner))]
    public class MetroTextBoxTime : MetroTextBox
    {
        [DefaultValue(5)]
        public new int MaxLength { get => 5; set=>base.MaxLength = 5; }

        [DefaultValue("")]
        public new string WaterMark { get=>base.WaterMark; set => base.WaterMark = value; }

        [DefaultValue("")]
        public new string Text { get=>base.Text; set=>base.Text = value; }


        public MetroTextBoxTime()
        {
            KeyPress += MetroTextBoxTime_KeyPress;
        }

        private void MetroTextBoxTime_KeyPress(object sender, KeyPressEventArgs e)
        {
            string inputText = Text.PadLeft(MaxLength);
            char[] outputTime = inputText.ToCharArray();
            int cursorPosic = SelectionStart;

            if ((cursorPosic == MaxLength) && (e.KeyChar != 8))
            {
                e.Handled = true;
                return;
            }

            outputTime[2] = ':';

            if (e.KeyChar >= 48 && e.KeyChar <= 57)
            {
                if (cursorPosic == 2) cursorPosic = 3;
                outputTime[cursorPosic] = e.KeyChar;
                Text = Join(outputTime);

                e.Handled = true;

                if (Text.Length <= 10)
                    SelectionStart = cursorPosic + 1;
            }
            else if (e.KeyChar == 8)
            {
                if (cursorPosic == 3) cursorPosic = 2;
                var charPosic = cursorPosic - 1;
                if (charPosic < 0)
                {
                    e.Handled = true;
                    return;
                }

                outputTime[charPosic] = ' ';

                Text = Join(outputTime);
                SelectionStart = cursorPosic - 1;
                e.Handled = true;
            }
            else if (e.KeyChar == 13 || e.KeyChar == 9)
            {

            }
            else
            {
                e.Handled = true;
            }

        }

        string Join(char[] chars)
        {
            var sb = new StringBuilder();
            sb.Append(chars);
            return sb.ToString();
        }
    }
}
