﻿using MetroFramework.Design;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;

namespace MetroFramework.Controls
{
    [Designer(typeof(MetroTextBoxDateDesigner))]
    public class MetroTextBoxDate : MetroTextBox
    {
        [DefaultValue(10)]
        public new int MaxLength { get => 10; set=> base.MaxLength = 10; }

        public MetroTextBoxDate()
        {
            KeyPress += MetroTextBoxDate_KeyPress;
        }

        private void MetroTextBoxDate_KeyPress(object sender, KeyPressEventArgs e)
        {
            string inputText = Text.PadLeft(MaxLength);
            char[] outputDate = inputText.ToCharArray();
            int cursorPosic = SelectionStart;

            if ((cursorPosic == MaxLength) && (e.KeyChar != 8))
            {
                e.Handled = true;
                return;
            }

            if (cursorPosic == 0)
            {
                // Key > 3
                if (e.KeyChar > 51)
                {
                    e.Handled = true;
                    return;
                }
            }

            outputDate[2] = '/';
            outputDate[5] = '/';

            if (e.KeyChar >= 48 && e.KeyChar <= 57)
            {
                if (cursorPosic == 2) cursorPosic = 3;
                if (cursorPosic == 5) cursorPosic = 6;

                if (cursorPosic == 3)
                {
                    //Key > 1
                    if (e.KeyChar > 49)
                    {
                        e.Handled = true;
                        return;
                    }
                }

                if (cursorPosic == 4)
                {
                    //key>2
                    if (e.KeyChar > 50)
                    {
                        e.Handled = outputDate[3].ToString().CompareTo("0") == 1;
                        if (e.Handled) return;
                    }
                }

                outputDate[cursorPosic] = e.KeyChar;
                Text = Join(outputDate);

                e.Handled = true;

                if (Text.Length <= 10)
                    SelectionStart = cursorPosic + 1;
            }
            else if (e.KeyChar == 8)
            {
                if (cursorPosic == 3) cursorPosic = 2;
                if (cursorPosic == 6) cursorPosic = 5;

                var charPosic = cursorPosic - 1;
                if (charPosic < 0)
                {
                    e.Handled = true;
                    return;
                }

                outputDate[charPosic] = ' ';

                Text = Join(outputDate);
                SelectionStart = cursorPosic - 1;
                e.Handled = true;
            }
            else if (e.KeyChar == 13 || e.KeyChar == 9)
            {

            }
            else
            {
                e.Handled = true;
            }

        }

        string Join(char[] chars)
        {
            var sb = new StringBuilder();
            sb.Append(chars);
            return sb.ToString();
        }
    }
}