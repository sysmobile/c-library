﻿using MetroFramework.Design;
using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

namespace MetroFramework.Controls
{
    public enum ENumberType
    {
        NumberWithDecimals,
        NumberWithoutDecimals,
        CurrencyWithDecimals,
        CurrencyWithoutDecimals
    }

    [Designer(typeof(MetroTextBoxNumericDesigner))]
    public class MetroTextBoxNumeric : MetroTextBox
    {
        [Browsable(true)]
        [DefaultValue(ENumberType.NumberWithDecimals)]
        public ENumberType NumberType { get => _numberType; set => _numberType = value; }

        public MetroTextBoxNumeric()
        {
            KeyPress += MetroTextBoxNumeric_KeyPress;
            KeyDown += MetroTextBoxNumeric_KeyDown;

        }

        //CAPTURE CTRL+V
        private void MetroTextBoxNumeric_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.V && (e.Modifiers & Keys.Control) != Keys.None)
            {
                if (!IsValidTextToPaste(Clipboard.GetText()))
                {
                    e.Handled = true;
                    e.SuppressKeyPress = true;
                }
                else
                {
                    Text = decimal.Parse(Clipboard.GetText()).ToString();
                    e.Handled = true;
                    e.SuppressKeyPress = true;
                }
            }
        }

        private bool IsValidTextToPaste(string clipBoardText)
        {
            bool isvalid = decimal.TryParse(clipBoardText, out var result);
            if (isvalid)
            {
                if (_numberType == ENumberType.NumberWithoutDecimals
                    || _numberType == ENumberType.CurrencyWithoutDecimals)
                {
                    isvalid = (result % 1 == 0);
                }
            }
            return isvalid;
        }

        private void MetroTextBoxNumeric_KeyPress(object sender, KeyPressEventArgs e)
        {
            char decimalSeparator = GetDecimalSeparator();
            e.KeyChar = ReplaceNumberDecimalSeparator(e.KeyChar, decimalSeparator);

            bool valid = char.IsControl(e.KeyChar)
                || char.IsDigit(e.KeyChar)
                || ((e.KeyChar == decimalSeparator) && (_numberType == ENumberType.CurrencyWithDecimals || _numberType == ENumberType.NumberWithDecimals));


            if (valid && e.KeyChar == decimalSeparator)
                if (_numberType == ENumberType.NumberWithDecimals || _numberType == ENumberType.CurrencyWithDecimals)
                    valid = ((sender as MetroTextBoxNumeric).Text.IndexOf(decimalSeparator) == -1);

            e.Handled = !valid;
        }

        char GetDecimalSeparator()
        {
            char result = Convert.ToChar(Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator);

            if (_numberType == ENumberType.CurrencyWithDecimals)
                result = Convert.ToChar(Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator);
            else if (_numberType == ENumberType.NumberWithDecimals)
                result = Convert.ToChar(Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator);

            return result;
        }

        char ReplaceNumberDecimalSeparator(char typedChar, char decimalSeparator)
        {
            if (typedChar == '.' || typedChar == ',')
                return decimalSeparator;
            else
                return typedChar;
        }

        private ENumberType _numberType;


    }
}
